function [value,isterminal,direction] = bounceEvents(t,Y)
% Locate the time when height passes through zero in a decreasing direction
% and stop integration.  
value = Y(2);    % detect height = 0 or x outside of the unit interval
isterminal = 1;   % stop the integration
direction = -1;   % directions