%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 8                                                         %
%     Lecture 8a : ODEs in MATLAB                                         %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Some examples

% MATLAB is good about providing examples, so let's look at some!
odeexamples

%% MATLAB's ODE Solvers for initial-value problems

% MATLAB  has first-order, numerical ODE solvers for initial value problems
% In general, use them like [T,Y] = solver(odefun,tspan,y0)
%  to solve dY/dt = odefun(t,Y), Y(0) == y0 for t = tspan(1) to tspan(end)

% ex : y' == -y , y(0) == 1;
close all; clear; clc
tspan = [0 1]; 
%tspan = linspace(0,1,100); %uncomment to compute at these points
odefun = @(t,y) -y;
y0 = 1;
[t,y] = ode45(odefun, tspan, y0);
odeExactSol = @(t) y0 * exp(-t);
figure;
plot(t,y,'b-x',t,odeExactSol(t),'r-o');
legend('Approx','Exact');

% Converting higher order systems to first-order systems
%  introduce new variables, one for each derivative beyond the first
% In Mathematica, the built-in symbolic capabalities make this sort
%  of thing unnecessary, but in MATLAB, what might be seen as a 
%  mathematical convenience is a required skill for you to have!

%{
 ex 1 : 

 y'' + y' + y == 0, y(0) == 1, y'(0) == 0 
 
 let p = y'(t) then the (first order) system is
 
 p' == -p - y
 y' == p
 y(0) == 1
 p(0) == 0

 let Y = [y ; p] 

%}
close all; clear; clc
odefun = @(t,Y) [-Y(1) - Y(2) ; Y(1) ];
Y0 = [1 0]';
%tspan = [0 10];
tspan = linspace(0,10,1000);
[t,Y] = ode45(odefun,tspan,Y0);
r = roots([1 1 1]);
odeExactSol = @(t) 0.5*(exp(r(1)*t) + exp(r(2)*t));
figure;
plot(t,Y(:,1),'b-x',t,odeExactSol(t),'r-o');
legend('Approx','Exact');

% ex2 
%{

Now consider y as a 2-vector

Ay'' + By' + Cy == 0 
y(0) = [1 1]'
y'(0)= [0 0]'

%}
close all; clear; clc
A = 1+rand(2);
B = 1+rand(2);
C = 1+rand(2);
odefun = @(t,Y) [-inv(A)*(B*Y(3:4) - C*Y(1:2)); Y(1:2)]; % later, we'll see how to do this without computing inv(A)
Y0 = [1 1 0 0]';
tspan = [0 10];
[t,Y] = ode45(odefun,tspan,Y0);

figure;
plot(t,Y(:,1:2));

%% Choosing a solver

% we'll work with this example,
odefun = @(t, y) cos(t) - y;

tspan = [0 2];
y0 = 0;
exact = @(t) 0.5*(-exp(-t) + cos(t) + sin(t));

close all; 

% The first thing to try, ode45 - the workhorse
tic
[t, y] = ode45(odefun, tspan, y0,odeset('Stats','on'));
comptime = toc;
subplot(1,4,1);
plot(t, y, t, exact(t), 'r--')
 title(sprintf('ode45 [%d s]',comptime));


% when you only need a crude solution
tic
[t, y] = ode23(odefun, tspan, y0,odeset('Stats','on'));
comptime = toc;
subplot(1,4,2);
plot(t, y, t, exact(t), 'r--')
 title(sprintf('ode23 [%d s]',comptime));

% Multistep - for even higher accuracy, or when function evals are
% expensive
tic
[t, y] = ode113(odefun, tspan, y0, odeset('Stats','on'));
subplot(1,4,3); 
comptime = toc;
plot(t, y, t, exact(t), 'r--')
 title(sprintf('ode113 [%d s]',comptime));

 % Note that all of these are inaccuate! The default behavior
 %  is to go for speed, not accuracy
 
 tic
[t, y] = ode113(odefun, tspan, y0,...
    odeset('RelTol',1e-10,'AbsTol',1e-10,'NormControl','on','Stats','on','MaxStep',0.001));
comptime = toc;
subplot(1,4,4); 
plot(t, y, t, exact(t), 'r--')
 title(sprintf('ode113 again [%d s]',comptime));

%% Stiffness
% An ode is called stiff when the solution varies rapidly when compared to
% the time scale one is ultimately interested in. 

dYdt = @(t,Y) -Y
tSpan = [0 1];
Y0 = 1;
[T,Y] = ode45(dYdt,tSpan,Y0);
comptime = toc;
plot(T,Y); shg
fprintf('computed using %d steps and %g seconds\n',length(T),comptime);

% A problem with 2 time scales, one very short
dYdt = @(t,Y) -diag([10000 1])*Y
tSpan = [0 1];
Y0 = [1 1]';
[T,Y] = ode45(dYdt,tSpan,Y0,odeset('Stats','on'));
comptime = toc;
plot(T,Y); shg
fprintf('computed using %d steps and %g seconds\n',length(T),comptime);

% stiff solvers

% ode15s - a multistep solver for stiff systems
dYdt = @(t,Y) -diag([10000 1])*Y;
tSpan = [0 1];
Y0 = [1 1]';
tic
[T,Y] = ode15s(dYdt,tSpan,Y0);
comptime = toc;
plot(T,Y); shg
fprintf('computed using %d steps and %g seconds\n',length(T),comptime);

% For crude solves of stiff systems (single step)
help ode23s

% Others
help ode23t
help ode23tb

%% Implicit ODE
fimp = @(t,y,yp) yp + y;
[t,y] = ode15i(fimp,[0 1],1,0);
close all;
plot(t,y); 

iburgersode

edit iburgersode

%% Setting options
%  Just like the optimization routines have optimset, the ode routines
%  have odeset

% Let's apply some common options to a simple problem

% Error control
% 'RelTol' %defaults to 1e-3, which is fairly 'loose' by many standards (3 digits)
% 'AbsTol' 
% 'NormControl'

% Problems with a mass matrix
% If the problem is of the form M y' = f(y,t)
% then it may not be practical or desirable to store inv(M)
% 'Mass'

% our example from earlier:
close all; clear; clc
A = 1+rand(2);
B = 1+rand(2);
C = 1+rand(2);
Z = zeros(2);
opt = odeset('Mass',[A ,Z ; Z , eye(2)]);
odefun = @(t,Y) [(B*Y(3:4) - C*Y(1:2)); Y(1:2)]; % later, we'll see how to do this without computing inv(A)
Y0 = [1 1 0 0]';
tspan = [0 10];
[t,Y] = ode45(odefun,tspan,Y0,opt);

figure;
plot(t,Y(:,1:2));

%'Refine' %defaults to 4 with ode45, otherwise 1

% 'MaxStep'

%% Events
% As we saw in Mathematica, we'd often like to stop solving an 
% ODE when some criteria are satisfied. This is defined by zeros of
% a function

% Y is of the form [x y vx vy]'

% boring
odefun = @(t,Y) [Y(3); Y(4) ; 0; -9.8];
Y0 = [0.5 ; 0.5; 0.1; 0];
tspan = [0 10];
[~,Y] = ode45(odefun,tspan,Y0);
close all;
plot(Y(:,1),Y(:,2));

% more interesting, define a function of the form
% [value,isterminal,direction] = events(t,y)
odefun = @(t,Y) [Y(3); Y(4) ; 0; -9.8];
opt = odeset('Events',@bounceEvents);
tspan = [0 10];
numBounces = 10;
close all;
figure; hold on;
Y0 = [0.5 ; 0.5; 0.1; 0];
for i=1:numBounces
    [~,Y] = ode45(odefun,tspan,Y0,opt);
    plot(Y(:,1),Y(:,2));
    Y0 = Y(end,:)';
    Y0(4) = -Y0(4); %reverse y velocity
end
hold off; shg

% even more interesting
odefun = @(t,Y) [Y(3); Y(4) ; 0; -9.8];
opt = odeset('Events',@bounceEvents2,'Refine',20);
tspan = [0 50];
numBounces = 100;
close all;
figure; hold on;
Y0 = [0.5 ; 0.5; 1; 0];
for i=1:numBounces
    [~,Y,~,~,IE] = ode45(odefun,tspan,Y0,opt);
    plot(Y(:,1),Y(:,2));
    Y0 = Y(end,:)';
    if(any(IE == 1)) % ground
        Y0(4) = -Y0(4); %reverse y velocity
    else % wall
        Y0(3) = -Y0(3); % reverse x velocity
    end
    Y0(3:4) = 0.95*Y0(3:4); %attenuate velocities
end
hold off; shg

% ODE plotting and output functions

% some built in plotting functions (you can also define your own)
help odeplot
help odephas2
help odephas3

tmax = 50;
f = @(t,y) y*cos(t^2)
opt = odeset('OutPutFcn', @odeplot);

ode45(f, [0 tmax], 1,opt)

ode113(f, [0 tmax], 1,opt)

ode15s(f, [0 tmax], 1,opt)

% Stats
opt = odeset('Stats', 'on');
ode45(f, [0 tmax], 1,opt)

%  Orbit example, with plot functions and events
edit orbitode 

%% DAE

% We'll briefly mention that MATLAB can also solve certain 
% 'Differential Algebraic Equations' with ode15s or ode23t

% take a look at this built-in example, and note how it
%  uses a singular 'Mass' matrix
hb1dae
edit hb1dae

%% Boundary Value Problems in MATLAB

% Also be aware that MATLAB has similar functions for solving boundary 
% value problems

mat4bvp
edit mat4bvp
help bvp4c
help bvp5c
help bvpset
