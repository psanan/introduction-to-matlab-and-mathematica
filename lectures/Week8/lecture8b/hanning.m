function [window] = hanning(N,dummy)
%HANNING function to create a hanning window.
%   hanning(N) creates a hanning window of length N.
x = (1:N)';
window = 0.5*cos(x*2*pi/N+pi)+1;  