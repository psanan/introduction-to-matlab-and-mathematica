%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 8                                                         %
%     Lecture 8b :  Spectral methods (and cell arrays)                    %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% The discrete Fourier transform:
% Consider a "signal" which is a discrete representation of a function,
% that is a set of function values at evenly-spaced time intervals

% Consider the following two signals:

% A random signal:
sr = 44100;
n = 2*sr;
t = (0:n-1)'/sr;
noise = 2*rand(n,1)-1;
stem(t,noise);

% A sinusoid:
freq = 250;
testTone = sin(2*pi*t*freq);
figure;
stem(t,testTone); shg

% Obviously, we use the same amount of space to store each signal
size(noise)
size(testTone)

%{
 However, to *describe* the signals, the situation is different
  to rebuild the noise signal, we need all n values
  to reconstruct the testTone signal, we just need to know that it's
  a sinusoid at some frequency, amplitude and phase (3 numbers)

 So, the natural question is whether there's a better way to represent
  signals.

 Recall from linear algebra the idea of a basis, and that of an
 orthonormal basis :

 - A basis for a vector space is a set of vectors b_i such
     that any vector x can be written as a linear combination of 
     the b_i
 - An orthonormal basis for an inner product space is a basis
     such that <b_i,b_j> = \delta_ij   , that is, different
     basis vectors are orthogonal, and basis vectors all have norm 1

%}

% The standard basis - basis vectors have 1 in one entry, 0 elsewhere
v = [ 1 3 4 5]'
v - ( [1 0 0 0]' + 3*[0 1 0 0]' + 4*[ 0 0 1 0]' + 5*[0 0 0 1]')

% Since my basis is orthonormal, it's easy to compute the coefficients!
dot(v,[0 1 0 0])
[0 1 0 0]*v

% Haar basis (each column of B is a basis vector - note the transpose)
B = [[1 1 1 1]/2; % average
    [1 1 -1 -1]/2; % large scale difference
    [1 -1 0 0]/sqrt(2);  % small scale difference
    [0 0 1 -1]/sqrt(2)]' % small scale difference
coeff = B'*v
v - (coeff(1)*B(:,1) + coeff(2)*B(:,2) + coeff(3)*B(:,3) + coeff(4)*B(:,4))

% This basis is very handy (in fact, it's a starting point for wavelet 
%  analysis), but how do we capture the the fact that this
soundsc(noise,sr)
% sounds much more complicated than this
soundsc(testTone,sr)
%?

% The Fourier basis - sines and cosines
%  or , noting cos(x) = (1/2)(e^ix + e^-ix), sin(x) = (1/2i)(e^ix-e^-ix)
%  a basis of terms e^{-ix}

% Basis vectors (in a different order than MATLAB presents them)
n = 4;
F = exp(-1i*2*pi*(0:n-1)'*(0:n-1)/n)


% We can put our signals into this fourier basis

noiseF = fft(noise); 
stem(abs(noiseF));

testToneF = fft(testTone); %just as noisy :(
stem(abs(testToneF))

find(abs(testToneF)  >= 1e-8);
testToneFSparse = spalloc(n,1,2);
testToneFSparse(abs(testToneF) >= 1e-8) = 1; % sparse!


%% The FFT in MATLAB

% fft and ifft
fft
ifft

% scaling
fft([ 1 1 1 1])
ifft([ 1 1 1 1])

% note that the fft and ifft MATLAB uses are NOT unitary (they change the
%  norm of the signal)
x = rand(10,1);
X = fft(x);
norm(x)
norm(X)
norm(X)/norm(x) % should be sqrt(10)

% ordering of 'bins' is 0 (DC), then positive frequencies low to high, then negative
% frequencies high to low. This is most clear if you look at the spectrum
% of a sound:
x = wavread('noisy_ADA');
figure; 
plot(abs(fft(x))); shg;

%ordering
n = 1024;
k = [0:n/2, -n/2+1:-1];

% fftshift
% to shift DC to the center
x = rand(1024,1)-0.5;
plot(abs(fftshift(fft(x)))); shg;

% multidimensional fft
% fft2
% fftn

% padding

% while O(N log N) algorithms exists for any size FFT,
%  the constants are smaller when N is a power of 2

% time for some powers of 2 and some primes
close all; figure; hold on;

n2 = (2.^(6:18))'; % a column for convenience later
num_n2 = length(n2);
t_n2 = NaN(num_n2,1);
for i=1:num_n2
    n = n2(i);
    x = rand(n,1);
    tic;
    X = fft(x);
    t_n2(i) = toc;
end
    plot(n2,t_n2,'bo');

% fit C + D n log n  in a least squares sense
%  this is a linear least squares problem, so we can just solve
%  with the backslash (more on regression in week 9)

% We want to solve [ones nlogn] [C; D] = data
%  in a least squares sense

nlogn2 = n2.*log(n2);
param2 = [ones(num_n2,1), nlogn2]\t_n2;
fit2 = param2(2)*nlogn2 + param2(1);
plot(n2,fit2,'b--');
  
% Repeat for some prime values of n
np = [691  1553 12347 15287  20327 62617 92639 104717 224737]';
num_np = length(np);
t_np = NaN(num_np,1);
for i=1:num_np
    n = np(i);
    x = rand(n,1);
    tic;
    X = fft(x);
    t_np(i) = toc;
end
plot(np,t_np,'rx');


nlognp = np.*log(np);
paramp = [ones(num_np,1), nlognp]\t_np;
fitp = paramp(2)*nlognp + paramp(1);
plot(np,fitp,'r--');


xlabel('n');
ylabel('time [s]')
title('Time to compute an n-point fft');
legend('Powers of 2','C + D n log n fit','Primes','C + D n log n fit');

% Thus, if you can get away with it, pad!
help padarray

% How does it work?
web('fftw.org')

%% Other time-frequency tools

% Windowed FFTs (STFT)

% How does your ear work? IANAB, but there are certain cells which respond
% to certain frequencies. Clearly, we can detect changing spectra of
% sounds:
sr = 44100;
n = 2*sr;
t = (0:n-1)/sr;
chirp = sin(2*pi*t.*linspace(40,500,n));
chirpF = fft(chirp);
figure; 
subplot(1,3,1);
plot(t,chirp);
subplot(1,3,2);
stem(t,abs(chirpF));

% 
%soundsc(chirp,sr);

% but the DFT describes one spectrum for a signal of any length.
% We'd like some sort of *local* frequency description. 

% One easy (but not always wonderful) way is to take fourier transforms
%  of little sections of the signal, using 'windowing'
% see Stft.m

% MATLAB's spectrogram command does this and shows you the amplitudes:
subplot(1,3,3);
spectrogram(chirp)

% Audio example 1 [low-quality time stretching]
% see pvoc_demo.m

% Audio example 2 [low-quality denoising]
% see denoise_demo.m

%% Related tools

% Discrete cosine transform - very closely related to the DFT
help dct

% Wavelets are a much better time-frequency tool for many approaches
%  see MATLAB's wavelet toolbox
%  ACM 126

waveinfo

%% Cell Arrays

% You'll need these for the homework so we'll briefly cover them now
% These are very much like lists in Mathematica, which store collections
% of other pieces of data, including more lists.
% However, there is no need to deal with nested lists for multidimensional
% cell arrays.

cell(3,4) % an empty cell array

% cell arrays are defined like matrices, but with curly braces:
cellArray = {'string', [ 1 2 3], @(x) x^2; 1, 2 , 'three'}

% You can get and set with both parentheses () and curly brace {}

% The difference is that parenthesis give subarrays, and curly braces give
% the data stored in entries of subarrays

cellArray(1:2,1:2)
cellArray(1:2,1:2) = {1,2; 'three', @(x) 4*x}
cellArray(1,2) = 'x' % error
cellArray(1,2) = {'x'}

cellArray{1,2} = 'x'
cellArray{1,2} = {'x'} % not an error, but now we have nested arrays!
cellArray{1,2} = {'x','y'}
cellArray{1,2}
class(cellArray{1,2})

% this returns a "comma separated list", which we've actually seen
%  before - it's what a function returns!
cellArray{1:2,1:2}
[a,b,c,d] = cellArray{1:2,1:2}

%% Dirty Tricks

% In Mathematica I had functions like Part, but in MATLAB you have to write
% them yourself to use functional programming tricks

part = @(A,r,c) A(r,c);

% which allows one to include indexing operations in anonymous functions
getTwoSmallestEntries = @(A) part(sort(A(:)),1:2,1)

getTwoSmallestEntries([ 34 2 23 23 2; 3 32 5 52 -2])

% Inside of a function, another  (and more readable) option
%   is to include a nested function

