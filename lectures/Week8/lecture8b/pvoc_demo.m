clear all; close; clc

%% Load
[x,fs] = wavread('voice');

%%
soundsc(x,fs)

%% Process
% time stretching factor
r = 0.1  %time stretching factor

% window size (samples)
win = 512;

% overlap
overlap = 4;

% FFT size (usually the same as window size)
Nfft = 512;

% Samples between fft windows
hop = Nfft/overlap;

% Compute ffts
X = Stft(x,win,overlap);

% Times to resynthesize
[rows, cols] = size(X);
t = 1:r:cols; %time vector (can be non-linear set of time instances)

% Crude interpolation
[X2,iF] = pvinterp(X, t, hop);

% Resynthesis
x2 = IStft(X2,overlap);

%% Play
soundsc(x2,fs)


