close all; clear; clc

%% Load
[x,fs] = wavread('Noisy_ADA');

%% Play 
soundsc(x,fs);

%% Visualize
% window size (samples)
win = 1024;

figure
subplot(1,2,1);
spectrogram(x,win)
view(-90,90);set(gca,'ydir','reverse'); % hack to swap x and y
title('raw');
colorbar
shg

%% Process

% overlap
overlap = 2;

% FFT size (usually the same as window size)
Nfft = 512;

% Samples between fft windows
hop = Nfft/overlap;

% Compute ffts
X = Stft(x,win,overlap);

% Threshold
Xp = X;
Xp(abs(X) < 0.13) = 0;

% Resynthesize
x2 = IStft(Xp);

%% Play and Visualize


subplot(1,2,2);
spectrogram(x2,win)
view(-90,90);set(gca,'ydir','reverse'); % hack to swap x and y
title('processed');
colorbar
shg

%% Compare 1
soundsc(x,fs)

%% Compare 2
soundsc(x2,fs)
