function [] = golGUI(m,n)
% creates a GUI to edit and step through a game of life
%
% Allows the user to toggle individual cells, step by a given
%  number of iterations, clear, randomize, change mode,..
%
% Also allows a 'play' mode using timer

if(nargin < 2) n = 20; end
if(nargin < 1) m = 20; end

% the Game of Life
gol = GameOfLifeInteractive(m,n);
gol.randomize(0.5);


% Timer to auto-step
t = timer('TimerFcn',@takeStep_Callback, 'Period', 1.0,'ExecutionMode','fixedRate');

%  Create and then hide the GUI as it is being constructed.
f = figure('Visible','off','Position',[360,500,450,285]);

% Kill all timers on closing the figure

% Controls
hStep =  uicontrol('Style','pushbutton','String','Step',...
    'Position',[315,220,70,25],...
    'Callback',{@takeStep_Callback});
hClear  =  uicontrol('Style','pushbutton','String','Clear',...
    'Position',[315,180,70,25],...
    'Callback',{@clear_Callback});
hRand =  uicontrol('Style','pushbutton','String','Randomize',...
    'Position',[315,140,70,25],...
    'Callback',{@randomize_Callback});
hAutostep =  uicontrol('Style','checkbox','String','Auto',...
    'Position',[315,120,70,25],...
    'Callback',{@toggleAuto_Callback});
hpopup = uicontrol('Style','popupmenu',...
          'String',{'Slow','Medium','Fast'},...
          'Position',[300,90,100,25],...
          'Callback',{@autoSpeed_Callback});


% Canvas
ha = axes('Units','Pixels','Position',[50,60,200,185]);
%set(ha,'ButtonDownFcn',@axesclick_Callback);

% Refresh function
    function [] = refresh()
        spy(gol.cellData); axis xy
        set(ha,'ButtonDownFcn',@axesclick_Callback);
    end

% Callback functions (accept two arguments)
    function [] = takeStep_Callback(~,~)
        gol.takeStep;
        refresh();
    end

    function [] = clear_Callback(~,~)
        gol.reset;
        refresh();
    end

    function [] = randomize_Callback(~,~)
        gol.randomize(0.5);
        refresh();
    end

    function [] = axesclick_Callback(~,~)
        coordinates = get(ha,'CurrentPoint');
        coordinates = coordinates(1,1:2);
        gol.toggleCell( round(coordinates(2)), round(coordinates(1)) );
        refresh();
    end

    function [] = toggleAuto_Callback(~,~)
        if(strcmp(t.Running,'off'))
            start(t);
        else
            stop(t);
        end
    end

    function [] = autoSpeed_Callback(source,~)
         str = get(source, 'String');
         val = get(source,'Value');
         running = strcmp(get(t,'Running'),'on');
         if(running)
            stop(t); 
         end
         switch str{val};
         case 'Fast' 
            set(t,'Period',0.01);
         case 'Medium' 
             set(t,'Period',0.4);
         case 'Slow' 
             set(t,'Period',1.0);
         end
         if(running)
           start(t); 
         end
    end


% Show
% Change units to normalized so components resize
% automatically.
set([f,ha,hStep,hClear,hRand,hAutostep],'Units','normalized');
set(f,'Name','Game Of Life')
movegui(f,'center')
refresh();
set(f,'Visible','on'); % or shg

end