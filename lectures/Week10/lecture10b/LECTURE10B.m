%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 10                                                        %
%     Lecture 10b : MATLAB Odds and Ends                                  %
%                   GUIs, Parallel Computations, Compiled Functions, etc. %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Building GUIs in MATLAB

% Simple GUI tools for axes
help ginput
help dragrect

%% GUI Builder

% The GUI builder can be used interactively, and can generate
%  m-files for you. 
guide

% It can also be used programmatically, which we'll focus on here.
%  The idea is simple - add controls to a figure!

uicontrol

timer

% timer example
t = timer('TimerFcn',@(~,~) disp('tick!!'), 'Period', 1.0,...
    'ExecutionMode','fixedRate');
start(t)
stop(t)

% (Note the existence of timerfind)
timerfind
clear t
timerfind
t = timerfind
delete(t)
clear t
timerfind

% Example = MathWorks demo simple_gui2.m
simple_gui2

% Example - golGUI.m
golGUI
golGUI(70,111)

%% MEX files (compiled functions)

% One of MATLAB's most commonly-cited drawbacks is that it can be 
%  unexpectedly slow, compared to compiled code. For this reason,
%  you can compile your own fast code to use in MATLAB!

% This course doesn't suppose knowledge of C or any other language,
%   so we'll just show how to compile existing code.

mex -setup
mex -v kbdwin.c %v for verbose

% See config
mex.getCompilerConfigurations

% Make your own options file:
% 1. copy a base one 

% 2. edit (here I changed O2 to O3)

% 3. 'force' mex to use your new file 
% (or place in the correct place and choose it with mex -setup)
mex -f mexoptsNew.sh -v kbdwin.c

%% Parallel computation

% A huge and quickly-developing field. See ACM 114 for more.

% There are many models of parallel computing, depending on 
%   whether instructions and/or memory are shared

% We'll be focussing on "Single Program, Multiple Data" models - that is,
%   We'd like to run the same code on different pieces of data, at the same
%   time.

% We'll also be focusing on things you can do on a single multicore
% machine. Once multiple machines are involved, things become more
% complicated.


%% Easy Ways to Parallelize in MATLAB

%% Don't worry about it
% First off, realize that an increasing number of functions, like
%  fft, mldivide (backslash), eig, svd, sort, etc are already multithreaded,
%  so on a multicore machine you are already getting some parallelism.

%% Open a pool of 'workers' with matlabpool
%
%  These are in some sense multiple copies of MATLAB running at once
%
%  See the lower right hand side of the GUI to see how many workers
%    you have (4 on my machine since I have 4 cores).
matlabpool 

% Sometimes this is just 'free speed'! However, this takes a long time, 
%  to start, which perhaps explains why this isn't the default
%  behavior for MATLAB (yet). 

%% Use a function which can take advantage 
% Many steps in optimization procedures can be done in parallel
% Here we borrow an example provided by MathWorks, from
% http://www.mathworks.com/products/optimization/examples.html?file=/products/demos/shipping/optim/optimparfor.html#6
close all; clear; clc

% This example performs a lot of useless computation
%  to simulate expensive yet trivially parallelized objective and 
%  constraint functions
type expensive_objfun

type expensive_confun

%% Serial
startPoint = [1 -2 0 5];
options = optimset('Display','iter','Algorithm','active-set','UseParallel','never');
startTime = tic;
fmincon(@expensive_objfun,startPoint,[],[],[],[],[],[],@expensive_confun,options);
time_fmincon_sequential = toc(startTime);
fprintf('Serial FMINCON optimization takes %g seconds.\n',time_fmincon_sequential);

%% Parallel
options = optimset(options,'UseParallel','always');
startTime = tic;
fmincon(@expensive_objfun,startPoint,[],[],[],[],[],[],@expensive_confun,options);
time_fmincon_parallel = toc(startTime);
fprintf('Parallel FMINCON optimization takes %g seconds.\n',time_fmincon_parallel);


%% Parfor

% You can use parfor instead of for if
%   - you have a matlabpool open
%   - you don't care what order the iterations happen in
%   - iterations are over a set of monotically increasing integers
%   - you use a restricted set of statements in the loop body

% You cannot
%  - nest parfor loops
%  - use break or continue statements
%  - use global or persistent variables (which we haven't covered)
%  - use char type data

%% An 'embarrassingly parallel' problem 
% - do the same thing many times, in any order

% Data to work with
n = 100;
m = 5;
s = 100; %with s = 10, overhead actually makes parfor slower

%% for
dataF = NaN(m,n);
tic
for i=1:n
   dataF(:,i) = eigs(rand(s),5);
end
toc

%% parfor (matlabpool needs to be open)
dataF = NaN(m,n);
tic
parfor i=1:n
   dataF(:,i) = eigs(rand(s),5);
end
toc

%% spmd ["Single Program, Multiple Data"]
% Another way to run programs in parallel, letting you work
%  on several copies of MATLAB at the same time

% labindex and numlabs tell you how many processes there are
close all; clear; clc;
n = 1999;
AA = NaN(n);
tic
AA = [ 1*ones(n), 2*ones(n); 3*ones(n) 4*ones(n)];
toc

tic
spmd
  B = labindex*ones(n);
end
BB = [B{1},B{2}; B{3},B{4}];
toc

% The interesting thing here is what B is
class(B)

help Composite

% You can do this sort of thing interactively with pmode
matlabpool close
pmode start 4 % slow

% Try basic things like 
%x = labindex, 
%pmode lab2client x 1
%y = 3; pmode client2lab y 1:4
pmode quit

%% Wrap Up


%% Pro/Con Table for MATLAB vs. Mathematica vs. Other Options

%{

       MATLAB  Pro:
            Great Documentation
            Widespread Adoption
            Easy to use language
            Nice Editor and Debugger
            Good Numerical Linear Algebra tools
            Easy Graphics and Plotting
            Good for Procedural Programming
            ...
            
       MATLAB Con: 
            $$$ 
            Closed Source 
            Toolbox Availability  
            Somewhat inconsistent syntax 
            Not Great for Functional Programming    
            ...

       MATHEMATICA Pro:
            Great Documentation
            Precise language
            Self-contained (no toolboxes)
            Excellent Plotting Tools
            Worldclass Symbolic Tools
            ...
              

       MATHEMATICA Con: (and alternatives beyond MATLAB)
            $$$$$ 
            Closed Source
            Hard to debug
            Steep Learning Curve
            ...
   
%}

% MATLAB vs. Mathematica vs. Something else
% ..
%{
Which of the following tasks would you do in which environment?
% Solve an ODE {Mathematica}
% Produce a plot for a paper {Mathematica}
% Process a massive amount of data {MATLAB}
% Check a calculation you did on paper {Mathematica for symbolic, MATLAB
for lin algebra}
% Solve a PDE {MATLAB?}
% Write a tic-tac-toe game {BOTH }
% Prototype a numerical method { MATLAB , Mathematica at First}
% Write a library {MATLAB }
 
%}