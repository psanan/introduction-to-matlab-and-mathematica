classdef GameOfLifeInteractive < GameOfLife
    % A Game of Life with methods to change cell data
    
    methods %(public)
        
        % Constructor (calls base class constructor)
        function gol = GameOfLifeInteractive(m,n)
            gol = gol@GameOfLife(m,n);
        end
                
        function [] = reset(obj)
            % Set all cells to zero
            obj.cellData(:) = 0;
        end
        
        function [] = toggleCell(obj,x,y)
            % function which toggles the state of a given cell
            [m,n] = size(obj.cellData);
            if( x<1 || y<1 || x>n || y>m)
                error('invalid cell index');
            end
            obj.cellData(x,y) = ~obj.cellData(x,y);
        end
        
    end
end