classdef GameOfLife < handle
    % A class which describes a game of life
    
    % Properties (much like fields in a struct)
    properties
        
        % A logical array of living/dead cells
        cellData = false(20)
        
        % An integer
        iteration = 0
        
        % 'periodic', 'grow', or 'kill'
        boundaryMode = 'periodic'
        
        % Maximum dimension to grow to
        maxDim = Inf;
        
    end
    
    % Functions
    methods
        
        % Constructor
        function gol = GameOfLife(m,n)
            gol.cellData = false(m,n);
        end
        
        function [] = randomize(obj,c)
            % Set a fraction c of entries to 1
            obj.cellData = rand(size(obj.cellData))<c;
        end
        
        function [] = takeStep(obj)
            % One iteration of the game of life
            obj.gol_step();
            obj.iteration = obj.iteration + 1;
        end
        
        
    end
    
    % Private methods (not callable outside of the class)
    methods(Access=private)
        
        function [] = gol_step(obj)
            %GOL_STEP Step in periodic game of life
            %   X is a matrix, in which non-zero values are 'alive' and 0's are 'dead'
            
            X = obj.cellData;
            
            switch obj.boundaryMode
                case 'periodic',
                    % Pad input periodically
                    [m,n] = size(X);
                    Xp = [X(m,[n 1:n 1]);
                        X(:,n), X, X(:,1);
                        X(1,[n 1:n 1])];
                    
                case 'kill',
                    % Pad input with zeros
                    [m,n] = size(X);
                    Xp = padarray(X,[1 1],0);
                    
                otherwise,
                    error('unknown boundary mode');
            end
            % Compute count of living neighbors
            r = 2:m+1;
            c = 2:n+1;
            
            N = (Xp(r-1,c+1)~=0) + (Xp(r+1,c)~=0) + (Xp(r+1,c+1)~=0) + (Xp(r,c+1)~=0) +...
                (Xp(r,c-1)~=0) + (Xp(r+1,c-1)~=0) + (Xp(r-1,c-1)~=0) + (Xp(r-1,c)~=0);
            
            
            % New live cells are all cells with 3 live neighbors,
            %  and live cells with two live neighbors
            obj.cellData = ((N==3)) | ((X==1) .* (N==2));
            
        end
        
        
    end
    
    
end