classdef ClassWithPrivateData
    properties
       publicData 
    end
   
    properties(GetAccess=private)
       writeOnly 
    end
    
    properties(SetAccess=private)
       readOnly = 'dontChangeMe'
    end
    
    properties(SetAccess=private, GetAccess=private)
       privateData = 'secrets'
    end
    
    methods(Static)
        function val = getPrivateData(obj)
           val = obj.privateData; 
        end
    end
end