classdef GetSetableClass 
   properties 
      data1
   end
   
   methods
       
       function  obj = set.data1(obj,value)
           if(value <= 0)
              error('data 1 must be positive'); 
           end
           obj.data1 = value;
       end
       
       function value = get.data1(obj)
           value = obj.data1;
           if(value > 100)
              disp('Warning, data1 > 100!'); 
           end
       end
       
   end
    
    
end