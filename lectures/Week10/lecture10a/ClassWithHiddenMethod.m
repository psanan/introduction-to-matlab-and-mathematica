classdef ClassWithHiddenMethod
    
    methods(Hidden)
        function [] = easterEgg(obj) % this method should be static,
                                     % since it doesn't depend on the state of the object! 
           fprintf('You found me!!\n'); 
        end
        
    end
    
end