classdef ClassWithStaticMethod
    
    properties
       value 
    end
    
   methods (Static)
       function obj = combine(obj1,obj2)
          obj.value = mean([obj1.value obj2.value]); 
       end
   end
    
end