%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 10                                                        %
%     Lecture 10a : Object-Oriented features                              %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Object-oriented programming in MATLAB
% Review of the VideoWriter class
help VideoWriter

%%  Example - The Game of Life
% The rules are simple. Maintain a grid of 1's and 0's. Each iteration
% a 1 remains a 1 if it has 2 or 3 neighboring 1's, a 0 becomes a 1 if it
% has (exactly) two neighboring 1's, and otherwise everything becomes
% or remains 0.
%
% The 'glider' is an interesting shape which moves!
vr = VideoWriter('glider.avi');
vr.set('Quality',100);
vr.set('FrameRate',5);
vr.open;
A = zeros(10);
A(4,4:6) = 1;
A(3,6) = 1;
A(2,5) = 1;
for i=1:100
  A = gol_periodic_step(A);
  spy(A); title(sprintf('step %d',i));
  vr.writeVideo(getframe); 
end

vr.close;
clear vr

% Play externally (bash command for my OS X system)
!open glider.avi

%% Another G.O.L. Example
vr = VideoWriter('rand.avi');
vr.set('Quality',100);
vr.set('FrameRate',15);
vr.open;
A = rand(500,777) > 0.5; 
figure('Position',[0 0 800 800]);
for i=1:300
  A = gol_periodic_step(A);
  spy(A); title(sprintf('step %d',i));
  vr.writeVideo(getframe); 
end
vr.close;
!open rand.avi
clear vr

%% Why is this a useful thing?
%  - we only need to know about the "interface" of VideoWriter
methods VideoWriter

% Videowrite keeps track of all the data we need, as well
%   as useful methods
help VideoWriter/open

% in this case the interface includes "get" and "set" commands
%   so we need to know about the data stored inside
properties VideoWriter

% - what VideoWriter is doing internally doesn't matter to us, the user,
%     and it if the interface doesn't change, we won't know or care

% Let's take a look inside the VideoWrite class definition
%   Good thing we don't have to understand this all! 
edit VideoWriter

% Properties of various kinds (Dependent, transient, hidden,
% Access=private, etc.)

% Methods of various kinds . The first one is a 'constructor'
% Types of methods - static, hidden, Access=private, etc

%% Classes and Objects - syntax

% Defining classes in MATLAB
%{
 
classdef (Attribute=value, Attribute=value,...) className < baseClass1 &
   baseClass2 & ...

...

end

%}

% We already saw one way to define a class in a single file
%   see GameOfLife.m

% One can also define a class with a folder of files
%  This lets you put functions in separate files
s = SimpleClass
s.data1= 12345
s.func1

% Getting info on objects and classes
methods(SimpleClass)
methods(s)
properties(SimpleClass)
properties(s)

% Note that these might be different, since objects 
%  can in some cases have properties added to them! See the VideoWriter
%  class for example, which inherints from dynamicprops

% Creating objects
%   Call constructors, as described below
s = SimpleClass()
s = SimpleClass %the same
c = ClassWithConstructor(444)

%%  Handle vs Value classes

% Value classes are familiar - they are ints, strings, matrices, etc.
% An integer is a value class - its identity changes if its value chnges
b = 2;
b = b+1; % b + 1 returns a new integer 
b++ % doesn't exist in MATLAB! 

% Handle classes are things which we'd like to 'modify in place'. That is,
%  they have an identity separate from their value.
%  the VideoWriter class we used last time is an example
vr = VideoWriter('dummy');
vr.set('FrameRate',24); % still the same object

% Value classes are the default. Handle classes must inherit from the
%  handle class

% See ValueClass.m and HandleClass.m
vc = ValueClass(); vc.a = 1; vc.b =2;
vc
vc.swap
vc % doesn't work!
vcSwapped = vc.swap2

hc = HandleClass(); hc.a = 1; hc.b = 2;
hc
hc.swap
hc % works since hc is a handle to a mutable object

%% Destroying handle objects with clear and delete
obj1 = GameOfLife(10,10)
obj2 = GameOfLife(10,10)
obj1copy = obj1
obj2copy = obj2
clear obj1
obj2.delete % or delete(obj2)

obj1copy % data is fine!

obj2copy %data is gone!


% clear doesn't destroy an object if some variable 'points' to it
clear obj1copy % last reference - this deletes the data obj1 and obj1copy pointed to!

% Value objects only exist one at a time, so this is not an issue


%% Methods

% Methods must always accept as their first argument an object
%  of the class they are a member of. 

% If your class is a value class, methods must also return an
%  object of the class.
% See ValueClass.m
m = ValueClass; m.a = 1; m.b = 2;
mSwapped = m.swap2() %same as swap2(m)


%% Constructors 
% A constructor is a function for making new objects
% It returns an object of the class, and has the name of the class
% Only one constructor may be defined per class
c = ClassWithConstructor(33)

% Initializers

% As an alternative to using a constructor, you can set default values
%   for properties. 

c = ClassWithConstructor(3333) %also uses initializers


%% Static Methods
%  are ones which can be called 
%  without an instance of the class. These are often 'helpers' 
%  which would never be used outside the context of the class
s1 = ClassWithStaticMethod; s1.value = 1;
s1
s2 = ClassWithStaticMethod; s2.value = 2;
s2
s3 = ClassWithStaticMethod.combine(s1,s2)


%% Hidden Methods
% Hidden methods do not show up when the user lists the methods.
%  Good for simplfying the interface, but sorta sneaky..
c = ClassWithHiddenMethod;
methods(c)
c.easterEgg

%% Private Methods
p = ClassWithPrivateMethod;
p.publicPrint
p.privatePrint %error

%% Get and Set Methods
h = GetSetableClass
%h.data1 = -1 %throws error
h.data1 = 1000

 %prints warning
data = h.data1 

%% Property Types

% Largely the same as method types, and also set with attributes
%  Important important ones are SetAccess=private, GetAccess=private
g = ClassWithPrivateData
g.writeOnly = 'doesntShowUp'


%% Demo Application

% Let's try and use object-oriented programming to work with a 'game of
% life'. 
%
% See GameOfLife.m


%% Play with our new class
gol = GameOfLife(20,20);
gol.randomize(0.5);
for i=1:1000
   spy(gol.cellData); shg
   gol.takeStep;
end
clear gol

%%
gol = GameOfLife(200,200);
gol.boundaryMode='kill';
gol.randomize(0.5);
for i=1:10000
   spy(gol.cellData); shg
   gol.takeStep;
end
clear gol

%% Inheritance
% Classes can be based on other classes

% Syntax : classdef derivedClass < baseClass1 [& baseClass2 [ &  baseClass 3...] ] 
%   See GameOfLifeMemory which inherits from handle
%   See VideoWriter which inherits from dynamicprops AND hgetset
%     (these both in turn are based on handle)


% Example: GameOfLifeMemory
%  We define a new class that remembers its previous 2 states, so in 
%    particular it can tell if it has stagnated
%
% See GameOfLifeMemory.m

% Calling base class methods
%   The syntax method@baseClass(args) calls a method from the base class
%   see two examples in GameOfLifeMemory. Otherwise, if a derived class
%   defines a method with the same name, this is the one that is called

% Virtual Methods
%  Some classes are 'abstract', which means that they are intended
%  To be use only as base classes. These define the 'Abstract' Attribute
%  handle is an example

%% Play with our new class
gol = GameOfLifeMemory(20,20);
gol.randomize(0.5);
for i=1:1000
   spy(gol.cellData); title(sprintf('iteration %d',gol.iteration));shg
   gol.takeStep;
   if(gol.stagnated())
      fprintf('stagnated after %d iterations!\n',gol.iteration);
      break; 
   end
end
clear gol

%% Play with our new class some more
gol = GameOfLifeMemory(200,150);
gol.boundaryMode = 'periodic';
gol.randomize(0.05);
figure('Position',[0 0 800 800]);
for i=1:10000
   spy(gol.cellData); title(sprintf('iteration %d',gol.iteration));shg
   gol.takeStep;
   if(gol.stagnated())
      fprintf('stagnated after %d iterations!\n',gol.iteration);
      break; 
   end
end
clear gol

%% Polymorphism
% The same function does different things depending on what kind of
%  object it's called on

% A familiar example:
abs(1)
abs(1 + 1i)

% Using it yourself:
%y = Yeller %error
y1 = LoudYeller
y2 = LouderYeller
yellers = {y1,y2}
for i=1:2
   yellers{i}.yell 
end

%% More Object-Oriented Programming (OOP) features , ideas, and buzzwords [If time]
% 'What' separeted from 'How' [Decoupling]
% Interfaces and Implementations
% Data Hiding
% Encapsulation
% ..



