classdef LoudYeller < Yeller
    
   methods
       function [] = yell(obj)
          fprintf('%s !\n',obj.phrase); 
       end
       
   end
end