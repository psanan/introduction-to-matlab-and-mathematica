classdef Yeller < handle
    
   properties
      phrase = 'aaaaaagh' 
   end
    
   methods(Abstract)
       yell(obj)
   end
end