classdef ClassWithPrivateMethod
    
    
    methods
        function [] = publicPrint(obj)
           disp('Call from outside'); 
           obj.privatePrint(); 
        end
    end
    
    methods(Access=private)
        function [] = privatePrint(obj) % this method should be static,
                                        % since it doesn't depend on the state of the object! 
           disp('The Caller is IN the Building!'); 
        end
    end
    
end