classdef SimpleClass
   properties
      data1
      data2
   end
   
   methods %attributes if required
      % include signatures (not required unless attributes used)
      [] = func1(obj); %defined in another file
       
   end
    
end