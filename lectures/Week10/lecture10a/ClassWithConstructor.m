classdef ClassWithConstructor
    
   properties
      data
      moreData=3
   end
   
   methods

       function obj = ClassWithConstructor(value)
          obj.data = value; 
       end
       
   end
    
end