classdef HandleClass < handle
    properties
       a
       b
    end
    
    methods
       
        % Now this does work
        function [] = swap(obj)
           tmp = obj.b;
           obj.b = obj.a;
           obj.a = tmp;
        end
        
    end
end