classdef GameOfLifeMemory < GameOfLife
    
   properties 
        cellDataPrev
        cellDataPrev2
   end
   
   methods
       function gol = GameOfLifeMemory(m,n)
           
          % Call the base class's constructor 
          gol = gol@GameOfLife(m,n); 
          
          gol.cellDataPrev = NaN(m,n);
          gol.cellDataPrev2 = NaN(m,n);
       end
       
       function [] = takeStep(obj) 
           
          % Update memory 
          obj.cellDataPrev2 = obj.cellDataPrev;
          obj.cellDataPrev = obj.cellData;
          
          % Take a step as usual
          takeStep@GameOfLife(obj); 
       end
       
       function s = stagnated(obj)
           % Check for stagnation. Note that this won't detect 
           %   the  situation where the state becomes periodic with
           %   period greater than 2!
         
          s = all(obj.cellData(:) == obj.cellDataPrev(:)) || ...
              all(obj.cellData(:) == obj.cellDataPrev2(:)) ;
       end
       
   end
end