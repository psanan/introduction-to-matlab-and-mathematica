function [ Xout] = gol_periodic_step( X)
%GOL_PERIODIC_STEP Step in periodic game of life
%   X is a matrix, in which non-zero values are 'alive' and 0's are 'dead'


% Pad input periodically
[m,n] = size(X);
Xp = [X(m,[n 1:n 1]); 
        X(:,n), X, X(:,1);
        X(1,[n 1:n 1])];


% Compute count of living neighbors
r = 2:m+1;
c = 2:n+1;
N = (Xp(r-1,c+1)~=0) + (Xp(r+1,c)~=0) + (Xp(r+1,c+1)~=0) + (Xp(r,c+1)~=0) +...
    (Xp(r,c-1)~=0) + (Xp(r+1,c-1)~=0) + (Xp(r-1,c-1)~=0) + (Xp(r-1,c)~=0);


% New live cells are all cells with 3 live neighbors,
%  and live cells with two live neighbors
Xout = ((N==3)) | ((X==1) .* (N==2));
    
end

