classdef ValueClass 
    properties
       a
       b
    end
    
    methods
       
        % This seems like it should work, but it doesn't!
        %  One fix is to base this class off of handle
        function [] = swap(obj)
           tmp = obj.b;
           obj.b = obj.a;
           obj.a = tmp;
        end
        
        % Another fix is to note that for value classes,
        %  changing the value changes the identity, so a *new* 
        %  object is returned
        function obj = swap2(obj)
             tmp = obj.b;
           obj.b = obj.a;
           obj.a = tmp;
        end
        
    end
end