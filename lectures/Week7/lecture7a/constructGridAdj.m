function A = constructGridAdj(n)
% Return an adjacency matrix for an n x n grid
%  as a sparse matrix

% number nodes by rows, and note that A has entries on 4 diagonals
n2 = n*n;
dg = repmat([ones(n-1,1); 0],n,1);
dg2 = repmat([0; ones(n-1,1)],n,1);
dgs = [ones(n2,1), dg, dg2, ones(n2,1)];
A = spdiags(dgs,[-n -1 1 n],n2,n2);

end