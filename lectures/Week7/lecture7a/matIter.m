function [ out ] = matIter( A,x,k )
%MATITER Repeatedly applies a matrix,
%   matIter(A,x,k) computes (A^k)*x




 out = (A^k) * x;


end

