function P = spectralLayout(A)
% SPECTRALAYOUR produces an embedding of a graph
%
%  spectralLayout(A) returns a planar embedding of the graph with adjacency
%   matrix A, as an n by 2 matrix

% Perform some checks on the form of A
[m,n] = size(A);
if( m~=n) 
    error('Adjacency matrix must be square');
end

if (max(max(abs(A-A'))) > 100*eps)
   error('Adjacency matrix must be symmetric'); 
end

% Construct the Laplacian
L = A;
L(1:n+1:end) = 0;
L(1:n+1:end) = -sum(L);

% Compute the 2nd and 3rd bottom eigenvectors
[V,~] = eigs(L,3,'SM'); %the three eigenvalues with smallest magnitude

% Form our embedding
P = V(:,2:3);