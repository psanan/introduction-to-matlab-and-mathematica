function [ P ] = spectralLayout( A )
%SPECTRALLAYOUT compute an embedding for the graph with adjacency matrix A

[m,n] = size(A);

% Check A
if (m ~= n)
   error('Adjacency matrix must be square!'); 
end

if(norm(A-A') > 100*eps)
   error('Adjacency matrix must be symmetric'); 
end


%% Generate Graph Laplacian
L = A;
L(1:n+1:end) = 0;
L(1:n+1:end) = -sum(A);

%% Compute 2nd and 3rd bottom eigenvectors
[V,~] = eigs(L,3,'SM');

% Return points
P = V(:,2:3);

end

