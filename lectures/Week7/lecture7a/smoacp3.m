function [P, sigma] = smoacp3(A, epsilon, PGuess)
%SMOACP finds a layout for a graph that as much as possible is an isometry
%from the graph to the plane (local minimum of the stress function).
%
% [P, SIGMA] = SMOACP(A, EPSILON) A is the weighted adjacency matrix for
% the graph, and EPSILON is a stopping tolerance. Once the relative
% decrease in the stress drops below EPSILON, terminate. P is the n-by-2
% matrix of vertex coordinates and SIGMA is the stress of P
%
% [P, SIGMA] = SMOACP(A, EPSILON, PGUESS) uses PGUESS as the initial guess
% for P
%
% See Also SPECTRALGRAPHLAYOUT
%

% Check the form of A
[n,m] = size(A);
if(n~=m)
   error('A must be square'); 
end

if (max(max(abs(A-A'))) > 100*eps)
   error('Adjacency matrix must be symmetric'); 
end

if(any(A ~= 0 & A ~= 1))
   disp('Warning, not using weighting information'); 
end

% Use a random initial guess if none is provided
if(nargin < 3) PGuess = rand(n,2); end

% compute all-pairs distances
D = floydwarshall(A);

% compute L^G
LG = -D.^(-2);
LG(1:n+1:end) = 0;
LG(1:n+1:end) = -sum(LG);
LGhat = LG(2:end,2:end);

% Main solver loop
t = 0;
err = inf;
P = PGuess; %replace with random embedding
sigma = stress(P,D);

maxIter = 1000;

while(err > epsilon)

    %fprintf('iter %d : stress = %g\n',t,sigma);
    if (t > maxIter)
    %   fprintf(' max iterations (%d) reached', maxIter); 
       break; 
    end
    
    t = t+1;
   
    sigmaOld = sigma;
    
    LP = formLP(P,D);
    
    LPP = LP * P;
    LPPhat = LPP(2:end,:);
    
    Phat = LGhat \ LPPhat;
    P = [0, 0; Phat];
    
    sigma = stress(P,D);
    err = (sigmaOld - sigma)/sigmaOld;
        
end

end

function sigma = stress(P,D)
    sigma = ((dist(P')-D) ./ D).^2; 
    sigma(~isfinite(sigma)) = 0;
    sigma = sum(sum(tril(sigma)));
end

function [ LP ] = formLP(P,D)

    [n,m] = size(D);
% if(n ~= m)
%    error('D must be square'); 
% end
% if(~isequal(size(P),[n 2]))
%    error('P must be n x 2'); 
% end

    LP = -1./(D.* dist(P'));
    LP(1:n+1:end) = 0;
    LP(1:n+1:end) = -sum(LP);

end

