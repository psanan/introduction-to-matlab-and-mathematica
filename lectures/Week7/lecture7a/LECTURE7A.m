%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 7                                                         %
%     Lecture 7a : Vectorization and small-scale development              %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Vectorization

% "Vectorize!" is one of the mantras of MATLAB programming. 

% In the strict sense, this means evaluating some operation
%  on multiple sets of input at once, as in
[1 2 3] + [ 5 6 7] % vectorized add

%  In MATLAB we use a slightly looser definition which includes
%   processing things as vectors and matrices, avoiding dealing
%   with the individual elements of these objects. 

    % generate a matrix of polynomial basis functions
    n = 5000;
    m = 1000;
    x = linspace(0,1,n);
    
    % Bad
    tic
    B = NaN(m,n);
    for i=1:m
        for j=1:n
            B(i,j) = x(j)^i;
        end
    end
    toc
    
    % Better
    tic
    B1 = repmat(x,m,1).^(repmat((1:m)',1,n));
    toc
    
    % Even better
    tic
    B2 = bsxfun(@power, x, (1:m)');
    toc

% Why?
% 1. Often (not always) easier to read and understand - 'higher level'
        
% 2. Easier to write

% 3. More efficient

% 4. Easier to optimize and parallelize

% How?

% 1. Element-wise operations and clever use of indexing

% 2. bsxfun 

% 3. (advanced) write your own code [More in week 10]

% 4. (advanced) parallelize [More in week 10]


% This lecture will proceed mostly as one long example. We'll get the chance
%  to see how to one would approach solving a multi-file problem in 
%  MATLAB and use some common tools that we've seen in past lectures

%% The Task
%{

Graph layout
    Say we are given a graph G, a set of vertices and a set of edges (pairs
    of these vertices). Often, we have some notion of edge strength, which
    is captured by a weighted graph, which assigns a number to each edge.

    Given a (possibly weighted) graph, an embedding is an assignment of
    each vertex to a point in some space, often the plane R^2. 

    How can we automatically produce a 'good' embedding? 

    1. 'springs' - we have an implementation of the 'SMOACP' algorithm

    2. spectral graph theory - we'll write this ourselves


    % We will go through the process of writing a small demo program
    % To compare these two methods, and will then see what we can
    % easily do to make it faster

%}

%% Step 0: Playing with Eigenvalues

% MATLAB makes it very easy to do preliminary experiments, especially
%  with linear algebra! 

% Let's say that we've heard that you can lay out graphs by understanding
% the spectrum of a graph.

% Why should the 'spectrum' of a graph matter? 

% We've already seen how a graph can be represented by a matrix, and how a matrix
% can be represented with a graph.

% If we apply this matrix to a vector, what are we doing? We're replacing
% each entry in the vector with a weighted combination of the other
% entries! 

A = [1 2; 3 4]
x = [10 20]

% replace x(1) with x(1) + 2*x(2)),
% replace x(2) with 3*x(1) + 4*x(2)
x = A*x


% What if we do this over and over again? Will the result depend on the
% initial vector? If we normalize in some reasonable way, will this
% iteration converge to something? 

% Let's see!

%(for simplicity, I'm going to work with symmetric matrices)
n = 2;
symmetrize = @(A) 0.5*(A + A');
A = symmetrize(2*rand(n)-1); %symmetric sparse random matrix

% let's write a function to repeatedly apply A to a random vector
% (see matIter.m)

x = 2*rand(n,1)-1;
matIter(A,x,3)

close all;
figure; hold on;
for i=1:20
   x = rand(2,1);
   A20x = matIter(A,x,20);
   plot([0 A20x(1)],[0 A20x(2)],'x');
end
axis equal

% We see they all lie on a line!

% We can explain this behavior directly:
%{

A = V D V' [ do this with full, eig ]

A^k = (VDV')(VDV')...(VDV') = VD^kV'

%}

[V,D] = eig(A)

% So now we know that the eigenvector associated with the largest eigenvalue
% governs the behavior here

% Let's consider the iteration above but now with a normalization each 
% time we apply A - that is, we now only care about the direction.

% see and test matIterNorm.m


A = rand(5); A = A+A';
[V,D] = eig(A)

matIterNorm(A,rand(5,1),10)

% what if we apply this to eigenvectors?

matIterNorm(A,V(:,1),10)

matIterNorm(A,V(:,1),100) % quiz : why didn't this work?

% How do we apply this to graph layout? 
%  Let's set the digonal of our adjacency matrix to be the negative of the
%  row sum. We now have a "discrete laplacian". 
% Note that applying this matrix to a a vector sends each coordinate to a
% weighted combination of its neighbors - if our vector is stationary, up
%  to scaling, under this procedure, we might have a useful layout!

% Now we note that eigenvectors, are stationary points of this
%  iteration (with the normalization), so eigenvectors of
%  the graph Laplacian are good ways to lay out the coordinates.

% It turns out that the eigenvectors corresponding to the smallest
%  eigenvalues are optimal. The smallest eigenvalue is 0, with
%  corresponding eigenvector ones(n,1) [why?] , so we choose the 
%  second and third eigenvectors.


%% Step 1: Setting up our main script

% One common approach to software development is the 'top-down' approach,
% wherein one sets up the 'shell' of the code first and then 'fills in the
% details' later. Let's do that here. I've created MAIN.m, but let's do it
% again 'live' an we'll see how different the results are. 

% We'll create a file called MAIN.m . This is just my own convention -  you
% could call it RUNME.m or anything else that makes it obvious that this is
% the starting point

% decide what we'd like the code to do
%  1. [ generate a graph ]
%  2. [ lay out with two methods ]
%  3. [ visualize the results side-by-side ]

% We'll work from the outline in, filling in more and more detail. We'll
%  write down the names of functions to perform longer subtasks

% Create functions to perform the subtasks
%   Soon we run into the issue that we don't know enough about how these
%   functions work to proceed

% Note that at this point we're more interested in making sure the
%  machinery which creates and draws the graphs is working, so we can
%  test our algorithms. Thus, let's just do a 'braindead' version
%  of the spectral layout, and implement it 'for real' later

% Now we should have running code, which does not produce complete results
%   because we haven't implemented the 'guts' of the spectral layout

%% Step 2: Implementing / Adapting the algorithms

% Now we switch to a 'bottom up ' approach. We know what the basic tools
% are to accomplish the task, so we code those up and then assemble them
% into something which accomplishes the task. 

% The big downside of doing this is that it's often hard to test - our aim
% here is to be able to test things as we code them, catching bugs. 

%% Step 2a: Stress minimization

% Here we do a very common thing - we use someone else's code!
% see smoacp1.m

% Pros: A lot less work, a quick way to test things. All we have to do is
%  make sure we give the algorithm the arguments it needs, and use the
%  results correctly.

% Cons: You do not know all the assumptions being made, and may easily
% overlook subtle errors or inefficiecies. 

% Note here that you might not know what the tolerance is supposed to be.

%% Step 2b: Spectral graph layout
% Now let's fill in spectralLayout.m properly

% Again, I'll try and do this 'live', but there is a prepared
% spectralLayout.m

% The great thing about the way we're working is that I can more quickly
% test to see if my code is working. I don't have to code the 'hard part',
% write some wrapper code to plot it, forget what I was doing, and return 
% to fix  bugs.


%% Step 3: Putting everything together and documenting
% It's a good idea to include a 'README' file if you worry the person
%  opening the file might not know where to start

% Now at the end, we have code that accomplishes a task, is self-contained,
% and most importantly will be understandable in a few months when we want
% to come back to it!

% Once things are working, it's a good idea to go back and do a little bit
%  of cleanup - remove commented out junk, etc.

%% Step 4: Profile

% What we have is slow. 

% MATLAB makes it very easy to catch when you've done something very
% inefficient. 

% It's very important to note that inefficient, working code is better 
% better than, efficient, non-working code! The top-down approach
% can also apply to efficiency - first put everything in place, THEN
% see where optimization might be useful. Likewise, the bottom-up approach
% is not without uses - if you can easily get it right the first time,
% you might as well do so, but don't sweat it until you're sure it'll
% help. 

% Click run and time!

%what do we learn?

% pinv is the culprit!

%   Compared to this, the rest of the code is fast, so tweaking in other
%   places isn't a good use of ones time

% This highlights one of the downsides of using someone else's code - you
%   don't know what mistakes they might have made, and when you do it's
%   harder to remedy them

% Let's set a breakpoint and see what pinv is doing

% We note that we don't actually need pinv! Furthermore, this loop looks 
% suspicious - we should be able to vectorize it

% This also tells you something very important about MATLAB programming - 
%  We waste a lot of time here not because pinv is slow
%  but because we call it so many times - function calls are slow and 
%  unlike in other languages, there are not 'optimized away'

% We can do a lot better with this, and we know where to focus our efforts!
%  We do everything we can to vectorize - in particular, note the
%  existence of the 'dist' function which lets us get rid of the ugly loop. 

% See smoacp3.m, where we've made this and other changes

% NOW we see that the bottleneck is our terrible implementation of
% floyd-warshall. You can do better than this on your homework!

% Note that these two speedups (at least for the problem size we're using
% here) are on different scales - it isn't worth your effort to speed up
% the floyd-warshall code until you've dealt with the pinv problem
% [of course, in a class it's fun and educational - this advice applies
%    to using MATLAB in the service of a larger goal!] 