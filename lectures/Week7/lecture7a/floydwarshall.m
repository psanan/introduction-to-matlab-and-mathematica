% A poor (but correct) implementation of the Floyd-Warshall algorithm
function Dc = floydwarshall(A)
    [n,m] = size(A);
    Dp = A;
    for i=1:n
        for j=1:n
           if(A(i,j)==0)
               Dp(i,j) = Inf;
           end
        end
    end
    for i=1:n
        Dp(i,i) = 0; 
    end
    for k=1:n
        for i=1:n
            for j=1:n
                if i == j
                    continue;
                end
                Dc(i,j) = min(Dp(i,j), Dp(i,k) + Dp(k, j));
            end
        end
        Dp = Dc;
    end
end
    