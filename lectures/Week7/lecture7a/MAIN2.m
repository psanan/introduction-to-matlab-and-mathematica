% Script to compare two methods of graph layout

close all; clear; clc

%% Generate an adjacency matrix
n = 7;
A = constructGridAdj(n);

%% Generate Layouts
P_smoacp = smoacp3(A,1e-6);
P_spectral = spectralLayout(A);

%% Plot Layouts

figure; 
subplot(1,2,1);
gplot(A,P_smoacp);
title('smoacp');
axis square
subplot(1,2,2);
gplot(A,P_spectral);
title('spectral');
axis square