function [ out ] = matIterNorm( A,x,k )
%MATITER Repeatedly applies a matrix, normalizing the result
%   matIter(A,x,k) computes (A^k)*x

normalize = @(x) x/norm(x);
out = normalize(x);

for i=1:k
 out = normalize(A*out);
end

end

