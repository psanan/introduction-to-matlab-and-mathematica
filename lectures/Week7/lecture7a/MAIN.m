% Tests to compare spectral and stress-minimization-based approaches
%  to graph layout

close all; clear; clc

%% Generate an Adjacency Matrix
gridSize = 7;
A = constructGridAdj(gridSize);

%% Generate Layouts
%P_smoacp = smoacp1(A,1e-6);
P_smoacp = smoacp3(A,1e-6);
P_spectral = spectralLayout(A);

%% Plot Results
close all;
figure;
subplot(1,2,1);
gplot(A,P_smoacp);
axis equal;
title('SMOACP');

subplot(1,2,2);
gplot(A,P_spectral);
axis square;
title('Spectral Layout');

