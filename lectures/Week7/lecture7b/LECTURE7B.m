%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 7                                                         %
%     Lecture 7b : Optimization                                           %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Optimization problems

%{
A huge number of useful tasks can be accomplished by solving a problem of
this form:

Minimize f(x) for x in Omega
such that g_i(x) <=0  for i =1,...,m

This is called an (mathematical) optimization problem, or sometimes a 
 'program' [The use of the word 'program' here actually predates computer 
 programs - these problems were first considered in terms of 
 resource-allocation programs ]

x is an element of R^n, f is the objective function, the g_i are the
constraint functions, and Omega (a subset of R^n) is the domain.

As you might imagine, this is a very general class of problem, and
imposing more structure makes it easier to solve. Some things that make
it easier:

   m = 0 (the problem is 'unconstrained')
  
   the constraints come in pairs g_i = h(x), g_{i+1) = -h(x), so we actually
        have the 'equality constraint' h(x) = 0

   f and g_i are linear or quadratic 

   Omega is simple (all of R^n, an intersection of half-spaces, 
      a convex set,..)
    
   f and g are convex or concave (definition below)

A main goal in solving an optimization problem is to write it
    in the simplest way possible. A simple yet very important example:

    minimize log(x)^2 
    such that x > 0

is a nonlinear, constrained problem,
yet a simple change of variables y = log(x) lets us solve the
quadratic, unconstrained problem

    minimize y^2

This may seem absolutely trivial, but this kind of insight can make
a massive difference to the time it takes to solve a problem!

%}

%% Writing your own code

% It's easy to write your own simple algorithms in MATLAB, of course. We've
% already seen some toy examples with Newton's method. 

%% The Optimization Toolbox
% MATLAB comes with a useful set of optimization tools in the optimization
% toolbox (which is available with all of Caltech's MATLAB versions)

%% Root-finding and unconstrained minimization
% Finding zeros of functions is very related to minimization
%  indeed, we used a root-finding method already (Newton's method)
%  to minimize a function by finding a zero of its gradient

f = @(x) cos(x)+0.1*x;
ezplot(f); shg
fzero(f,0)
fsolve(f,0)

% As you can tell by the fact that you need an initial guess,
%  the algorithms only search locally and return local minima.
xmin0 = fsolve(f,0)
xmin1 = fsolve(f,1)
close all;
ezplot(f); hold on;
plot([xmin0 xmin1],f([xmin0 xmin1]),'rx','MarkerSize',25,'LineWidth',2); shg

% what's the difference between fzero and fsolve? 

% fzero looks for zeros, that is for an interval where the function 
%  changes sign
fzero(@(x) (x-1)^2,0)

% fsolve looks for approximate solutions to f(x) = 0,
%   which may exist even without a change in sign
fsolve( @(x) (x-1)^2,0) % gives a terrible answer!
opt = optimset('TolFun',1e-12,'TolX',1e-12);
fsolve(@(x) (x-1)^2,0,opt) % a little better..

% this example shows that it's important not to trust these methods
%  too much. They provide a lot of output
opt = optimset('TolFun',1e-12,'TolX',1e-12); % more in a minute
[xmin, fval, flag, output] = fsolve(@(x) (x-1)^2,0,opt) % a little better..


% output is a "struct", a collection of data similar to a "class" 
% [More week 10]
class(output)

% access fields with dots
outout.iterations
output.message

% Unconstrained minimization
f = @(x) 0.1*(x+1).^2 + cos(x) 
close all; ezplot(f); hold on;
[xmin0, fval0] = fminunc(f,0);
[xmin1, fval1] = fminunc(f,1);
plot([xmin0 xmin1],[fval0 fval1],'rx');

g = @(X) norm(X-1);
fminunc(g,[0.5 0 1]')


%% Optimset
% optimization functions have a lot of settings to adjust
% Some of these won't make much sense unless you understand how
% the algorithms involved work (and going into that is beyond the scope
%  of this class - see ACM113 and ACM106! )

% optimset encapsulates a set of optimization options
mySettings = optimset('TolX',1e-12,'TolFun',1e-12)

% We'll focus on Newton's method, since we have played around with a little
%  in this course

% Methods
% 'Method'

% Providing derivatives
% For newton's method, you would want to provide the gradient and Hessian

% Tolerances and Maximum iterations
% 'TolX' and 'TolFun'
% (as above)

% Monitoring
% 'Display' 
fminunc(@(x) cos(x) + x^4,3,optimset('Display','iter-detailed')) % more in doc fminunc


% use the 'PlotFcn' option
fminunc(@(x) cos(x) + x^4,3,optimset('PlotFcn',@optimplotstepsize)) % more in doc fminunc


%% 'Wrapping objective functions'

% It's often the case that the function you want to minimize
% has various parameters (extra arguments that you want to 'freeze')
f = @(x,param) x.^2 + param*(x-1).^2;

% f as written isn't in the form that fminunc expects, but we can 'wrap it'
paramVal = 0.3243;
f_wrapped = @(x) f(x,paramVal);

[xmin, fmin] = fminunc(f_wrapped,0)


%% Least Squares

% This will be useful for regression and data-fitting

A = rand(10,20);
B = rand(10,5);
b = rand(10,1);

[x,flag,relres] = lsqr(A,b) %doesn't work so well by default!

[x,flag,relres] = lsqr(B,b)

% The backslash operator also returns least squares solutions
%  but uses a direct method. [google 'QR decomposition' for more]
A\b
B\b

%% Constrained Minimization

% Linear program
%{
minimize f'* x 

s.t. A*x - b <= 0


Matlab breaks this up into inequality, equality, and bound constraints
  because it turns out that these can be dealt with differently

%}

help linprog

close all; clear; clc
f = [-5; -4; -6];
A =  [1 -1  1
      3  2  4
      3  2  0];
b = [20; 42; 30];
lb = zeros(3,1); %lower bounds

[x,fval,exitflag,output,lambda] = linprog(f,A,b,[],[],lb);


% Constrained least squares
%{
minimize ||Cx-d||_2 s.t Ax - b <= 0 

again, MATLAB breaks up the constraints into
 equality , inequality, and bound types

%}
C = [
    0.9501    0.7620    0.6153    0.4057
    0.2311    0.4564    0.7919    0.9354
    0.6068    0.0185    0.9218    0.9169
    0.4859    0.8214    0.7382    0.4102
    0.8912    0.4447    0.1762    0.8936];
d = [
    0.0578
    0.3528
    0.8131
    0.0098
    0.1388];
A =[ 
    0.2027    0.2721    0.7467    0.4659
    0.1987    0.1988    0.4450    0.4186
    0.6037    0.0152    0.9318    0.8462];
b =[
    0.5251
    0.2026
    0.6721];
lb = -0.1*ones(4,1);
ub = 2*ones(4,1);

[x,resnorm,residual,exitflag,output,lambda] = ...
                    lsqlin(C,d,A,b,[ ],[ ],lb,ub);

% Linearly constrained Quadratic program
%{

Minimize (1/2)*x'*H*x + f'*x

s.t. A*x - b <= 0

Otherwise very similar to linprog

%}

help quadprog %lots of options!

H = [1 -1; -1 2]; 
f = [-2; -6];
A = [1 1; -1 2; 2 1];
b = [2; 2; 3];
lb = zeros(2,1);
opts = optimoptions('quadprog','Algorithm','active-set','Display','off');
[x,fval,exitflag,output,lambda] = ...
   quadprog(H,f,A,b,[],[],lb,[],[],opts);

% Minimize a function of one variable over an interval 
close all; clear; clc
f = @(x) 4*cos(x) + (x-1)^2 + (x-2)^(2);
[xmin,fval] = fminbnd(f,0,1,-1);
ezplot(f); hold on;
plot(xmin,fval,'rx'); shg


% General constrained minimization (hard!)
%  We won't get into the generalities of this
%  This function can handle all of the previous examples and more
%  but many problems of this type are computationally intractable
help fmincon


%% CVX
%{

if A and B are vectors in R^n, a convex combination of A and B is an
expression of the form 

sA +(1-s)B
 
where s is a real number in [0,1]. The set of all convex
combinations of A and B is thus the line segment between A and B. 

A convex set is one which contains all convex combinations of all of its
poitns. That is, you can 'see' any point in the set from any other point.

A function f:R^n -> R defines a set (its 'epigraph') of all the points in
R^(n+1) with x_(n+1) >= f(x_1,...,x_n). That is, the set 'above' the
function. 

A function is called convex if its epigraph is a convex set. The key
property of convex functions is that _local_ minimizers are in fact
_global_ minimizers. 

A convex optimization problem is one in which the objective function, the
domain, and the constraints are all convex. 

We'll just touch on how to solve these problems with cvx, a package for
MATLAB. For (much) more, see
    - This (free, excellent) textbook: http://www.stanford.edu/~boyd/cvxbook/
    - The CVX manual: http://cvxr.com/cvx/cvx_usrguide.pdf
    - ACM 113 

This package is not always the (computationally) fastest way to solve a problem,
especially a large one, but it is extremely useful for small-to-medium 
convex programming, and way well be the fastest approach in terms of your 
own time. 

%}

%% Getting cvx running
% 1. Visit cvxr.com
% 2. Download from cvx-->download
% 3. unzip to /some/directory
% 4. Point MATLAB to /some/directory/cvx (cd /some/directory/cvx, or use the GUI)
% 5. run cvx_setup.m (just enter cvx_setup at the command line)
% 6. Take a look at the output (ignore warnings about Gurobi and MOSEK -
%                               these are commerical 3rd party solvers)


help cvx % useful!

%% How to use cvx

% one uses a 'cvx specification', wrapped in special commands
%  this lets you mix MATLAB commands with cvx-specific ones

% (If you get weird errors, the first thing to check is that cvx_setup
%    got called)
run ../cvx/cvx_setup %this is where I happend to put it

% or 
addpath ../cvx
cvx_setup

% save the path for next time (so you can just use cvx_setup
savepath

% -------  using cvx -------- %
%(MATLAB commands)

% cvx specification:
cvx_begin %quiet [suppresses output]
    % (MATLAB and cvx commands) 
cvx_end
% (More MATLAB commands)
% --------------------------- %


% Least squares example
close all; clear; clc
m = 16; n = 8;
A = rand(m,n);
b = rand(m,1);
cvx_begin % quiet
    variable x(n) %you need to declare variables before using them
    minimize(norm(A*x-b))
cvx_end
x %now holds the solution
cvx_status 
cvx_optval % the optimal value
who % cvx defines some other values

%% The 'Disciplined Convex Programming' Paradigm

% A set of rules for specifiying programs. If cvx accepts your input,
%  the program is convex! However, the inverse is not true - there are 
%  convex programs you can specify that cvx will not accept.

% in between cvx_begin and cvx_end you can specify variables, constraints, 
%   and objectives. When cvx_end is called, cvx tries to compute the
%   objective and converts the variables to matlab values

% variables
%  as in MATLAB, these can be real or complex, and can be scalar, vector,
%  matrix, or higher dimensional arrays. The syntax is
%  name(size1,size2,...) structure1 structure2 ...
% possible structures:
%{
banded(lb,ub) complex diagonal hankel hermitian lower_bidiagonal 
lower_hessenberg lower_triangular scaled_identity 
skew_symmetric symmetric toeplitz tridiagonal upper_bidiagonal 
upper_hankel upper_hessenberg upper_triangular
%}

% examples from the user manual:
%{
variable w(50) complex;
variable X(20,10); 
variable Y(50,50) symmetric;
variable Z(100,100) hermitian toeplitz;
%}

% quickly declare multiple real (unstructured) variables
%variables x y z v(100) m(10,10)

% Constraints 
%{
f(x) == 0 ;
f(x) >= 0;
f(x) <= 0;
f(x) > 0 ; %discouraged, not really supported

l <= x <= u %chaining

A >= 0 % vector/matrix inequalities interpreted elementwise (see 'sdp mode', though)

%}

% Sets/domains
%{
X == semidefinite(n)
X <In> semidefinite(n) 
help cvx/sets
%}

% Objective functions
%{
minimize
maximize % one minimizes a convex function and maximizes a convex one
%}

% There's a 3rd option, 'feasability' which determines if any x exists 
%   to satisfy the constraints in the domain - this is assumed if no
%   minimize/maximize statement appears

clear; close all; clc
n = 10;
cvx_begin
    variable X(n,n) symmetric;
    X == semidefinite(n);
    trace(X) <= 0;
    % minimize(0) % this does the same thing - minimize a constant
cvx_end
cvx_status % can we have a symmetric semidefinite matric with nonpositive 
           %  trace? (for n=10)


clear; close all; clc
n = 10;
cvx_begin
    variable X(n,n) symmetric;
    X == semidefinite(n);
    det_rootn(X) >= 1; 
    trace(X) <= 0;
    % minimize(0) % this does the same thing - minimize a constant
cvx_end
cvx_status %can we have a symmetric semidefinite matrix with det >= 1 
           % and nonpositive trace? (for n=10)

%% Supported Functions
% Listed in Appendix B of the cvx manual
help cvx/builtins
help cvx/functions

%% A Quick tour

% Here, let's demo some of the program that cvx is really good at

% For many more (more realistic) examples, see 
%   http://cvxr.com/cvx/examples/
web('http://cvxr.com/cvx/examples/') % MATLAB has its own web browser for some reason!

% Linear program
close all; clear; clc
n = 10;
f = rand(n,1);
A = rand(n)-1;
b = -rand(n,1);
cvx_begin
    variable x(n)
    A*x-b <=0
    x >= 0
    minimize(f'*x)
cvx_end


% (Linearly constrained) Quadratic program
close all; clear; clc
n = 10;
H = rand(n); H = H*H'; % make spsd
f = rand(n,1);
A = rand(n)-1;
b = -rand(n,1);
cvx_begin
    variable x(n)
    A*x-b <=0
    x >= 0
    minimize(0.5*x'*H*x + f'*x)
cvx_end

% Quadratically constrained) Quadratic program
close all; clear; clc
n = 10;
H = rand(n); H = H*H'; % make spsd
f = -rand(n,1);
Q = rand(n); Q = Q*Q'; % make spsd
g = -rand(n,1);
cvx_begin
    variable x(n)
    0.5*x'*Q*x+g'*x <=0
    x >= 0
    minimize(0.5*x'*H*x + f'*x)
cvx_end

% Second order Cone program (SOCP)
close all; clear; clc
n = 10;
H = rand(n); H = H*H'; % make spsd
f = rand(n,1);
A = rand(n);
b = rand(n,1);
B = 10*rand(n);
c = rand(n,1);
cvx_begin
    variable x(n)
    norm(A*x-b) <= B*x - c
    x >= 0
    minimize(0.5*x'*H*x + f'*x)
cvx_end

% In general, convex programming can 'crush' one more general class of convex
% programs, called semidefinite programs, and then things get trickier
% See cvx's 'SDP mode' for more.

