%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 7                                                         %
%     Lecture 9a : Data Analysis and Regression                           %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc;

%% Interpolation
% matlab offers interp1, interpft, spline, pchip to perform 1d
% interpolation; you can also use the plot tools
% Note: for all interpolations, the values in x must be monotonic
% (increasing or decreasing) - use the sort function.

x = [0 0.785 1.570 2.356 3.141 3.927 4.712 5.497 6.283];
y = [0 0.707 1.000 0.707 0.000 -0.707 -1.000 -0.707 -0.000];

% use plot tools
plot(x,y, 'o');

% Using interp1
xi = linspace(0,2*pi,50); % points to interpolate value at
yinearest = interp1(x,y,xi,'nearest'); % nearest neighbor interpolation
yilinear = interp1(x,y,xi,'linear');
yicubic = interp1(x,y,xi,'cubic');
yispline = interp1(x,y,xi,'spline');

subplot(2,2,1)
title('Nearest neighbor interpolation');
plot(x,y,'o',xi, yinearest);
subplot(2,2,2)
title('Linear interpolation');
plot(x,y,'o', xi, yilinear);
subplot(2,2,3)
title('Cubic interpolation');
plot(x,y,'o',xi,yicubic);
subplot(2,2,4)
title('Spline interpolation');
plot(x,y,'o',xi, yispline);

% can also have the interpolation returned (for any of the four 
% interpolation methods interp1 accepts)
pp = interp1(x,y,'nearest','pp');
yi = ppval(pp, xi); % evaluate the interpolation at the points xi
figure()
plot(xi, yi);

% A piecewise polynomial is somewhat similar to the InterpolatingFunction
%  construct we saw in Mathematica

% use interpft if you have periodic data: the x values should be equispaced
% inside a period of the function, y should be the corresponding values,
% and you pass in a number of equispaced samples that you want from that 
% period
x = x(1:end-1);
y = y(1:end-1);
yi = interpft(y,100);
dx = 2*pi/100;
xi = [0:dx:2*pi-dx];

% one would suspect that this is the most accurate interpolation method for
% this data set
plot(x,y, 'o', xi, yi, 'rx')

% We can do this ourselves using the methods from the last lecture - just
% add some zeros to your frequency data
Y = fft(y);
Ypadded = fftshift(padarray(fftshift(Y),[0 20],0));
lOld = length(Y);
lNew = length(Ypadded);
yInterp = (lNew/lOld)*real(ifft(Ypadded));
figure
xInterp = linspace(0,2*pi-1/lNew,lNew);
plot(x,y,'o',xInterp,yInterp,'ro');

%% Numerical Integration
% MATLAB can perform numerical approximation of definite integrals
% quad uses the adaptive Simpson's rule to evaluate 1d integrals
% quadl and quadgk are alternatives (that use Lobatto and Gaussian
% quadrature respectively) which supposedly can handle infinite intervals,
% but they can give you nonsense, so it's best to convert your infinite
% integrals to definite ones 

% calling syntax for quad is I = quad(fun, a, b, tol)
% recall fun is a function handle or anonymous function
% and must be vectorized

Iapprox = quad(@(x) exp(-x.^2), 1/2, 3/2, 1e-8)
% exact result: \sqrt(pi)/2 (erf(3/2) - erf(1/2))
Iexact = sqrt(pi)/2*( erf(3/2) - erf(1/2))

% absolute error
abs(Iexact - Iapprox)

% How would I estimate the value of int(exp(-x^2), 0, Inf) = sqrt(pi)/2?
quadl(@(x) exp(-x.^2), 0, Inf)
quadgk(@(x) exp(-x.^2), 0, Inf, 'AbsTol', 10e-8)

% Or, I could transform the integral by hand and then evaluate it
quad(@(t) exp(-tan(t).^2).*sec(t).^2, 0, pi/2, 10e-8)

% Or, I could just evaluate it in Mathematica (which is likely to give more
% accurate results for numerical integrations in general)

% MATLAB can also do double and triple integration
help dblquad
help triplequad

%% Least Squares

% Review - ways to solve linear least squares problems in MATLAB

% 0. The normal equations (Useful theoretically, but not ideal in practice)
A = rand(20,10);
b = rand(20,1);
x = A'*A\(A'*b);
disp('normal equations: residual for overdetermined system:');
disp(norm(A*x-b));

A = rand(5,10);
b = rand(5,1);
x = A'*A\(A'*b);
disp('normal equations: residual for underdetermined system:');
disp(norm(A*x-b));

% 1. The backslash (QR decomposition - the first thing to try usually)
A = rand(20,10);
b = rand(20,1);
x = A\b;
disp('backslash: residual for overdetermined system:');
disp(norm(A*x-b));

A = rand(5,10);
b = rand(5,1);
x = A\b;
disp('backslash: residual for underdetermined system:');
disp(norm(A*x-b));

spparms('spumoni',1);

A = sprand(2000,1000,0.01);
b = rand(2000,1);
x = A\b;
disp('sp backslash: residual for overdetermined system:');
disp(norm(A*x-b));

A = sprand(500,1000,0.01);
b = rand(500,1);
x = A\b;
disp('sp backslash: residual for underdetermined system:');
disp(norm(A*x-b));

A = spdiags(rand(2000,3),-1:1,2000,1000);
b = rand(2000,1);
x = A\b;
disp('sp backslash: residual for tridiagonal overdetermined system:');
disp(norm(A*x-b));

spparms('spumoni',0);

% lsqr (iterative method for large, sparse systems)
m = 3000;
n = 1500;
A = sprand(m,n,0.01);
b = rand(m,1);

tic
xDirect = A\b;
tDirect = toc;
fprintf('backslash took %g seconds to reach a residual of %g\n',...
    tDirect, norm(A*xDirect-b));

tic
x = lsqr(A,b);
t = toc;
fprintf('lsqr took %g seconds to reach a residual of %g\n',...
    t, norm(A*x-b));

fprintf('Speedup : %g\n',tDirect/t);

%% Polynomial Fitting

% Using the GUI (Tools->Basic Fitting)
x = [5 10 20 50 100]';
y = [15 33 53 140 301]';
plot(x,y,'o')

% (Note, we've avoided these GUI features mostly, but be aware there 
%   are many handy ones like this!)

% "by hand"
A = [ones(length(x),1) x];
m = A\y;
figure
plot(x,y,'o',x,m(1) + m(2)*x,'k-.');

% MATLAB represents polynomials by vectors of coefficients
P = [1 0 2 3] % corresponds to p(x) =  x^3 + 2*x + 3

% evaluate with polyval
xx = linspace(-3,3,100);
plot(xx,polyval(P,xx));

% Fitting with polyfit
Pfit = polyfit(x,y,1); %"1" for a 1st degree polynomial
yfit = polyval(Pfit,x);
figure
plot(x,y,'o',x,yfit,'k-.');


%% Fitting general functions

% Linear Least Squares
close all; clear; clc

f = @(m,x) m(1)*cos(x) + m(2)*exp(x); %linear in the parameters!
n = 1000;
x = linspace(0,1,n)';
mref = [1 -1];
data = f(mref,x) + 0.1*randn(n,1);
A = [cos(x) exp(x)];
mest = A\data;
figure;
plot(x,data,'r+',x,f(mest,x),'k-.',x,f(mref,x),'g--')
legend('data','fit','ground truth');

% Nonlinear least squares

% We saw the following function with the optimization toolbox:
help lsqnonlin 

% Nonlinear curve fitting
close all; clear; clc
f = @(m,x) m(1) + exp(m(2)*x);

n = 1000;
x = linspace(0,1,n)';
mref = [1 -1];
data = f(mref,x) + 0.1*randn(n,1);
m0 = [3, -3]; 
mest = lsqcurvefit(f, m0, x, data)

figure;
plot(x,data,'r+',x,f(mest,x),'k-.',x,f(mref,x),'g--')

%% Look for changes of variables!
close all; clear; clc
f = @(m,x) m(1)*exp(m(2)*x); %nonlinear
fc = @(mc,x) mc(1) + mc(2)*x; % mc = [log(m(1)), m(2)] 
n = 1000;
x = linspace(0,1,n)';
mref = [1 -1];
data = f(mref,x) + 0.1*randn(n,1);
datac = log(data);
A = [ones(length(x),1), x];
mcest = A\datac;
mest = [exp(mcest(1)) mcest(2)];
figure;
plot(x,data,'r+',x,f(mest,x),'k-.',x,f(mref,x),'g--')
legend('data','fit','ground truth');


%% Error Analysis

%{
% A fit by itself doesn't always mean anything. Here are a couple of easy
%  ways to get some idea of how much to trust your fit. 

For the full story, see ACM 118. (And for an even fuller story, see ACM
116)

%}

% We'll construct some data sets which are a polynomial plus Gaussian noise
%  (we add the same noise, scaled, each time)

close all; clear; clc

m = 100;
x = sort(rand(m,1));
Plinear = [1 -1]; %degree 1 polynomial
yLinear = polyval(Plinear,x);
noise = randn(m,1); %unit variance noise
yNoisy = yLinear + noise;
PlinearFit = polyfit(x,yNoisy,1);
yLinearFit = polyval(PlinearFit,x);

noiseLevel = 0.001;
yNicer = yLinear + noiseLevel*noise; %scale down the noise
PlinearFitNicer = polyfit(x,yNicer,1);
yLinearFitNicer = polyval(PlinearFitNicer,x);

Pcubic = [1 -1 1 -1];
yCubic = polyval(Pcubic,x);
yCubicNoisy = yCubic + noise;
PcubicFit = polyfit(x,yCubicNoisy,3);
yCubicFit = polyval(PcubicFit,x);

close all;
figure; hold all;
plot(x,yNoisy,'ro',x,yLinearFit,'k-.',x,yNicer,'b+',x,yLinearFitNicer,...
    'k',x,yCubicNoisy,'gs',x,yCubicFit,'k--');
legend('noisy data','fit','nicer data','fit','Noisy cubic','Cubic Fit');

% To test the 'null hypothesis' that our data can be
%  described by a polynomial plus some Guassian noise
%  we can use any number of test statistics. 
% Here, let's use a Chi^2 test, conveniently built in:

% chi-squared goodness-of-fit 
[hRandom,pRandom] = chi2gof(yNoisy-yLinearFit)
[hNicer,pNicer] = chi2gof(yNicer-yLinearFitNicer)
[hWrong,pWrong] = chi2gof(yCubic-yLinearFit)
[hCubic,pCubic] = chi2gof(yCubic-yCubicFit)
