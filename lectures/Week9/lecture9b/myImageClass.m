classdef myImageClass
    
    % Akin to fields in a struct
    properties
       pixelArray
       cmap
    end
    
    % Functions which use the local data
    methods
        function [] = draw(obj)
            figure();
            imshow(obj.pixelArray,obj.cmap);
        end
        
    end
    
end