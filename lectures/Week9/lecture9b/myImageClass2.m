classdef myImageClass2
    
    % Akin to fields in a struct
    properties
       pixelArray
       cmap
       figureHandle = 999
    end
    
    % Functions which use the local data
    methods
        function obj = draw(obj)
            figure(obj.figureHandle);
            clf;
            imshow(obj.pixelArray,obj.cmap);
        end
        
        function obj = invert(obj)
           obj.pixelArray  = 128-obj.pixelArray;
        end
        
    end
    
end