%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 9                                                         %
%     Lecture 9b : Image Data and Movies                                  %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Structure Arrays

% We saw before that MATLAB represents polynomials with matrices

P = [1 0 3 5]; % x^3 + 3x^2 + 5
x = linspace(-3,3,200);
plot(x,polyval(P,x));

%  and similarly, images and colormaps are also matrices
load durer
figure;
imshow(X,map); 
% or
image(X); colormap(map); axis equal off

% This works fine, but it has some problems
%  1. We can't tell from looking at X that it's supposed to be an image
%  2. The pixel data and colormap data are not obviously related

% One way to solve the second problem and partially solve the first is to 
%   define a struct, a collection of data with *named* fields. 

% Structs are much like cell arrays, but with a string (a 'field' name) 
%    attached to each entry
myImage = struct('pixelArray',X,'cmap',map);

% Entries are accessed by their field names
figure;
imshow(myImage.pixelArray, myImage.cmap);

% Structure arrays are what they sound like - arrays of similar sets of
% data. Define them with cell arrays of values

myStruct = struct(...
    'year', {1954,1960,1850,1882},...
    'name', {'Daubechies','Marcolli','Kovaleskaya','Noether'}...
    )

myStruct(1) 
myStruct(1).year 

myStruct(1:3) % returns a sub struct array
myStruct(1:3).name % returns a comma separated list

% creating a struct from cell arrays

anotherStruct = cell2struct(...
    {1954,1960,1850,1882; 'Daubechies','Marcolli','Kovaleskaya','Noether'},...
    {'year','name'},...
    1 ...
)

anotherStruct(3)

% A shortcut to create an empty array:
myStructArray(10) = struct('field1',[],'field2',[])

% A shortcut to create an constant array:
myStructArray(1:10) = struct('field1','defValue1','field2',[])

%% Objects

% To continue with our example, we'll also introduce the idea of an object

% An object is an intance of a class. We've already used many built in
% classes - numbers, strings, functions, etc.

% The idea here is that not only do we bundle together data, but also 
%  functions which operate on this data. 

% Let's define a class which represents image data and also has the ability
% to draw it.

% Like (non-anonynmous) functions, classes are defined in m-files
%  see myImageClass.m

load durer;
myImage = myImageClass();
myImage.pixelArray = X;
myImage.cmap = map;


% you can call methods with either
%  object.method(args) or
%  method(obj,args)

myImage.draw;
draw(myImage)

% One immediate benefit here is that once this class is written, we don't
% have to know or care about how the data inside is stored. 

% For example, we can go into our class definition and add information about
%   what figure the object draws to, so that it always draws in the same
%   window. Se myImageClass2.m

load durer
myImage2 = myImageClass2();
myImage2.pixelArray = X;
myImage2.cmap = map;

myImage2.draw;

myImage2inv = myImage2.invert;
myImage2inv.draw;

% Objects are much like structs, but with methods and a class name

%% Image Data

% Let's play with a real dataset. Thanks to Danielle Brown!
% http://bioimaging.caltech.edu/people/dvbrown/image01.html

%% Loading the Data

close all; clear; clc

% Our first task is to load the data we need. Here, it will all fit in
%  memory (RAM) 

% A helper function to form filenames
fname = @(z,t,c) sprintf('CellData/Part2_t%03d_z%03d_c%03d.tif',t,z,c);

% Ranges of t, z, c
tMin = 1;    tMax = 8;  sizeT = tMax-tMin+1;
zMin = 15;   zMax = 30; sizeZ = zMax-zMin+1;
cMin =1 ;    cMax = 2;  sizeC = cMax-cMin+1;

% We open the first image, presuming all to be the same size
%  and using the same colormap
[im1,map1] = imread(fname(zMin,tMin,1));
[~,map2] = imread(fname(zMin,tMin,2));
map = {map1, map2};  % a cell array
[sizeX,sizeY] = size(im1);
clear map1 map2 im1 ;

% A 5-dimensional array to hold all the data
% It's a good idea for anything that takes a long time
%   to print some notion of progress
fprintf('Loading: ');
data = NaN(sizeX, sizeY, sizeZ, sizeT, sizeC);
 for t = tMin:tMax 
    for zInd = 1:sizeZ
      z = zMin+zInd-1;  
      for c = cMin:cMax
         data(:,:,zInd,t,c) = imread(fname(z,t,c)); 
      end
      fprintf('[%3.1f%%]',100*((t-tMin)*sizeZ+(z-zMin+1))/(sizeT*sizeZ));
    end
   fprintf('\n');
 end
 clear z t c zInd fname

 %% Let's save this data as a .mat file so we can retrieve it later
 disp('saving..');
 save celldata
 disp('done.');

%% Making a movie

% A movie is a struct, with fields for color data and a colormap
%  for each frame
zInd = (floor(sizeZ/2));
c = cMin;
M1(sizeT) = struct('cdata',[],'colormap',[]);
for i=1:sizeT
    t = tMin+i-1;
    M1(i) = im2frame(data(:,:,zInd,t,c),map{c});   % cell array indexing
end

% play
close all;
movie(M1); shg
movie(M1,10); shg
movie(M1,-10); shg

%%

% One can also make an avi from a series of figures, using getframe
zIndVals = 1:5:sizeZ;
nZIndVals = length(zIndVals);

M2(sizeT) = struct('cdata',[],'colormap',[]);
close all;
figPos = [0 0 nZIndVals*500 500];
figure('Position',figPos); hold on; axis off; axis equal;

c = cMax;
scale = 4;
for i=1:sizeT
    cla
    t = tMin+i-1;
    for j = 1:nZIndVals
        zInd = zIndVals(j);
        xPos = ([0 1]+(j-1))*sizeX;
        yPos = [0 1]*sizeY;
        image(xPos,yPos,data(:,:,zInd,t,c)); % first two arguments give position
        %colormap(map{c});
        colormap(gray); %easier to see
    end
    M2(i) = getframe();
end

% play
shg 
movie(M2,-5);

%% Exporting our movie

% MATLAB has a movie2avi function to save an avi file from 
%  movie data - this doesn't work well on my system.

% A more full-featured option is to use a VideoWriter object
vr = VideoWriter('myMovie');

% this is an object of type VideoWriter
class(vr)

% It holds data
properties(vr)

% and has methods
methods(vr)

% VideoWriter lets us write directly to file as we render. 
vr2 = VideoWriter('static.avi');
vr2.set('FrameRate',15); % a method which changes a property
open(vr2)
figure
axis off square
for k = 1:50
    imagesc(rand(100));
    writeVideo(vr2,getframe);
end
close(vr2);

% The exclamation points sends system commands (to a bash shell in
%   my case)
!open static.avi
    
%% Rendering our previous example to file

zIndVals = 1:5:sizeZ;
nZIndVals = length(zIndVals);

close all;
figPos = [0 0 nZIndVals*500 500];
figure('Position',figPos); hold on; axis off; axis equal;


vr3 = VideoWriter('myMovie.avi');
vr3.set('FrameRate',5);
vr3.open; %or open(vr3)

c = cMax;
scale = 4;
for i=1:sizeT
    cla
    t = tMin+i-1;
    for j = 1:nZIndVals
        zInd = zIndVals(j);
        xPos = ([0 1]+(j-1))*sizeX;
        yPos = [0 1]*sizeY;
        image(xPos,yPos,data(:,:,zInd,t,c)); % first two arguments give position
        %colormap(map{c});
        colormap(gray); %easier to see
    end
       writeVideo(vr3,getframe); %or vr3.writeVideo(getframe)
end
vr3.close;

% play
!open myMovie.avi

    
%% Reading Movies

% There is also a MovieReader class!

% Open a movie file and get some info
reader = VideoReader('myMovie.avi');
nFrames = reader.NumberOfFrames;
vidHeight = reader.Height;
vidWidth = reader.Width;

% Initialize a structure array that movie will understand
M(1:nFrames) = struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
           'colormap', []);

% Read frames
for k = 1:nFrames
    M(k).cdata = read(reader, k);
end
figure('position', [150 150 vidWidth vidHeight])
movie(gcf,M,-5,reader.FrameRate);

%% Combine the two channels into one truecolor (rgb) image.

% We can get away with adding images together, but in
%  general, more elaborate compositing might be required!

data2 = NaN(sizeX,sizeY,3,sizeZ,sizeT); 
for t = 1:sizeT 
    for zInd = 1:sizeZ
      data2(:,:,:,zInd,t) = zeros(sizeX,sizeY,3);  
      for c = cMin:cMax
          data2(:,:,:,zInd,t) = data2(:,:,:,zInd,t) + ...
              ind2rgb(data(:,:,zInd,t,c),map{c});
      end
    end
end

%%
save celldata2 data2

%% See a video of one slice of the combined data
M3(sizeT) = struct('cdata',[],'colormap',[]);
zInd = (floor(sizeZ/2));
for i=1:sizeT
    M3(i) = im2frame(data2(:,:,:,zInd,i)); 
end

% play
close all;

movie(M3,-10); shg

%% Finding edges

% The edge command incorporates several algorithms for finding 
%  edges in intensity (grayscale) images

% Convert one frame of our rgb data to intensity
im = rgb2gray(data2(:,:,:,floor(sizeT/2),floor(sizeZ/2)));
close all;
imshow(im,[0,max(max(im))]); %<-- from the image processing toolbox

% or
figure
im2 = gray2ind(im,256);
image(im2); colormap(gray); axis equal off

%% Compare all 6 methods
methods = {'sobel','prewitt','roberts','log','zerocross','canny'};
figure
for i=1:length(methods)
    subplot(2,3,i); 
    imshow(edge(im,methods{i}));
    axis equal; 
    title(methods{i});
end

tightfig % handy utility from the file exchange! 
% [http://www.mathworks.com/matlabcentral/fileexchange/34055]  

%% We can improve these results by adjusting parameters
figure
imEdges = edge(im2,'canny',0.04);
imshow(imEdges);


%% blobs, done poorly

%regionprops does all kinds of computation
help regionprops

% A poor attempt at computing the centroids of the black regions:
figure
imEdges = edge(im2,'canny',0.01);
s = regionprops(~imEdges,'centroid'); %Notice we negate the image
centroids = cat(1, s.Centroid);
imshow(imEdges); colormap(gray)
hold on
plot(centroids(:,1), centroids(:,2), 'b*')
hold off
        
%% Final

% Will be available tomorrow, due in one week for seniors (June 6) and it two weeks for
% underclasspeople (June 13). As with the midterm, you can pick from some
% suggested projects, or devise your own. One project will be to work with
% more biological imagery.