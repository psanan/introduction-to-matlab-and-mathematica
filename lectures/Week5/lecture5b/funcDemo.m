function [output1, output2] = funcDemo(input1, input2)
% FUNCDEMO produce some arbitrary output
%   Demonstrates local and nested functions

    % you can provide default values for undefined input arguments
    %  using the nargin function
    if (nargin < 2) 
        input2 = 1; 
    end

    % this is a local variable, which is destroyed once this function
    % returns
    count = 0;

    % this is a 'nested function'. It can access local variables in 
    %  its parent function. These are confusing - avoid if possible
    function out = nestedFunction(in)
       count = count + 1;
       out = in + 2; 
       
    end

    % Despite appearances, the return values are NOT a matrix
    %   rather, they are a list of variable names. This allows you
    %   to compute several values with one function, [a,b] = func(x,y,z)

    output1 = nestedFunction(input1) + nestedFunction(2323) +...
        subFunction(input2,count);
    output2 = count;
    
end %<-- this is optional if this m-file defines only one function

% Function m-files can define local functions. This function is only
% callable from within this file
%
% These are clear to understand and useful!
function out = subFunction(in,count)
    out = in + 1;
    disp(count)
end