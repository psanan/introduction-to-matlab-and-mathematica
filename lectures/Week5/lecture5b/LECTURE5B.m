%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 5                                                         %
%     Lecture 5b : Basic Operations in MATLAB                             %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% More on Matrices
% Practically every task in MATLAB uses matrices in some way, so let's
% go over some more ways to work with them

A = [1 4 7; 2 5 8; 3 6 9]

% reading
A(1,1) %indexing is 1-based in MATLAB (as in Mathematica)
A(1,2)
A([1 3],1)
A(2, [2 3])
A(1:3,1)
A(:,1)
A(1,:)
A(2:end, 3) % the end keyword is overloaded to allow this
A([1 3 2],:) % permute rows
A([1 3 2],[1 3 2]) % permute rows and columns

% also see 'logical indexing' below for another way to get subarrays
A(logical([1 0 0]),logical([1 0 1]))

% getting the size
length([ 1 2 3]) %only for row/column vectors
size(A,1)
size(A,2)
[m,n] = size(A)

% writing
A(1,1) = 3
A(1:3,2:3) = NaN
A(1:2,1:3) = [100 200 300; 400 500 600]

% linear indexing
%  Another place where MATLAB's flexibility can get you into trouble!
%  MATLAB uses "column major order", meaning a matrix is stored as
%  as a linear array, one column after another
A(:)
A([1 7 8]')
A(1:4:end) % handy trick to extract the diagonal : NbyNMatrix(1:N+1,end)

% reshaping
reshape(1:9,3,3)
B = [1 2 3 4; 5 6 7 8]
reshape(B,4,[])
reshape(B,[],4)

% higher dimensional arrays
M = reshape(1:27,3,3,3)
M(:,:,1)

% generating matrices
eye(7) %the 'eye'dentity
ones(5) % the same as ones(5,5)
ones(5,3)
rand(2,2)
randn(3,3)
zeros(4,4)
inf(3,3) 
nan(4,1)
magic(5)

M = repmat((1:5)',1,7)

% the brackets/space/comma/semicolon syntax assumes you want an array of rank 2 or less, 
%  which allows you to do 'block' operations. In Mathematica, this sort of
%  thing would require calls to Flatten, probably!
block = [1 2; 3 4]
B = [block, 2*block; -block, 2*block]

% when you have to:
A = NaN(5);
for i=1:5
    for j=1:5
        A(i,j) = i*j;
    end
end

% but you often don't have to:
A = (1:5)'*(1:5)

% Aside: a more esoteric but very useful function, bsxfun
M = bsxfun(@plus,rand(5),1:5) % A taste of Mathematica! 

%% Numeric Data Types
% these are the names of numeric data types, and also functions which
% convert to those data types. 
logical
double % the workhorse
single
int8
int16
int32
int64
uint8
uint16
uint32
uint64
char % not actually 'numeric', but behaves very similarly

help datatypes

% tests
isnumeric(3)
isnumeric([1 2 3 4 5])
isnumeric('string')
isfloat(1)
isfloat(single(1))
isfloat(double(1))
isinteger(1) %<-- this is a double by default!
isinteger(int32(1))
isinteger(1.1)
islogical(1)
islogical(logical(1)) %<--confusing since logical '1' and numeric '1'
                      % look the same as output!
islogical(true) %better to use..
[1 2 3 4] == [1 2 100 200] % but not what is output.

% floating point types have special values inf and nan
0/0
1e999^2
isnan(NaN)
isinf(Inf)
isfinite(NaN) % useful for automatically checking numerical code for problems
isfinite(-Inf)
isfinite(3)

% some other useful values for floating point numbers:
realmax('double')
realmax('single') % also see intmax, etc. 
eps('double') % the smallest number eps so that, in double precision, 1+eps > 1
eps('single')

% some more on logical data

% like most basic operations in MATLAB, logical operators are vectorized
[1 2 3 4] <= [1 3 2 1]
logical([1 0 1 1]) | logical([1 0 0 0]) % elementwise 'or'

% any and all are useful
any([1 0 0 0]) % matlab correctly interprets the input as logical
all([1 1 1 1])
all([1 0 0 0]}

% 'short circuit' variants are NOT vectorized, since they are designed to
% return without evaluating all the operands
a || b % if a is true, returns true without looking at b
a && b % if a is false, returns false without looking at b

% Finally, you can use logical values to perform 'logical indexing'
% instead of an array of integer indices, pass in a logical array
v = 1:10
indexSet = rand(1,5) < 0.5
v(indexSet)
A = [1 2 3; 4 5 6; 7 8 9];
A < 5 % uses linear indexing 
A(A<5) = NaN % very handy!

%% Strings
% strings are just arrays of char values in MATLAB. 
% characters are essentially integers
'a string'
['a' ' ' 's' 't' 'r' 'i' 'n' 'g']
char(77)
double('s')

% you can operate with strings accordingly
['this' 'combines' 'strings']

['this'; 'is an error because the dimensions are wrong']

{'this','is probably what you wanted'} % (more on cell arrays later)

% equality between strings
strcmp('aaa','bbbbb')
strcmp('aaa','aaa')
 
% Some basic output functions
disp('display this string, and add a newline')
disp(3) % also displays numbers or other values

fprintf('integer: %d, double: %f, string: %s \n',1234, 0.999, 'Hello, World.')
disp(['integer: ' num2str(1234) ', double: ' num2str(0.999) ', string: '...
    'Hello, World.']);
myString = sprintf('integer: %d, double: %f, string: %s \n',1234, 0.999,...
    'Hello, World.');

%% Sparse Matrices
% work very much like dense matrices, except they don't explicitly store
% any zero entries
S = sparse(10,10)

S(4,7) = 47
S(6,7) = 67
S(1,1) = 11
S(3:10,3:10)
spy(S) % very useful for seeing the sparsity structure
full(S) 

% sizes
size(S,1)
nnz(S) %[n]umber of [n]on-[z]eros

% Generating sparse matrices
S = spalloc(10,10,20); % knowing the number of nonzero entries expected is useful
sprand(10,10,0.1)
sparse(rand(4,4))
v = (1:8)';
spdiags([-v 2*v v],-1:1,8,8)

% The 'usual' way you'll see in numerical codes. Unless your matrix is
% very structured, this is more or less the way sparse matrices are
% constructed in practice
iSet = [1 3 4 6];
jSet = [3 6 7 8];
vals = [122 342 345345 34534];
sparse(iSet, jSet, vals, 20, 20, 4)

% with spalloc
disp('random sparse with spalloc');
n = 100000;
nonZeroCount = 10000;
tic
S = spalloc(n,n,nonZeroCount);
for k=1:nonZeroCount
    i = ceil(n*rand(1));
    j = ceil(n*rand(1));
    S(i,j) = rand(1); %<-- The editor GUI knows this isn't a good idea
end
toc

% This is a more standard way to accomplish the same task
disp('random sparse with standard procedure');
n = 100000;
nonZeroCount = 10000;
tic
S = sparse(...
    ceil(n*rand(1,nonZeroCount)),... % row indices
    ceil(n*rand(1,nonZeroCount)),... % col indices
    rand(1,nonZeroCount),...         % values
    n,n, nonZeroCount);              % dimensions, nnz
toc

%% Loops and Control
% These are simple. Whitespace doesn't matter if you are ending your
% statements with semicolons. Otherwise line breaks are used to separate
% statements so they do make a difference

for i=1:10
   disp(i);
end

% you can do this, but it's very hard to read
for i = 1:10 disp(i); end

% this would work
for i=1:10
   disp(i)
end

% this wouldn't work
%for i = 1:10 disp(i) end

for i=[1 4 6; 7 8 9]
   disp(i)
end

if (1 > 0)
   disp(' 1 is greater than 0'); 
else
    disp('this will never happen')
end

% you can save space with on-liner if statements, if you'd like,
%  but be aware that this is potentially harder to read
if (1>0) disp('1 is greater than 0'); end

% else
for i=1:10
    if rand(1) > 0.5
        disp('A'); 
    else
        disp('B');
    end 
end

% while
a = 1;
while a < 100
   disp(a);
   a = a*2; %MATLAB still doesnt't have *= and similar operators
end


% switches are a bit different from some other languages. 
a = 'str';
switch a %<-- a scalar or a string
    case 1,
        disp('1');
    case 2,
        disp('2');
    case 'str',
        disp(a);
    otherwise,
        disp('other');
end

% break and continue
a = 0;
while (1)
    a = a + 7;
    disp(a)
    if mod(a,5)==0
       break; %breaks the loop
    end
end

for i=1:100
   if i > 10
       continue; %skips the rest of the loop
   end
   disp(i)
end

%% More on Functions
% the function syntax is used to define functions inside of m-files.
% function definitions are NOT allowed in scripts, so see 
% funcDemo.m

% functions can accept a variable number of arguments
help varargin

% Anonymous functions
% The @ symbol allows you to refer to a function
% this can be a built-in function
@plus

% or it can be an anonymous function (like we saw in Mathematica)
%  The syntax is functioName(arg1,arg2,...)
f = @(x,y,z) x+y+z;

% these anonymous functions are less flexible than functions as defined in m-files
%  so should be reserved for small tasks

% you can get some efficient methods using anonymous functions with
% bsxfun
pairwiseOp = @(a,b) a.^b + a;
bsxfun(pairwiseOp,[1 2 3; 4 5 6; 7 8 9],1:3)
