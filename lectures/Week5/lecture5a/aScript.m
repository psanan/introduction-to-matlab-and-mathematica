% A script which generates a random matrix and produces a plot

close all; clear; clc

n = 100;

disp('Generating random matrix.');
tic
A = rand(n);
A = A.*(A < 0.5);
toc

disp('Visualizing random matrix.');
tic
spy(A)
toc