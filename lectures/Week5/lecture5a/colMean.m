function r = colMean(A)
% mean column value
% colMean(A) returns a column vector which is the mean of the columns of A

[m,n] =  size(A);

for i=1:m
    c = 0;
    for j=1:n
        c = c + A(i,j); 
    end
    r(i) = c/size(A,1);
end
r = r';

%{

First, whenever possible, preallocate any vectors or matrices you are building. In this case, add a line like 
r = NaN(size(A,1),1)
which prevents matlab from having to resize r, and also gives you a hint if you have a bug in your code, as the default value is obviously incorrect.

Second, use auxiliary variables to make your code more readable (and efficient). In this case, it would be better to include a line like
[m,n] = size(A);
and use m and n in your loop bounds

The next couple of points actually make the first two irrelevant in this particular example!

Third, avoid using loops wherever possible. The alternative is to use MATLAB's extensive set of vectorized functions. For example, it is both slow to run and poor style to add a pair of vectors with a loop, like

c = NaN(length(a),1);
for i=1:length(a)
c(i) = a(i)+b(i)
end

when one could use vectorized addition to write
c = a+b;

This leads to the fourth point, which is to utilize MATLAB's built-in functions as much as possible. MATLAB already has a function to compute means! Indeed, you can calculate the average of the columns of a matrix A just by calling
mean(A,2)

%}