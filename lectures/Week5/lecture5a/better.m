% A script to compute a least squares solution to a randomly
%  generated tridiagonal system and give the L-infinity error

close all; clear; clc

% System size
n = 10000;

% Generate a tridiagonal matrix
c = ones(n,1);
A = spdiags([c,4*c,c],-1:1,n,n);

% Generate a random rhs
b = rand(n,1);

% Solve 
x = A\b;

% Compute a residual
resInfNorm = norm(A*x-b,'inf');

% print
fprintf('For n=%d, the maximum magnitude entry in the residual is %g\n',...
    n,resInfNorm)
