% This is the help text for this script

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 5                                                         %
%     Lecture 5a : Introduction to MATLAB                                 %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{

What is MATLAB?

MATLAB is short for Matrix Laboratory. It is a numerical computation 
environment which provides a suite of tools for computation, visualization,
and more. 

When to use MATLAB?
(1) for rapid prototyping of numerical algorithms 
(2) for quick data analysis and visualization
(3) when it's free (like here at Caltech)

When not to use MATLAB?
(1) when you need to handle truly massive datasets
(2) when efficiency is paramount
(3) when you need production quality code
(4) when you need symbolic capabilities
(5) when you have to pay for it

Competition for MATLAB:
- OCTAVE, an open-source clone of MATLAB
- computational languages/environments like S and R
- Excel
- Mathematica, Maple, Axiom, other CASes (Computer Algebra Systems)
- systems languages (C/C++, ...)
- general purpose languages (Java, Lisp, ...)
- scripting languages (Python/NumPy/SciPy, Perl, ...)
- MATLAB clones (Scilab, ...)

In one way or another, most of MATLAB's competitors can do what MATLAB 
does. For instance, you can solve Poisson's equation in Excel... but that
doesn't mean you should. Here are some of MATLAB's advantages:

(1) the language is intuitive and mathematically expressive (vectorization!)
(2) the documentation is excellent
(3) many toolkits are available which extend the functionality
(3) the debugger and profiler are integrated and easy to use
(4) MATLAB is an industry standard.
(5) MATLAB matrix manipulation algorithms (esp. for sparse matrices) are 
state of the art

and disadvantages:

(1) the scripting system is somewhat primitive
(2) for complex tasks (especially ones which require for loops), MATLAB
can sometimes be slower than hand-coded C or Fortran
(3) MATLAB is expensive

%}

%{
 
The Matlab interface 

- Elements of the Matlab desktop: 
  - command window, command prompt
  - history window ( and other history features available at the command prompt:
    <up> key, tab completion, drag-n-drop )
  - workspace, variable editor (and plot window)
  - current directory and file details editor
  - the current folder selector, which help determines search path ( make
    sure you're consistent about which directory you work in )
  - the editor (you can use your own editor!)

  %}


%{ 
 
 About m-files and the editor:

This file, and all script and all non-compiled files in Matlab, are known
as m-files, and have the extension ".m". This file is LECTURE5A.m. 
Matlab backs up m-files automatically as you work on them, so  you may
also see a "LECTURE5A.m~" file and in Windows you may also see a 
"LECTURE5A.m.asv" file. You can ignore these, or turn them off in the
Preferences.


Comments:
Most of the text in this file is in the form of comments, text that is 
(mostly) ignored by MATLAB. Use comments to document your code! There are two
types of comments: 
- group comments, which begin with %{ on a line by itself and end with %} 
on a line by itself,
- line comments, which begin with % and end at the end of the line. These
can occur anywhere in the code 

Cells:
There are two meanings of the word "cell". One is a datatype, which we will
discuss later. In the context of an m-file, a cell is a unit of division,
similar to an input cell in Mathematica.

You can use cells to split script files into chunks which execute separately.
To execute a single cell, see the button in the toolbar, or hold down "ctrl" 
and then hit "enter" on your keyboard. The beginning of a cell is a special 
comment line that begins with two "%", not just one.

Note that the titles of cells appear in the 'details' section of the 
Current Folder pane in the GUI

We will use cells a lot in lecture as a convenient way to step through
code, but be aware when working on your own code that error messages
are not as good when evaluating cells, so you're usually better of running
scripts and functions.

Keyboard shortcuts:
These are useful, but vary between Windows and Unix. Check out the
menus and tooltips over buttons on your OS to learn the appropriate ones.
A useful one on my machine is F5, which saves and runs the script in the
editor

%}

% Note: we won't break this entire file into cells, but to execute
%  a subset of lines, you can enclose them in a cell, or highlight them and
%  use "Evaluate Selection" (right click, or Shift-F7 on my machine)

%% The Help system: your best friend

% help shows the format(s) for using a command and directs you to related
% commands; without any arguments, it gives you a hyperlinked list of
% topics to find help on; with a topic as an argument, it gives you a list
% of subtopics

help plot
help qr
help

% if you want to see all the commands associated with elementary matrix
% manipulation
help matlab/elmat 

% doc is like help, except it comes up in a different window, and may
% include more details
help fft
doc fft

% lookfor is used when you don't know what command you want; it does 
% something like a keyword search through the documentation

lookfor decomposition % find MATLAB's matrix decomposition functions

% similarly, you can use docsearch
docsearch fourier

% which helps you tell which file a particular command refers to, or
% whether it is built in

which abs
which hadamard

% demo gives video guides and example code
demo

%% MATLAB as a calculator

% standard commands to start a session
clc % clears the screen
clear % clears all variable definitions
close all % close all figures

3 + 5 % displays result

3 + 5; % suppresses display of result (but the calculation is done)

3 +        5 % white space doesn't matter where it shouldn't

% Wrapping lines
3 + ...
5

%%

% as long as i or j hasn't been assigned to as a regular variable, MATLAB
% interprets them as sqrt(-1)
i^2
exp(i*2*pi)
1 + j

1i %better

% MATLAB respects the standard order of operations
3 + 5/2 
(3 + 5)/2

% Variables: MATLAB doesn't require you to declare variables, and is case
% sensitive
a = 4

A = 7

a^A

mod(a, A)

rem(A, a)

ans+1 % MATLAB stores the result of the last calculation as a variable named 'ans'

x = 8; a+x % multiple commands on one line

% a word of warning - MATLAB is relatively permissive, which can lead
%  to errors
a = [ 1 2 3 ]
b = 1
a + b % allowed, will not give an error, even if b was supposed to be a vector!


% who and whos give information on the variables currently defined
%  you can also see this information in the 'workspace' tab, which
%  lists all the global data you have in memory
who 
whos

clear a A x % get rid of variables we've defined earlier
who

% Arguments to functions in MATLAB can be given with a comma-separated
% list in parentheses
mod(55,7)

% or as a space-separated list after the function name where the arguments
% are interpreted as STRINGS. 
hold on %common, same as hold('on')

mod 55 7 % don't do this!
mod(55,7) % rather do this
mod(['5', '5'],'7') % equivalent to mod 55 7

a = 3
clear('a') % same as clear a

%% Saving and importing
save('examplestorage.mat', 'a', 'A') % store just the variables a and A
clear % clear the memory
load('examplestorage.mat') % loads back a and A
anothervar = 34 % make a new variable
save examplestorage % alternative formats for saving and loading entire workspace
load examplestorage

%% The next step: Matrices and vectors. 
% Matrices are the basic datatype in Matlab

x = [1, 2, 3] % row vector: separate elements in the same row with commas
y = [1 2 3] % row vector: spaces also work to separate elements in a row

z = [1; 2; 3] % column vector: separate elements in the same column with ;

z = 1:20 % can use the range notation to generate row vectors
z = 1:2:20
z = 20:-2:0
z = [0:.1:1] % the array brackets are optional


A = [1, 2, 3; 4, 5, 6; 7, 8, 9] % a 3-by-3 matrix
A = [1 2 3
    4 5 6
    7 8 9] % can also use newlines to separate rows

%A*x % invalid! just like regular math, quantities need to have the correct 
    % dimensions to make a valid MATLAB expression

z = [1; 2; 3]
A*z % this works: matrix times column vector yields column vector

% use x' to take the conjugate transpose of a vector, and .' to take the
% real transpose; e.g.: 

A = [i 0; 0, -i];
z = [-i, i];
A*z.'
A*z'

% It often convenient to define column vectors like this
x = [1 5 6 7 7]'

% Matlab requires you to include * explicitly for multiplication
a = 1;
b = 2;
a b % error - this means a('b')
a * b 

% Also, looking ahead, note that by default MATLAB does *matrix*
% multiplication and 'division' (solving linear systems)
% for elementwise operations, add a dot
A = [1 2; 3 4];
B = [5 6; 7 8];
b = [1 2]';
A*B
A.*B
A\b %solves Ax = b
A.\b %error
A.\B
A./B
b'/A % solves x'A = b', that is A'x = b

%% M-files
% MATLAB uses m-files to define scripts and functions

% Scripts are simply files which list a series of commands to execute in
% order
% 
% see aScript.m

% Function m-files function like Module or Block in Mathematica. They
% define a local scope. They must begin with the correct syntax for a
% function, and must have the same name as the function they define
%
% see square.m 

% built in MATLAB functions are sometimes m-files
edit cgs

% and sometimes call compiled code
edit sum

%% GUI syntax highlighting and tips
% using the editor in MATLAB is a good idea

%it'll warn you about parentheses, and highlight parentheses pairs
a = 3 * ( 4 + 5 * ( cos(5)- 3) );

% it'll warn you if you forget to end a line with a semicolon
a = 3

% it'll even tell you if you're doing something inefficient!
a(find(a > 3))

a(a > 3)

a = [];
for i=1:1000
   a(i) = [a 1]; 
end

%% Preallocation

% This works in MATLAB
for i=1:1000
   a(i) = 1+2*i; 
end

% This is better
a = NaN(1000,1);
for i=1:1000
   a(i) = 2*i-1; 
end

% This is much better
a = 1:2:1999;


%% Some old unix friends:
pwd
ls
cd
mkdir newDir

% The editor
edit newFile.m

%% Debugging and Survival Tools

% Test often, find bugs before the get buried

%Use breakpoints

%% Initialize with NaN
a = zeros(100,1); %common
a = NaN(100,1); %better

% Error messages

% Matlab produces useful error messages, which include line numbers
% when running a script or function (but not when evaluating cells)

%%
[1 2] + [1 2 3]

%%

% Use scripts to develop, then convert to functions
% Much as Remove["Global`*"] was our friend in Mathematica, here we have
clear; close all; clc

% Control-C
for i=1:1e10, a = rand(10); end

% But be warned, MATLAB to this point doesn't have the kernel-killing
%  ability Mathematica does, so if you
%  call a piece of compiled code which will take a long time
%  you may have to wait, or kill the MATLAB application.
a = rand(100)

% a = rand(1e6) %NO! 


%% Style
% Writing clear and maintanable code - some tips:
%   1. Use functions liberally
%        a. function m-files
%        b. anonymous /inline functions
%   2. Use auxiliary variables

n = 100
A = rand(n)
 = svd(A)
for i = 1:n
   disp(S(i)) 
end

%   3.. Take the time to clean up code after you write it
%        1. Respect this line -------------------------------------------->
%   4. document!
%        a. cells
%        b. help text
%        c. comments
%   5. Vectorize when possible, and use builtin functions where possible
%   6. Choose consistent naming conventions

%% Examples

% poor.m [awful]

% better.m [a bit better, at least usable]

% colMean.m [needs to be fixed]
