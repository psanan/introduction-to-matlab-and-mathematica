%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 6                                                         %
%     Lecture 6a : Basic graphics in MATLAB                               %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Figures and Axes

% a figure is MATLAB's name for a graphics window. You can create a new one
% with the figure command
figure

% MATLAB uses figure handles ( doubles ) to refer to these windows
figHandle1 = figure
figHandle333 = figure(333)

% figure('PropertyName',value,...) lets you describe properties
figure('Position',[0 0 400 500])

docsearch 'figure properties'

figure('Name','An Empty Graphics Window')

% You can also adjust things interactively
propertyeditor

% We won't pursue MATLAB's extensive set of tools of this type, for two
% main reasons :
% 1. These GUI-based tools are (in theory) largely self-documenting
% 2. For most scientific applications, figures need to be regenerated
% frequently, so its worth the effort to do it with scripts

% Do note all the menus and buttons MATLAB provides, though!

gcf % creates a figure if none exists, and returns the current figure
figure(figHandle1)

figHandle2 = gcf 

% get and set work with many 
set(h,'Resize','off'); %turn off resizing
shg

clf %clears the current figure

close(figHandle1)
close %same as close(gcf)
close all %close('all')

% figure and axes properties include things like 'children'
figure
figure(444) %specify the handle should be 444
get(0,'children') %0 refers to the graphics root
axes
axes
axes
get(gcf,'children')

% An axes is a graphics object within a figure. They behave very much like
% subfigures. They have handles and properties
axesHandle = axes
gca
cla

docsearch 'axes properties'

% confusingly, axis controls the axes within an axes!

% subplot is a shortcut to create and work with grids of axes
subplot(3,4,1)
subplot(3,4,4)
subplot(3,4,11);

% Now, on to using figures with common plot commands

%% 2D Plotting

% The plot command is similar to Mathematica's ListPlot
x = linspace(0,10,100);
y = cos(x);
plot(x,y);

% if x is not provided, index values are used
plot(sin(1:100))

% plot has its own peculiar syntax for specifying plotting styles
docsearch LineSpec
plot(x,y,'g-*');

% plot also accepts options
docsearch 'Lineseries properties'
plot(x,y,'ks','MarkerSize',4);

% this one is undocumented, and bug-riddled, but might be something
%  to try if you want antialiased lines:
subplot(1,2,1)
plot(x,y,'LineSmoothing','on')
subplot(1,2,2)
plot(x,y)

% multiple data sets can be plotted
x2 = linspace(0,10,1000);
y2 = sin(x2);
plot(x,y,'r',x2,y2,'b+','LineWidth',3);
shg

% matrices are interpreted columnwise
plot(repmat((1:10)',1,5),rand(10,5)); shg %plot 5 random sets of 10 points

% you can plot many y's vs the same x's
plot((1:10).^2,rand(10,7)); shg

% by default, plot commands draw to the current figure and axes,
% overwriting anything there
close all
plot(rand(1,10)); shg
plot(rand(1,10)); shg

% the hold command alters this behavior, so plots add to the current axes
hold on
plot(rand(1,10)); shg
plot(rand(1,10)); shg
hold off

% hold works with the NextPlot property of the current figure
set(gcf,'NextPlot','add'); % same as hold on
set(gcf,'NextPlot','replace'); % same as hold off

%hold all keeps the color and linestyle from resetting
figure
hold on
plot(cos(1:10)); %blue
plot(sin(1:10)); %also blue

close all
figure
hold all
plot(cos(1:10)); %blue
plot(sin(1:10)); %green

% colors are specified in many ways
docsearch ColorSpec
figure
plot(rand(1,10),'Color',[0 0 1]);
plot(rand(1,10),'Color','b'); shg
plot(rand(1,10),'Color','blue'); shg

% axis (not to be confused with axes)
axis([0 10 0 20])
[xmin, xmax, ymin, ymax] = axis;
axis off % axis('off') 
axis on
axis equal
axis square
help axis
axis xy
axis ij 
axis image
axis normal

% similar commands
xlim([0 10])
grid on
legend('label1','label2'); % use after plotting is finished
xlabel('x');
ylabel('y');
title('my title')

% fonts are stored as axes properties
listfonts
figure
axes
set(gca,'FontSize',20);
set(gca,'FontName','Comic Sans MS'); %noooo
title('This is the Title');

% an example of using handles
figure();
tanfighandle = gcf;
plot(x, tan(x), 'b')
lims = axis;
axis([lims(1) lims(2) -10 10]);
% ylims([-10 10])
axis off

figure(gcf+1);
secfighandle = gcf;
plot(x, sec(x), 'r');

figure(tanfighandle) % change the current figure back to the first plot
hold on; % or hold all to keep using new colors
plot(x, tan(x/2), 'r') 

% The ez-est plotting commands in MATLAB are fplot and the various ezplots
%
% fplot(function, [xmin xmax]) plots a function of a single variable in the
% specified range
% here function is a function handle or anonymous function
figure()
fplot(@(x) exp(-.1*x).*sin(x), [0,20])

% you can also specify the yaxis limits
figure()
fplot(@sin, [0,20, -2, 2])

% the ezplot command does much the same, except it labels your graph and
% x-axis (it determines what the independent variable is). The default x
% range is [-2*pi, 2*pi]
figure()
ezplot(@(u) exp(-.1*u).*cos(u)*log(u), [1,10])

% you can also plot implicitly defined functions using ezplot-- if the
% argument is a function of x and y, it will assume you want to plot
% f(x,y) = 0; the limits a,b apply to both x and y (you can specify [xmin
% xmax ymin ymax] also
figure()
levelSet = 3
ezplot(@(x,y) x.^2+y.^2 - x.*y-1 - levelSet, [-3,3])

% parametric ezplot
figure()
fr = @(t) exp(cos(t)) - 2*cos(4*t) - sin(t/12).^5; %note .^
fx = @(t) sin(t).*fr(t);
fy = @(t) cos(t).*fr(t);

ezplot(fx, fy, [-10,10])  % note the low quality 
                          % we can't specify the number of points to use

% more control with plot
t = linspace(-10,10,5000);
plot(fx(t),fy(t),'LineSmoothing','on')
shg

% there are other ez plotters, like ezcontour, ezpolar
% e.g.
figure()
ezpolar(@(th) sqrt(2*2^2*cos(2*th)))

figure()
ezcontour(@(x,y) cos(x)*cos(y)*exp(-sqrt((x^2+y^2)/4)))
figure()
ezcontourf(@(x,y) cos(x)*cos(y)*exp(-sqrt((x^2+y^2)/4)))

% there are also ez plotters for 3d graphs

% An example using subplot
figure()
for r = 1:2
    for c = 1:2
        subplot(2,2,(r-1)*2+c) % work on the (r-1)*2+c plot in a 2x2 grid
        if mod(c, 2) == 0
            stem([1:10], randn(10,1),'m+');
            title('stem plot');
        else
            plot(randn(10,1));
            title('regular line plot');
        end
    end
end

%% Examples of other common plot commands 

% semilogx and semilogy
t = linspace(0, 2*pi, 200);
semilogx(exp(-t),t); grid;
semilogy(t,exp(t)); grid

% a bode plot!
H = @(w) 20*(1i*w).*(1i*w + 100)./((1i*w+2).*(1i*w+10));
w = logspace(-3, 3, 200);
semilogx(w, 20 * log(abs(H(w)))); grid

%loglog
t = linspace(0, 2*pi, 200);
x = exp(t);
y = 100 + exp(2*t);
loglog(x,y, 'LineWidth', 2); grid

%polar
t = linspace(0, 2*pi, 200);
r = sqrt(abs(2*sin(5*t)));
p = polar(t,r, 'g');
set(p, 'LineWidth', 2);

% bar (see also hist and histc)
x = linspace(0, 1, 200);
y1 = x.^2.*(1-x).^4;
y2 = x.^2.*(1-x).^6;
y3 = x.^5.*(1-x).^5;
bar(x,[y1',y2',y3'])
title('likelihood functions for two different datasets')
legend('2 heads, 4 tails', '2 heads, 6 tails', '5 heads, 5 tails');
shg

% errorbar
%{
x = -1:.01:1;
acosapprox = invtrigseries(x, 6, 'acos');
er = acosapprox - acos(x);
errorbar(x, acosapprox, er); % symmetric error bars
errorbar(x, acosapprox, er.*(er <0), er.*(er >0)); % more accurate
%}

% barh
randseq = randn(1,10e6);
binpercent = @(l, h) sum((randseq >= l) & (randseq <=h))/length(randseq);
top = [-4 -2  0  2  4 100];
bot = [-100 -4 -2 0 2 4];
amount = NaN(size(bot));
for i = 1:length(bot)
    amount(i) = binpercent(bot(i), top(i));
end
barh(amount, 'stacked');
xlabel('Percentage of random vector')
shg

% pie
pie(amount); shg

% stem
stem(rand(1,100));

% plotyy - like subplot, manages a pair of axes for you
x = -5:.1:5;
y1 = sinh(x);
y2 = cosh(x);
ax = plotyy(x, y1, x, y2);
xlabel('x');
hy1 = get(ax(1), 'ylabel');
hy2 = get(ax(2), 'ylabel');
set(hy1, 'string', 'sinh(x)');
set(hy2, 'string', 'cosh(x)');

%plotyy with different scalings/plot commands
plotyy(x,y1,x,y2,@stem,@semilogy)

% trimesh
pts = rand(50,2); 
tris = delaunay(pts);
trimesh(tris,pts(:,1),pts(:,2)); shg

% area, hist, stairs, compass, comet, contour, quiver, pcolor  ...

%% 3D Plotting

% plot3
plot3(rand(1,100),rand(1,100),rand(1,100))

%meshgrid
r = linspace(0,2,100);
[x,y]=meshgrid(r,r);
z = cos(x).*sin(2*y);

% surf
surf(x,y,z); shg

% mesh
mesh(x,y,z); shg

% trimesh
pts = rand(50,3); 
tris = delaunay(pts(:,1:2));
trimesh(tris,pts(:,1),pts(:,2),pts(:,3),'FaceColor',[0.7 0 0.3]); shg

% ez plots
ezplot3(@(t) cos(t), @(t) sin(3*t), @(t) cos(5*t),[0 10 0 10]); shg
ezsurf(@(x,y) cos(x).*sin(2*y),[0 10 0 10])

%% Saving / Loading figures

% Once you've created a figure, you may print it, export it to file, or
% save it for later loading in Matlab

% fig files
close all;
plot(rand(4));
hgsave(gcf, 'myfig'); % creates a .fig file, which is matlab's own format

saveas(gcf,'myfig','fig'); %the same

hgload myfig

% If you want to export to other file formats, use the menu in the figure
% window, or use saveas, e.g.
saveas(gcf, 'filename.eps')

% In general, you can use the figure window to manipulate all the
% parameters of your graphs after they've been created.

% A useful feature is the "Create Code" option from 
% the figure window. This lets you play with the GUI and then obtain
% code to produce a similar style

%% Images (if time)

% Images are represented as matrices in MATLAB. Colormapped/Indexed images 
% are 2 dimensional matrices with integer entries. Each entry is an index 
% into the variable colormap. colormap may have a variable length, but
% it has 3 columns, one each for R,G,B (varying from 0 to 1). 
%
% True color (RGB) images are stored as 3 dimensional (m-by-n-by-3)
% arrays. 
%
% Matlab stores images using either doubles or fixed integers (uint8 or
% uint16). In the former case, true color images have entries in the range
% 0,1 which represent how much of each color is present. In the latter
% case, true color images have integer entries in the range [0,255] (uint8) 
% or [0,65535] (uint16).
%
% The entries in colormaps for indexed images of class double are in the 
% range [0,1], while the entries in colormaps for indexed images of class
% uint8 are integers in [0,255] (for uint16, integers in [0,65535])
%
% to tell the class of a variable, use
% class(var)
% to convert between classes, use
% uint8(var)
% uint16(var)
% double(var)

% load images with imread

% a b&w indexed image
[imbarbara, cmapbarbara] = imread('barbara.bmp');
image(imbarbara); shg % create a new figure file with the image in it
colormap(cmapbarbara); % set the colormap
colormap('jet'); % matlab has some built in colormaps; see the help
c = colormap; % returns current colormap

% a color indexed image
[immarbles, cmapmarbles] = imread('marbles.gif');
image(immarbles);
colormap(cmapmarbles);
colormap(cmapbarbara); % no reason I can't use barbara's color map

% Ex: how to turn the nose of the mandril blue?
load mandrill
image(X); shg
colormap(map) % the colormap for the mandrill

% to view a double array as an image without worrying about converting its
% entries to appropriate integers into a colormap, use imagesc and one of
% the default colormaps

fftX = fftshift(abs(fft2(X)));
imagesc(log(fftX)); 
colormap(gray)

% an RGB image

horseim = imread('horse.jpg');
subplot(2,2,1)
image(horseim); % rgb image (no need for a colormap)
subplot(2,2,2)
imagesc(horseim(:,:,1));
colormap('gray');
subplot(2,2,3)
imagesc(horseim(:,:,2));
colormap('gray');
subplot(2,2,4)
imagesc(horseim(:,:,3));
colormap('gray');

% you might need to convert to double, if you're going to do complicated
% arithmetic--- to do so, use im2double
% it scales the data to [0,1], so e.g. if the image is type uint8, then
% 0 -> 0 and 255 ->1.0

im = imread('lena.bmp');
imdouble = im2double(im);
max(im(:)) % maximum pixel value in im (type uint8)
max(imdouble(:)) % maximum value in the converted scaled image

% Convert to grayscale by replacing (r,g,b) with sqrt(r^2+g^2+b^2)
figure();
grayhorse = sqrt(sum(im2double(horseim).^2, 3)); % convert to double!
imagesc(grayhorse);
colormap('gray');


% Once you've loaded an image, you can manipulate it as a matrix
% when you're done, you can write it back out using imwrite
% for indexed images:
% imwrite(x, colormap, filename, fmt)
% for true color:
% imwrite(x, filename)

% e.g. write out a bw image of the grayscale horse
imwrite(grayhorse, 'grayhorse.jpg');

% write out a true color image of the marbles (originally indexed)
red_lut   = cmapmarbles(:, 1);
green_lut = cmapmarbles(:, 2);
blue_lut  = cmapmarbles(:, 3);

indices_into_luts = immarbles(:)+1;
immarbles_redcomponent = ...
    reshape(red_lut(indices_into_luts),   size(immarbles));
immarbles_greencomponent = ...
    reshape(green_lut(indices_into_luts), size(immarbles));
immarbles_bluecomponent = ...
    reshape(blue_lut(indices_into_luts),  size(immarbles));

% recall that matlab stores matrices in column-major format as one large 
% vector; it does the same for 3d arrays (and more general tensors):
% each (:,:,i) "slice" is stored as a long vector vec(i), and these
% vectors are concatenated together to form a long vector 
% [vec(1) vec(2) .... ]
% So the following line stacks the rgb layers together appropriately to
% form a three layer truecolor image.
immarbles_truecolor = reshape([immarbles_redcomponent, ...
    immarbles_greencomponent, immarbles_bluecomponent], ...
    size(immarbles,1), size(immarbles, 2), 3);

imwrite(immarbles_truecolor, 'truecolormarbles.jpg');

