function x =linalg101DenseSolve(A,b)

[L,U] = lu(A);

x = U\(L\b);


end