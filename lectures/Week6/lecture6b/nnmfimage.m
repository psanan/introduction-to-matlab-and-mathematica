%% Comparison between Nonnegative Matrix Factorization and truncated SVDs
%% for image compression

close all; clear; clc

%% Load the image, view it and its spectrum
load gatlin; %this is gatlin.mat (somewhere)
I = X;
clear X map caption;
[U,S,V] = svd(I);

figure();
image(I);
colormap(gray);
title('Gatlin image');

figure();
plot(diag(S));
title('Spectrum of gatlin image');

%% View the truncated SVDs and NNMF approximations
% to save on computations, also compute the errors needed in the next task

klist = [5 10 20 40 80 100 200];
for kidx = 1:length(klist)
    k = klist(kidx);
    figure();
       
    % calculate the SVD approximation, the number of negative pixels it
    % contains, and the approx error
    Sprime = S;
    Sprime(k+1:end, k+1:end) = 0;
    Isvd = U*Sprime*V';
    negcount(kidx) = sum(sum(Isvd<0));
    svderr(kidx) = norm(I - Isvd, 'fro');
    
    subplot(2,1,1);
    image(Isvd);
    title(['k = ' num2str(k) ' truncated SVD approximation' ...
           ' of Gatlin image (' num2str(negcount(kidx)) ' negative pixels)']);
    colormap(gray);
    
    % calculate the NNMF approximation and the approx error
    W = U(:, 1:k);
    W(W < 0) = 0;
    H = V';
    H = H(1:k, :);
    H(H < 0) = 0;
    [W,H] = nmf(I, W, H, 10e-6, 180, 100000);
    Innmf = W*H;
    nnmferr(kidx) = norm(I - Innmf, 'fro');
    
    subplot(2,1,2);
    image(Innmf);
    title(['k = ' num2str(k) ' NNMF approximation of Gatlin image']);
    colormap(gray);
    
end

%% Compare the SVD and NNMF errors
figure();
plot(klist, svderr, 'b*-', klist, nnmferr, 'ro-');
title('||\cdot||_F errors in approximating the Gatlin image');
xlabel('rank of approximant');
legend('SVD approx err', 'NNMF approx err');
print('-dpng','nnmf-svd-comparison.png');
