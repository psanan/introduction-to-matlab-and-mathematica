%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% ACM 11 : Week 6                                                         %
%     Lecture 6b : Linear Algebra                                         %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear; clc

%% Logarithmic plots and convergence rates

% Convergence is a simple enough concept but is frequently glossed over.

% We'll present two common uses,
% which actually use different notions of convergence. Let's examine both,
% and see how they look with various scalings of the axes. 

% This is a review of some of the plotting functions we saw last time
%  and will help us interpret some of the timings we'll do later

close all;

% some test values of n
nlog = logspace(0,5,20)'; % 10^1 to 10^5 in 20 increments evenly spaced in
                          % the exponent
nlin = linspace(0,1000,20)'; % 0 to 1000 in 20 increments, linearly spaced

% very slow (sub-algebraic) convergence
slow = 1./log(nlog);

% algebraic convergence
algLinear = nlog.^-1;
algQuadratic = nlog.^-2;
algCubic = nlog.^-3;

% geometric convergence
geomFirstOrder = exp(-nlin);
geomSecondOrder = exp(-2*nlin);
geomThirdOrder = exp(-3*nlin);

% extremely fast (super-geometric)convergence
fast = exp(-nlin.^2); % 'second order exponential convergence'
ludicrous = exp(-exp(nlin)); 

dataLog = [algLinear,algQuadratic,algCubic],
dataLin = [geomFirstOrder,geomSecondOrder, geomThirdOrder];

figure('Position',[0 0 1200 700]);
    subplot(1,2,1); 
    semilogy(nlog,dataLog); hold all
    semilogy(nlin,dataLin);
    xlim([1 1000])
     legend('alg1','alg2','alg3','geom1','geom2','geom3')
    subplot(1,2,2); 
    loglog(nlog,dataLog); hold all
    loglog(nlin,dataLin);
    ylim([1e-50,1]) 
    legend('alg1','alg2','alg3','geom1','geom2','geom3')
     
    
% Grdient Descent and Newton's method
% Recall the experiments from hw2. Gradient descent provides iterates which
% converge with GEOMETRIC order 1, and Newton's method with GEOMETRIC order
% 2
f = @(x) cosh(x/2) + atan(x).^2;
fp = @(x) 0.5*sinh(x/2) + 2*atan(x)./(1+x.^2);
fpp = @(x) 0.2*cosh(x/2) + (2 - 4*x.*atan(x))./((1+x.^2).^2);

xCurr = 1;
xValuesN = NaN(1,20);
for i=1:20
    xValuesN(i) = xCurr;
    xCurr = xCurr - fp(xCurr)/fpp(xCurr);
end

xCurr = 1;
xValuesGD = NaN(1,20);
for i=1:20
    xValuesGD(i) = xCurr;
    s = - fp(xCurr);
    while( f(xCurr + s) > f(xCurr))
       s = s * 0.5; 
    end
    xCurr = xCurr + s; 
end

figure
semilogy([abs(xValuesN)',abs(xValuesGD)']);
xlabel('iteration'); ylabel('error');
legend('Newton','GD');
 
% An extremely useful fact is that if a function is C^k, its Fourier series
% converges with ALGEBRAIC order k

% If a function is C^infinity, the series converges super-agebraically.
% This is sometimes called "spectral convergence", and working with series
% with this property gives rise to the field of "spectral methods".

%% Linear Algebra 101
% basic commands


rank
det
inv
cond
norm
orth
null


% Gaussian elimination
A = rand(10);
b = rand(10,1);
[L,U] = lu(A);
norm(L*U-A,'inf') % max elementwise error
% now solve Ly = b, Ux = y with forward/back substitution


%% Solving Linear Systems

% The naive way
naiveDenseSolve = @(A,b) inv(A)*b;
[ts_naive,ns_naive] = generateTiming(naiveDenseSolve);

% the 'linear algebra 101' way
% linalg101DenseSolve.m
[ts_101,ns_101] = generateTiming(@linalg101DenseSolve);


% The standard way
% The backslash operator attempts to decide what solution method
% will work well. It should be your first choice most of the time.
standardDenseSolve = @(A,b) A\b;
[ts_std,ns_std] = generateTiming(standardDenseSolve);

close all;
loglog(ns_naive,ts_naive,'ro',ns_101,ts_101,'m+',ns_std,ts_std,'b+');
xlabel('n');
ylabel('t [seconds]');
legend('Naive','linalg101','Standard'); shg

% More control
linsolve

% when would the naive ways actually be better?
%  - when A is very small
%  - when you need to solve for O(N) different b values (though factoring A
%  is even better)

% fun command for sparse solves:
help spparms
spparms('spumoni',1); %<-- how'd it do it?
A = sprand(100,100,0.1);
b = rand(100,1);
x = A\b;
spparms('spumoni',0);

% sparse direct methods (see 'Matrix Decompositions' below)

% sparse iterative methods
pcg
gmres

% preconditioners
ilu
ichol

% Least squares solvers (more when we discuss regression)
A = rand(10,20);
b = rand(10,1);
x =lsqr(A,b);

%% Matrix Decompositions

A = rand(10);

% LU
lu
[L, U, P] = lu(A) % the LU decomposition (with pivoting P*A = L*U )

% the Jordan canonical form
jordan
% "Anything the Jordan form can do, the Schur form can do better" 
%       -Van Loan [imprecisely remembered]
% That is, this function is not for use in anything other than tests

% The SVD - the sledgehammer of numerical analysis
svd

% Example application of SVD to image compression
% retain only the top singular values and vectors

load durer
I = ind2gray(X, map);
[U,S,V] = svd(I);
figure();
imagesc(I)
colormap(gray)
figure();
spy(S) % view the sparsity pattern of the singular value matrix
lastsvalidx = ceil(length(diag(S))*.1); %take 10% of the singular vectors
S(lastsvalidx:end, lastsvalidx:end) = 0;
figure()
spy(S)
figure();
imagesc(U*S*V')
colormap(gray)

disp('original number of pixels:');
disp(prod(size(I))) 

disp('size of compressed data:');
disp(...
sum(S(:)>0)*(size(U,1) + size(V,1) + 1)... % singular values + singular vectors
)

% Polar Decomposition
[U,S,V] = svd(A);
R = U*V'
Y = V*S*V'
% there are better ways to compute this!

% Diagonalization
[V,D] = eig(A)


% Cholesky Decompostions
chol
L = chol(A, 'lower') %Cholesky decomposition (L is lower triangular)

% QR and LQ decompositions
[Q,R] = qr(A)
[Qt,Lt] = qr(A')

% Schur Normal Form
[U,T] = schur(A)

% Non-negative matrix factorization
nnmf
nmf %better!

% demo - nnmfimage.m

%% Eigenvalue Computation

% For small matrices, you can perform a complete eigendecomposition
A = gallery('binomial', 7); %<-- quick way to get many common matrices!
[V,D] = eig(A);

% For sparse matrices, you can calculate the extremal eigenvalues only
A = sprand(1000,1000,0.01);
A = A+A';
[V,D] = eigs(A,10);
