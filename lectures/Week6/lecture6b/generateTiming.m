function [times, nvalues] = generateTiming(g)
% GENERATETIMING runs a (fixed) set of tests for a function g
%   g must be a function g(A,b) where A is a square matrix and b is a
%   compatible column vector

    maxexp = 3;
    nvalues = floor(logspace(1,maxexp,2*maxexp));
    nTests = length(nvalues);
    times = NaN(nTests,1);
    
    for i=1:nTests
       n = nvalues(i);
       [A,b] = generateTestSystem(n); 
       tic;
       g(A,b);
       times(i) = toc;
    end

end