\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}
	%\usepackage{attachfile}
	\usepackage{verbatim}

\begin{document}

\title{Introduction to Mathematica and MATLAB}
\author{Patrick Sanan}
\date{}
\maketitle

This document contains course information and guidelines for ACM 11, a one-quarter undergraduate course offered at Caltech in the Spring of 2013. Note that some outdated specifics have been omitted, including the URL of the course website and the homework submission email address. These should be updated for a new offering of the course.
%\paragraph{ACM 11. Introduction to Matlab and Mathematica}\emph{ 6 units (2-2-2); third term. Prerequisites: Ma 1 abc, Ma 2 ab. CS 1 or prior programming experience recommended}. Matlab: basic syntax and development environment; debugging; help interface; basic linear algebra; visualization and graphical output; control flow; vectorization; scripts, and functions; file i/o; arrays, structures, and strings; numerical analysis (topics may include curve fitting, interpolation, differentiation, integration, optimization, solving nonlinear equations, fast Fourier transform, and ODE solvers); and advanced topics (may include writing fast code, parallelization, object-oriented features). Mathematica: basic syntax and the notebook interface, calculus and linear algebra operations, numerical and symbolic solution of algebraic and differential equations, manipulation of lists and expressions, Mathematica programming (rule-based, functional, and procedural) and debugging, plotting, and visualization. The course will also emphasize good programming habits and choosing the appropriate language/software for a given scientific task.

\section{Course Logistics}

The course will be divided into two halves, covering Mathematica and MATLAB, respectively. Your midterm will be a small project in Mathematica, and your final project will be a larger-scale project in MATLAB. 

\paragraph{Course Website}
This course uses \href{https://piazza.com}{Piazza} (a web-based message board) for our course website. %Please enroll yourself to use the website at \href{https://piazza.com/caltech/spring2013/acm11/home}{https://piazza.com/caltech/spring2013/acm11/home} . 
Instead of emailing the instructor or TA about course material or homework assignments, please post questions on the Q\&A section of the class Piazza page. 
Note that you can post private messages which only the instructors can see, and you have the option of posting anonymously. While the instructor and TA will check this board periodically, you should not count on immediate responses, especially close to the due date for assignments. 

\paragraph{Homework}
Homework assignments will be assigned weekly (beginning from the first week) and due a week later. There will be 7 homework assignments. Your lowest score (which may be a zero from an unsubmitted assignment) will be dropped and the others weighted equally to compute your homework score. No late work will be accepted without explicit permission from the instructor \emph{prior to the due date}. Follow the homework guidelines in section \ref{sec:hwGuidelines}.

\paragraph{Grading}
Your final grade will be a weighted combination of your homework, midterm, and final scores. We reserve the right to curve the final scores, but do not anticipate doing so.
\begin{itemize}
\item Homework: 50\% 
\item Midterm: 20\%
\item Final: 30\%
\end{itemize}

\paragraph{Code}
It is important that you write all submitted code yourself, except when explicitly asked to incorporate some existing material. Code copied from the internet without citation will be considered plagiarism, and code copied with citation will not be considered for credit. Note that in this course, you will be graded on clarity of presentation as well as correctness of your code - undocumented code is largely useless in the `real world', and sparsely documented code unfortunately fairs little better. 

\paragraph{Collaboration Policy}
Students are encouraged to discuss the material and assignments at the conceptual level, and general discussion of the homework problems is allowed. Students may help one another with coding issues, but should never send another student code or write code for them. To reiterate, sharing, copying, and refactoring of code is \emph{not allowed} in this course. Most of your graded work will be code, and is important that you write it yourself. Copying code from the internet or another source without citation is plagiarism. No collaboration is allowed on the midterm and final projects.  

\paragraph{Textbooks}
There is no required textbook for this course. Most of what you will need is contained in the course notes and in the extensive documentation included with MATLAB and Mathematica. Some useful books by the creators of the two environments are
\begin{itemize}
\item \emph{The Mathematica Book} by Stephen Wolfram\\ (several copies available at SFL, and the entire book is in Mathematica's included documentation)
\item \emph{Numerical Computing with MATLAB} by Cleve Moler \\(available at \href{http://www.mathworks.com/moler/chapters.html}{http://www.mathworks.com/moler/chapters.html})
\end{itemize}

\section{Homework Guidelines} \label{sec:hwGuidelines}

One of the aims of the course is to teach you how to produce code that is intelligible, reusable, and easily modifiable by other people. Note that even if you never intend to share a script or piece of code, returning to your own code months later is quite similar to reading someone else's work. A second aim is that the TA be able to grade your assignments without too much anguish. These two desiderata motivate most of the following requirements for  your homework assignments.

\begin{itemize}
\item All homework should be submitted to the TA, by email, before the end of the day due (that is, at 23:59:59 or earlier, local time).%, to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com} . 
The subject line of the email should include the name of the assignment and your full name, for example \texttt{Homework 1 - Patrick Sanan} or \texttt{Sanan, Patrick - Midterm}. 
\item Your submission should consist of a single file sent to the above email address. 
\begin{itemize}
\item The name of the submitted file must start with your last name and include your full name and the name of the assignment. For example, \texttt{SananPatrickHw1.nb} or \texttt{SananPatrickFinal.zip} are fine. 
\item You may resubmit a revised version of an assignment before the due date, and in this case you should append a version number to the end of your filename, for example \texttt{SananPatrickHw6-v4.zip} would be my fourth version of Homework 6. The TA will only grade the latest revision.
\item If more than one file is requested by the assignment, submit a \texttt{.zip} file. Make sure it expands to a directory (with the same name, minus the extension), not a collection of files. 
\end{itemize}
\item Your code must run without modification by the TA.
\begin{itemize}
\item It is a good idea to test your code immediately before submitting (after making even what you think is a very minor typo fix), and to do so ``from scratch'', that is from an instance of Mathematica or MATLAB with nothing defined in the global workspace.
\item If you run out of time and cannot get your code to work properly, it is better to submit running code which produces partial results than `complete' code which does not run at all. You can add comments and commented-out partial code for things you couldn't get to work properly, but the TA will only give partial credit if these are understandable. 
\end{itemize}
\item Your code must be readable by a human, and produce intelligently-formatted output. 
\item Comments and documentation should be written in clear English. 
\item Submitted Mathematica notebooks (\texttt{.nb} files) should not include any output, unless specifically requested. This can be done easily by choosing ``Delete All Output'' from the ``Cell'' menu.
\item Submitted Mathematica notebooks should produce all your results when the TA evaluates the entire notebook in Mathematica (test this out by restarting the Mathematica kernel and choosing ``Evaluate Notebook'' from the ``Evaluate'' menu). 
\item MATLAB scripts should run from the command line without modification or (unless stated otherwise) definition of any global variables.
\item MATLAB functions should not produce any output unless specifically required to do so.
\end{itemize}

\section{Approximate Syllabus}

\paragraph{Week 1}
\begin{itemize}
\item Introduction to Mathematica. 
\end{itemize}

\paragraph{Week 2}
\begin{itemize}
\item How Mathematica works
\end{itemize}
 
\paragraph{Week 3}
%symbolic solution of algebraic and differential equations,
\begin{itemize}
\item Applications of Mathematica
\end{itemize}

\paragraph{Week 4}
\begin{itemize}
\item Advanced Mathematica 
\item  Midterm assigned %Thursday, April 25
\end{itemize}

\paragraph{Week 5} 
\begin{itemize}
\item Introduction to MATLAB
\item Midterm due 
\end{itemize}

\paragraph{Week 6} 
\begin{itemize}
\item How MATLAB works, Numerical Linear Algebra
\end{itemize}

\paragraph{Week 7} 
\begin{itemize}
\item MATLAB Topics 1: Vectorization and Optimization
\end{itemize}

\paragraph{Week 8} 
\begin{itemize}
\item MATLAB Topics 2 : ODE and PDE
\end{itemize}

\paragraph{Week 9} 
\begin{itemize}
\item MATLAB Topics 3 : Signals, Data, and Statistics 
\item Final project assigned 
\end{itemize}

\paragraph{Week 10}
\begin{itemize}
\item Object-Oriented Features, Parallelism, and Compiled Functions
\item Final due for seniors 
\end{itemize}

\paragraph{Finals Week}
\begin{itemize}
\item Final due for non-seniors. 
\end{itemize}
\end{document}
