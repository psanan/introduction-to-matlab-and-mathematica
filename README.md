# Introduction to MATLAB and Mathematica

Course materials from ACM 11, as taught at Caltech
in Spring 2013.

This material is based on materials from previous iterations
of the course by Stephen Becker and Alex Gittens, with help
from Eldar Akhmetgaliev.

For more on the course, see the Course Guide.

For now, I have not included the homework solutions,
though we have these and can make them available to
instructors.

You are free to modify and use these notes, but you must do
so in accordance with the license included. My intent is that
you can use them as you wish but must properly cite this
as the source and cannot republish this content in any non-free way.

If you would like to use these notes for your own teaching,
I'd appreciate you contacting me; these lectures
are based on the versions of MATLAB and Mathematica
available at the time the course was taught, so I would 
appreciate any patches to update them.

-Patrick Sanan