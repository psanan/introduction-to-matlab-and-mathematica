\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}


% ---- New Commands --- %
	% Derivatives
	\newcommand{\pt}[2]{\frac{\partial{#1}}{\partial{#2}}}
	\newcommand{\ptn}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}
	\newcommand{\dv}[2]{\frac{d #1}{d #2}}
	\newcommand{\dvn}[3]{\frac{d^{#3} #1}{d #2^{#3}}}

	\newcommand{\eps}{\varepsilon}
	\newcommand{\tr}{\text{tr}}
	\newcommand{\x}{\times} % cross 
	\newcommand{\ox}{\otimes} % direct product
	\newcommand{\mbf}[1]{\mathbf{#1}} %math bold face
         \newcommand{\hh}{\frac{1}{2}} % one half
         \newcommand{\argmin}{\text{argmin}}
         \newcommand{\w}{\wedge} %wedge
         \newcommand{\W}{\bigwedge} %big wedge

	% Spaces
	\newcommand{\R}{\mathbb{R}} %R
	\newcommand{\RR}{\mathbb{R}^2} %R^2
	\newcommand{\RRR}{\mathbb{R}^3} %R^3
	\newcommand{\Rn}{\mathbb{R}^n} %R^n
	\newcommand{\C}{\mathbb{C}} %C
	\newcommand{\SO}{\text{SO}} %SO
	\newcommand{\GL}{\text{GL}} %GL

	% Matrix Shortcuts
	\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
	\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
	\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
	\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}
	
	% Text Styles
	\newcommand{\red}[1]{\textcolor{red}{#1}} % red text


% For commenting out solutions
	\newif\ifanswers
	\answerstrue % comment out to hide answers


\begin{document}

\begin{center}
\bf{\Large ACM 11, Spring 2013, Final Project}\\
Due Sunday, June 9, 2013 (seniors) \\
Due Thursday, June 13, 2013 (non-seniors)
\end{center}


Submit by 23:59:59 PDT, as a \texttt{.zip} file \texttt{LastnameFirstnameFinal.zip}, expanding to a directory of the same name, to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com}, with a subject line including your full name and ``Final''. 

\section*{Final Project}

Choose a contained computational problem to solve with MATLAB, either from the suggestions below, from a scientific area you're interested in, or from your imagination. The project must be your own work - if you're inspired by something you've seen online or elsewhere, please discuss with the instructors to make sure you'll be modifying it enough.  There is no collaboration allowed, but please feel free to contact the instructors if you have problems. The best projects will demonstrate your facility using MATLAB to create a useful, reusable, and efficient set of tools to solve a particular problem. 

\subsection*{ Requirements}
\begin{itemize}
\item Vectorize where possible. 
\item Use sparse matrices where appropriate.
\item Comment and document  code properly. Provide help text and use cells to organize. 
\item Produce well-structured and readable code. Use script and function m-files to provided encapsulated and reusable functionality.
\item Include an obvious test script and/or `Readme' file
\item Produce useful and properly labelled plots and visualizations
\item Use at least one of the following features: compiled mex files, object-oriented features, cell arrays, or GUI design. 
\end{itemize}
 
\section*{Detailed Project Idea: Build Your Own MP3}

Hugely successful formats for (lossy) audio compression are based around the idea of producing a time-frequency representation of the audio signal and throwing away inaudible information. Mp3, Ogg Vorbis, AAC, and other common formats use the \emph{Modified Discrete Cosine Transform} (MDCT) on overlapping blocks of the audio signal, storing only a subset of the resulting coefficients. The \href{http://en.wikipedia.org/wiki/Modified_discrete_cosine_transform}{Wikipedia article on the topic} is quite good. Note that an MDCT performed on a signal of length $2N$ produces only $N$ coefficients.

Audio files and copies of the third-party code you'll need are on the course website as \\ \texttt{audioCompressionFiles.zip}. 

\begin{enumerate}[(a)]
\item Download \texttt{mdct4.m}, \texttt{imdct4.m}, \texttt{kbdwin.m} and \texttt{kbdwin.c} from \href{http://www.ee.columbia.edu/~marios/mdct/mdct_giraffe.html}{Marios Athineos}, or from the course website. 
\item Compile the \texttt{kbdwin} function (run \texttt{mex -setup} and then \texttt{mex kbdwin.c}). 
\item Write a function m-file \texttt{compress.m} which defines a function which accepts a vector \texttt{x}, an even block size \texttt{b}, and a compression ratio \texttt{c} and returns a sparse matrix \texttt{Xc}. It should perform the following tasks, without using any loops:
\begin{enumerate}
\item Pad the signal with zeros to ensure that its length is a multiple of \texttt{b/2}.
\item Do the following for blocks of length \texttt{b}, starting at intervals of \texttt{b/2} (That is, the blocks overlap by a factor of 2). 
\begin{enumerate}
\item `Window' the block data by multiplying it elementwise with a \href{http://en.wikipedia.org/wiki/Kaiser_window#Kaiser-Bessel_derived_.28KBD.29_window}{KBD window function} of length \texttt{b}, produced with the \texttt{kbdwin} function you compiled, with $\alpha = 2.5$. 
\item Use \texttt{mdct4} to transform this windowed data, and store the result in a column of a (dense) matrix \texttt{X}
\end{enumerate}
\item Form a \emph{sparse} matrix \texttt{Xc} by retaining a fraction equal to \texttt{c} of the largest magnitude entries of \texttt{X}. 
\end{enumerate}
\item Write a function m-file \texttt{resynthesize.m} which defines a function which accepts a sparse matrix \texttt{Xc} and returns a vector \texttt{xr}. It should perform the following tasks, without using any loops:
\begin{enumerate}
\item Perform an inverse MDCT on each column of \texttt{full(Xc)} with \texttt{imdct4}.
\item `Window' the resulting blocks again as above. 
\item Recombine the overlapping blocks into \texttt{xr}. 
\end{enumerate}
\item Write a function m-file \texttt{experiment.m} which accepts a filename, a compression ratio, and a block size, and which performs the following tasks 
\begin{enumerate}[(i)]
\item Load the audio file using \texttt{audioread}.
\item If needbe, sum the columns of the resulting data to produce a single-channel (mono) signal.
\item Pad the signal by adding \texttt{b/2} zeros to each end.
\item Compress the audio using your \texttt{compress} function with the provided compression ratio and block size.
\item Plot a spectrogram of the compressed sound by visualizing the compressed data using \texttt{imagesc}. Ensure that low frequencies are plotted at the bottom of the plot, and time proceeds left to right.
\item Reconstruct an approximate signal using your \texttt{synthesize} function. 
\item Using \texttt{soundsc}, play the sound  \footnote{don't forget to specify the sample rate or it may take a long time to play!} and the reconstruction \footnote{It's also interesting to listen to the difference between the sound and its reconstruction - note that the lengths will be different}. Use \texttt{pause} to ensure that the sounds play one after another.
\end{enumerate}
\item Write a script m-file \texttt{tests.m} which runs the \texttt{experiment} for various values of \texttt{b} and \texttt{c} and at least four different audio files, including \texttt{birds\_sec.wav}, \texttt{voice.wav} and \texttt{nin\_ghosts1\_piano\_clip.wav} provided on the course website \footnote{These are all open-source audio files of one kind or another, from Tom Erbe at UCSD, \href{http://www.ninremixes.com/multitracks.php}{ninremixes.com}, and \href{http://www.freesound.org/people/jus/sounds/73617/}{freesound.org} }, and one of your own choosing. For each, provide some illuminating settings for the block size (use powers of 4. 1024 is a good place to start) and compression ratio, and include some comments. Which signals can be compressed more before you hear audible artifacts? What's the most you can compress the voice sample before it's unintelligible? 

\end{enumerate}

\section*{Other Project Ideas}

Here are some suggestions for more open-ended projects; you may also design your own (feel free to discuss with the instructors if you want some more guidance or have questions about how much to include). 

%\paragraph{The Solar System}
%Revisit the three body gravitational problem for the idealized solar system of the sun, moon, and earth, and aim to compute the solution for a very long period of time. For this reason we would like to use a numerical method which  respects important conserved quantities like momentum and energy, and important topological features of the solutions (close orbits). Integrate the equations of motion using the \href{http://en.wikipedia.org/wiki/Semi-implicit_Euler_method}{Symplectic Euler} method and compare the long-time results with the forward and backward Euler methods.

\paragraph{Photo Shop}
Use MATLAB to build your own set of photo touchup tools. Write functions to crop, rotate, resize, adjust  input/output curves (which includes brightness/contrast), and sharpen with an \href{http://en.wikipedia.org/wiki/Unsharp_masking}{unsharp mask}. Give some simple examples of using your tools to load, touch up, and save an image.

\paragraph{Biological Image Processing}
Email Patrick to obtain a new set of 4D microscope data, which can be processed in several interesting ways!

\paragraph{Interactive Game}
Use MATLAB's GUI-building tools to design a simple interactive game, such as tic-tac-toe. 

\end{document}
