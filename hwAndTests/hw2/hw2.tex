\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}


% ---- New Commands --- %
	% Derivatives
	\newcommand{\pt}[2]{\frac{\partial{#1}}{\partial{#2}}}
	\newcommand{\ptn}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}
	\newcommand{\dv}[2]{\frac{d #1}{d #2}}
	\newcommand{\dvn}[3]{\frac{d^{#3} #1}{d #2^{#3}}}

	\newcommand{\eps}{\varepsilon}
	\newcommand{\tr}{\text{tr}}
	\newcommand{\x}{\times} % cross 
	\newcommand{\ox}{\otimes} % direct product
	\newcommand{\mbf}[1]{\mathbf{#1}} %math bold face
         \newcommand{\hh}{\frac{1}{2}} % one half
         \newcommand{\argmin}{\text{argmin}}
         \newcommand{\w}{\wedge} %wedge
         \newcommand{\W}{\bigwedge} %big wedge

	% Spaces
	\newcommand{\R}{\mathbb{R}} %R
	\newcommand{\RR}{\mathbb{R}^2} %R^2
	\newcommand{\RRR}{\mathbb{R}^3} %R^3
	\newcommand{\Rn}{\mathbb{R}^n} %R^n
	\newcommand{\C}{\mathbb{C}} %C
	\newcommand{\SO}{\text{SO}} %SO
	\newcommand{\GL}{\text{GL}} %GL

	% Matrix Shortcuts
	\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
	\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
	\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
	\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}
	
	% Text Styles
	\newcommand{\red}[1]{\textcolor{red}{#1}} % red text


% For commenting out solutions
	\newif\ifanswers
	\answerstrue % comment out to hide answers


\begin{document}

\begin{center}
\bf{\Large ACM 11, Spring 2013, Homework 2}\\
Due Thursday, April 18, 2013. \\
\end{center}


Submit by 23:59:59 PDT, as a single Mathematica notebook file \texttt{LastnameFirstnameHw2.nb}, to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com}, with a subject line including your full name and ``Homework 2''.  \\

The following problem should be completed with Mathematica. Your submitted notebook should use section  and subsection cells for separate questions and subquestions. Please follow all the other guidelines  in the course guide (section 2). To make sure your notebook runs smoothly when the TA chooses ``Evaluate All Cells'', consider including \texttt{Remove["Global\`{}*"]} before each question.
\section{The Euclidean Algorithm} 

\begin{enumerate}[(a)]
\item Recall the \emph{Euclidean algorithm} for computing the greatest common divisor (gcd) of a pair of natural numbers. One step of the algorithm transforms a pair of positive integers $(a,b)$ into the pair $(b,r)$, where $r < b$ is the remainder when $a$ is divided by $b$ (see \texttt{QuotientRemainder} in Mathematica). The algorithm terminates when $b = 0$, at which point $a$ is the greatest common divisor of the initial arguments. Define a function which computes one step of the algorithm. Define a second function which computes the gcd of two natural numbers, using \texttt{NestWhile} and the previous function. Do not define any global variables beyond the two functions; that is, either use \texttt{Module} to restrict any auxiliary variables to local scope, or use anonymous functions and other trickery to write `one-liners'. You can only use the built-in \texttt{GCD} function to check that your code is correct.

\item Since we have used a while loop, write down a simple argument as to why this algorithm will always terminate. Feel free to look this up on the internet or in a book, but cite your source if you do, and write your argument clearly.  
\item Check your results by computing the gcds of the following list of pairs of numbers and comparing with the results given by the built-in \texttt{GCD} function:
\begin{verbatim}
{
   {100, 17}, 
   {24324, 2342}, 
   {6170, 1234}, 
   {4598345937534953754578, 1148912640183684688920495752521232567550}
}
\end{verbatim}
\end{enumerate}

% The Euclidean Algorithm in nifty form
% Also a good chance to use FullForm, TreeForm
% Precision! Do it with different precisions, comment on results
%  Kicker - continued fractions are the best way to think about real numbers, and if they weren't so hard to add and multiple, would be how we store approximations to real numbers


\section{More on Newton's Method}
In class we saw \emph{Newton's method} for nonlinear root finding, that is finding solutions $x$ (``roots"') to $f(x)=0$. The idea behind the method is quite intuitive. Given a function $f(x)$ and an initial guess $x_0$, execute the following steps for $k = 0,1,..$
\begin{itemize}
\item Approximate $f$ by $\tilde f_k(x) = f(x_{k}) + f'(x_{k})(x-x_{k})$, its first-order Taylor series at $x_{k}$. That is, approximate it by its tangent at $(x_{k}, f(x_{k}))$. %Picture
\item Find the zero of this function, that is, solve the linear equation $\tilde f_k(x_{k+1}) = 0$ to obtain $x_{k+1} = x_{k}-\frac{f(x_k)}{f'(x_k)}$
\item Break the loop if convergence is detected, or if a maximum number of iterations is reached. \footnote{``Convergence'' here can be measured in several ways, but typically one checks that the magnitude of either $x_k-x_{k-1}$ or  $\frac{x_k-x_{k-1}}{x_k}$ is `small', or that the function value is sufficiently close to zero. In this assignment, we'll just rely on \texttt{FixedPoint}, with a limited number of iterations}
\end{itemize}

Concisely, one looks for a fixed point of the iteration $x_{k+1} = x_k - \frac{f(x_k)}{f'(x_k)}$. In the lecture notes we saw a way to use Mathematica's \texttt{FixedPoint} function to quickly compute zeros.

Newton's method can also be used to locate an extremum of a continuous function, by finding a zero of the derivative. In this case, the Newton iteration is
\begin{equation}\label{eq:newtonMinIteration}
x_{k+1} = x_k - \frac{f'(x_k)}{f''(x_k)}
\end{equation}

\begin{enumerate}[(a)]
\item Write an iteration function \texttt{newtonStep[f\_,x\_]} which computes one step of \eqref{eq:newtonMinIteration}. Describe 3 of the many things that can `go wrong' with Newton's method as described above. For example, what might happen if $f''$ is zero at a minimum? What happens if the function has multiple extrema? Are there choices of $f$ for which this iteration diverges? 
\item Write a function in Mathematica which accepts three arguments: an iteration function (such as you wrote in the previous subquestion), a function to minimize, and a list of initial values. The function should return a list of the fixed points of the provided iteration for each initial value, or terminate after 50 iterations if no fixed point is reached for a given initial value. 
\item Write a second iteration function which can be used to implement \emph{gradient descent}. Given a point $x_k$ and a function $f$, one step of the iteration should be $x_{k+1} = x_k - \alpha f'(x_k)$, where $\alpha = 2^{-m}$, and $m \in \{0,1,....,20\}$ is chosen to be as small as possible to satisfy $f(x_{k+1}) \le f(x_k)$.  This is a simple \emph{backtracking line search}. 
\item Finally, using \texttt{If}, define a third, hybrid iteration function which takes a Newton step if this will decrease the objective function, and otherwise takes a gradient descent step.
\item Consider the following three functions 
$$g(x) = x^2 + \cosh(x/2) -1 $$
$$h(x) = \cos(x) + \sin(4x) + \cosh(x/2) $$ 
$$t(x) = (1/2) \left(\arctan(x)\right)^2$$

Devise a method to compute the fixed points of all three iterations for a \texttt{Range} of starting values from -4 to 4 in increments of $0.1$, for each of the three functions above. Make sure your initial points are in machine precision. 

For each of the three methods you just defined (Newton, Gradient Descent, and ``Hybrid"), plot the three functions and their first two derivatives along with all the fixed points for the given initial values (if you observe some divergent behavior, you don't have to plot those points). Use \texttt{GraphicsRow} to nicely line up the three plots for each method, and add a large title to each set. Choose plotting options such that color is not required to interpret the graph \footnote{This is a good idea in general, since by some estimates 10\% of the male population is red/green colorblind, and even in this day and age people still occasionally print things out in black and white.}. 
\item Now consider the function $p(x) = \cosh(x/2) + \left(\arctan(x)\right)^2$. Using \texttt{ListLogPlot}, plot (on the same axes) the (absolute values of the) first 30 iterates for each of the three iteration schemes you defined, with initial value 4. Compute with 40 digits of precision. Add a legend. Comment on the results. 
\end{enumerate}

\end{document}
