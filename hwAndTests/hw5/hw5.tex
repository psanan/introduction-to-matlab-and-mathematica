\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}


% ---- New Commands --- %
	% Derivatives
	\newcommand{\pt}[2]{\frac{\partial{#1}}{\partial{#2}}}
	\newcommand{\ptn}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}
	\newcommand{\dv}[2]{\frac{d #1}{d #2}}
	\newcommand{\dvn}[3]{\frac{d^{#3} #1}{d #2^{#3}}}

	\newcommand{\eps}{\varepsilon}
	\newcommand{\tr}{\text{tr}}
	\newcommand{\x}{\times} % cross 
	\newcommand{\ox}{\otimes} % direct product
	\newcommand{\mbf}[1]{\mathbf{#1}} %math bold face
         \newcommand{\hh}{\frac{1}{2}} % one half
         \newcommand{\argmin}{\text{argmin}}
         \newcommand{\w}{\wedge} %wedge
         \newcommand{\W}{\bigwedge} %big wedge

	% Spaces
	\newcommand{\R}{\mathbb{R}} %R
	\newcommand{\RR}{\mathbb{R}^2} %R^2
	\newcommand{\RRR}{\mathbb{R}^3} %R^3
	\newcommand{\Rn}{\mathbb{R}^n} %R^n
	\newcommand{\C}{\mathbb{C}} %C
	\newcommand{\SO}{\text{SO}} %SO
	\newcommand{\GL}{\text{GL}} %GL

	% Matrix Shortcuts
	\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
	\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
	\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
	\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}
	
	% Text Styles
	\newcommand{\red}[1]{\textcolor{red}{#1}} % red text


% For commenting out solutions
	\newif\ifanswers
	\answerstrue % comment out to hide answers


\begin{document}

\begin{center}
\bf{\Large ACM 11, Spring 2013, Homework 5}\\
\bf{\Large ``Graphs and Matrices''}\\
Due Thursday, May 16, 2013. \\
\end{center}


Submit by 23:59:59 PDT, as a \texttt{.zip} file \texttt{LastnameFirstnameHw5.zip} (which expands to a directory \texttt{LastnameFirstnameHw4} containing the m-files mentioned below), to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com}, with a subject line including your full name and ``Homework 5''.  \\

Note: This assignment differs from previous ones in that you are asked to provide functions which will be tested by the grader. You should take the time to thoroughly test your code to make sure it behaves as expected - it's a good idea to work out some test cases by hand and/or generate random input and check the output in MATLAB.

\section{The Floyd Warshall Algorithm}

A weighted graph is defined by a set of vertices, a set of edges between pairs of vertices, and a scalar weight corresponding to each edge.

Let $G$ be a weighted, undirected graph on $n$ vertices with adjacency matrix $A$. That is, the $(i,j)$th entry of $A$ is the edge weight corresponding to the edge connecting vertices $i$ and $j$, and is $0$ if no edge connects the vertices\footnote{Note that in this context it might be more natural to think of these entries as infinite}. A path
\[ 
(u = p_1, p_2, p_3, \ldots, p_{k-1}, p_k= v) 
\]
 from vertex $u$ to vertex $v$ is a sequence of vertices so that $p_{i}$ and $p_{i+1}$ are neighbors (are connected by an edge) for $i=1, \ldots, k-1$. The length of this path is $\sum_{i=1}^{k-1} A_{p_i, p_{i+1}}$. The graph theoretical distance $d_{uv}$ between $u$ and $v$ is the length of the shortest path between $u$ and $v$. If $u = v$, then $d(u,v) = 0$, and if there are no paths between $u$ and $v$, then $d_{uv} = \infty$ by convention. Note that this definition only makes sense if $A$ has no `negative weight loops' - this is satisfied if $A$ has non-negative edge weights. 

For $k > 0$, let $d(k,i,j)$ denote the length of the shortest path from $i$ to $j$ that uses only vertices numbered less than or equal to $k$; when $k = 0$, set $d(0, i,j) = a_{ij}$ if $i$ and $j$ are neighbors, $d(0,i,j) = 0$ if $i=j$, and otherwise set $d(0, i, j) = \infty$. 

Clearly $d_{ij} = d(n,i,j)$. Also notice that for $k > 0$, if the shortest path between $i$ and $j$ does not include $k$, then $d(k, i, j) = d(k-1, i, j)$. If it does, then $d(k, i, j) = d(k-1, i, k) + d(k-1, k, j)$.

Floyd-Warshall's algorithm for computing graph theoretical distances uses these simple observations to efficiently compute the matrix $D$ of distances between the vertices of $G$:

\begin{center}
\textsc{Floyd-Warshall's} Algorithm \\ 
\vskip .5em
\begin{minipage}{0.9\linewidth}
\begin{tabbing}
\textbf{Output: } \= blah \kill
\textbf{Input: } \> $A$, an adjacency matrix representing the graph $G$ \\ \\ 
\textbf{Output: } \> $D$, the matrix of graph theoretical distances between the vertices of $G$.
\end{tabbing}
\begin{tabbing}
\textsl{Step 1: } \= Initialize: Construct the matrix $D^{(0)}$ with entries $D^{(0)}_{ij} = d(0,i,j).$ \\
\textsl{Step 2: } \> The main loop: \\
		  \> For $k$ from 1 to $n$: \\
		  \> \quad \= Loop over vertices $i, j$: \\
		  \> \> \quad Construct the matrix $D^{(k)}$ with entries $D^{(k)}_{ij} = \min(D^{(k-1)}_{ij}, D^{(k-1)}_{ik} + D^{(k-1)}_{kj}).$ \\
		  \> End \\
		  \> $D = D^{(n)}.$
\end{tabbing}
\end{minipage}
\end{center}

Write a function \texttt{allPairsDistance.m} (with help text) that takes \texttt{A} as its argument and returns \texttt{D} as computed the Floyd-Warshall algorithm. Try to be as efficient as possible! In particular, use vectorization; it is possible to write this function with no loops apart from  the main loop (\texttt{repmat} or \texttt{bsxfun} are useful, as is the linear indexing trick for setting the diagonal of a matrix) .
\section{The Laplace Operator}
The Laplace operator $\triangle$ is perhaps the fundamental differential operator in mathematical physics. In two dimensions, $\triangle u = \ptn{u}{x}{2} + \ptn{u}{y}{2}$. If we are given the values of $u$ on a regular grid with x-direction spacing $d_x$ and y-direction spacing $d_y$,  we can approximate $\triangle u_{i,j} \approx \frac{u_{i-1,j} - 2u_{i,j} + u_{i+1,j}}{4d_x^2} +  \frac{u_{i,j-1} - 2u_{i,j} + u_{i,j+1}}{4d_y^2}$, where $u_{i,j}$ is the value of the solution at the $(i,j)$th grid point. This is a linear operator, so if $u$ is a vector describing the values of a function at the grid points, we can write the approximation to $\triangle u$ at these same grid points as $Lu$ for some linear operator $L$, represented by a matrix. 

\begin{enumerate}[(a)]
\item Write a function m-file \texttt{laplacian.m} defining a function which accepts arguments \texttt{m} (the number of grid points in the y direction), \texttt{n} (the number of grid points in the x direction), \texttt{hx} (the grid spacing in the x direction), and \texttt{hy} (the grid spacing in the y direction), and returns \texttt{L}, the sparse, banded, $mn \times mn$ matrix corresponding to the above approximation to the Laplace operator. Use the convention that undefined data points outside the unit square have zero value. Note that you will have to define a \emph{linear} ordering of your grid points - the most natural way to do this is to follow MATLAB's column-major convention, that is if \texttt{u} is an $m \times n$ matrix of point values of $u$, use the ordering given by \texttt{u(:)}.  You can partially test your function by comparing to the output of \texttt{del2}, which computes something very similar (but with different boundary conditions, so only the interior values will be useful). 
\item Write a script m-file \texttt{prob2.m} , which performs the following tasks:
\begin{itemize}
\item Use your \texttt{laplacian} function to compute an approximation to $\triangle u$ on the unit square, with $m = 30$ and $n = 20$ and
$$
u(x,y) = \left\{ \begin{array}{lc} \cos(2 \pi x) \cos (6 \pi y) & x\in[1/4,3/4], y \in [1/4,3/4] \\ 0 & \text{otherwise} \end{array} \right.
$$
\item Use \texttt{meshgrid}, \texttt{subplot}, and \texttt{surf} to produce a single figure which plots $u$ and $Lu$, side by side.
\end{itemize}
\end{enumerate}

\end{document}
