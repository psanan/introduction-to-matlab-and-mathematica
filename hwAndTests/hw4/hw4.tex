\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}


% ---- New Commands --- %
	% Derivatives
	\newcommand{\pt}[2]{\frac{\partial{#1}}{\partial{#2}}}
	\newcommand{\ptn}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}
	\newcommand{\dv}[2]{\frac{d #1}{d #2}}
	\newcommand{\dvn}[3]{\frac{d^{#3} #1}{d #2^{#3}}}

	\newcommand{\eps}{\varepsilon}
	\newcommand{\tr}{\text{tr}}
	\newcommand{\x}{\times} % cross 
	\newcommand{\ox}{\otimes} % direct product
	\newcommand{\mbf}[1]{\mathbf{#1}} %math bold face
         \newcommand{\hh}{\frac{1}{2}} % one half
         \newcommand{\argmin}{\text{argmin}}
         \newcommand{\w}{\wedge} %wedge
         \newcommand{\W}{\bigwedge} %big wedge

	% Spaces
	\newcommand{\R}{\mathbb{R}} %R
	\newcommand{\RR}{\mathbb{R}^2} %R^2
	\newcommand{\RRR}{\mathbb{R}^3} %R^3
	\newcommand{\Rn}{\mathbb{R}^n} %R^n
	\newcommand{\C}{\mathbb{C}} %C
	\newcommand{\SO}{\text{SO}} %SO
	\newcommand{\GL}{\text{GL}} %GL

	% Matrix Shortcuts
	\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
	\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
	\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
	\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}
	
	% Text Styles
	\newcommand{\red}[1]{\textcolor{red}{#1}} % red text


% For commenting out solutions
	\newif\ifanswers
	\answerstrue % comment out to hide answers


\begin{document}

\begin{center}
\bf{\Large ACM 11, Spring 2013, Homework 4}\\
\bf{\Large ``The Sparse Cholesky Challenge''}\\
Due Thursday, May 9, 2013. \\
\end{center}


Submit by 23:59:59 PDT, as a \texttt{.zip} file \texttt{LastnameFirstnameHw4.zip} (which expands to a directory \texttt{LastnameFirstnameHw4} containing the m-files mentioned below), to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com}, with a subject line including your full name and ``Homework 4''.  \\

Before it was a broader environment used in such a vast number of applications, MATLAB was developed as a MATrix LABoratory. In that spirit, we'll use some of its basic functionality to do some experiments with matrices. 

\section*{Background}
A fundamental problem in numerical linear algebra is to compute a solution $x$ to the linear system $Ax = b$ in a `good' way, where `goodness' includes the following and more:
\begin{itemize}
\item If it exists and is unique, computing $x$ to a an accuracy acceptable to the user
\item If $x$ does not exist or is not unique, behaving well, which commonly means returning an error or providing an accurate least-squares solution (that is, $x$ which minimizes $||Ax-b||_2$).
\item Controlling the amount of time required to compute the solution 
\begin{itemize}
\item Controlling the number of arithmetic operations (`flops') required 
\item Controlling the time spent reading and writing to memory. (This is more complex than simply limiting the amount of memory required, and often provides the bottleneck in implementations)
\item Controlling the time spent communicating between processes, if working in parallel
\end{itemize}
\item Controlling the amount of memory required to compute the solution 
\item Controlling how the time and memory requirements scale with the size of $A$
\item Controlling how the time and memory requirement scale with the number of processors used in parallel
\item If required, allowing solving for multiple values of $b$
\end{itemize}

The aim of the following problems is to examine some of these issues when $A$ is large, sparse, real, positive definite (hence square), and symmetric, and when we'd like to be able to solve for multiple different values of $b$.

\section{Dense Linear Systems}
\begin{enumerate}[(a)]
\item Write a function m-file \texttt{randSPD.m} which accepts an integer $n$ as input and produces as output  a random, symmetric, positive definite, dense $n \times n$ matrix with the following procedure:
\begin{enumerate}
\item Construct a random matrix $A$ with each entry uniformly distributed on the unit interval, using \texttt{rand}.
\item Return $A^TA+I$, where $I$ is the $n \times n$ identity matrix. (Do not use any loops)
\end{enumerate}
Include comments so that \texttt{help randSPD} returns useful information.
\item Write a matlab script \texttt{prob1.m} which does the following:
\begin{enumerate}
\item Executes \texttt{close all; clear; clc}
\item Defines a list of $n$ to work with, \texttt{n = [10, 100, 1000, 2000]}.
\item Uses a \texttt{for} loop to do the following for each value of $n$
\begin{enumerate}
\item Generate a random matrix \texttt{S} with your function \texttt{randSPD}, as defined above.
\item Generate a random $n \times 1$ right hand side \texttt{b} with entries uniformly distributed on the unit interval, using \texttt{rand}.
\item Use \texttt{tic} and \texttt{toc} to time how long it takes to solve $Sx =  b$ (Use the backslash operator to solve the linear system: $x = S\backslash b$), and print the results out clearly using \texttt{disp} or \texttt{fprintf}. 
\end{enumerate}
\end{enumerate}
Include comments so that \texttt{help prob1} returns useful information.
\end{enumerate}

\section{Sparse Linear Systems}

\begin{enumerate}[(a)]
\item Write a function m-file \texttt{sprandSPD.m} which generates a random \emph{sparse} SPD matrix using the same procedure as your \texttt{randSPD} function, except replacing \texttt{rand} with \texttt{sprand} with a non-zero density of 0.005. Again, include comments so that \texttt{help sprandSPD} produces helpful output, and do not use any loops. 
\item Write a matlab script \texttt{prob2.m} which repeats the experiment from the previous problem, using the new function you just defined. Again, include a comment to provide useful help text.
\end{enumerate}

\section*{The Cholesky Factorization}
A real square matrix $A \in \mathbb{R}^{n \times n}$ is called \emph{positive definite} when $x^TAx > 0$ for all $x \in \mathbb{R}^n$. If $A$ is symmetric ($A = A^T$) and positive definite\footnote{Note that there is ambiguity in some of the literature; some authors include symmetry in the definition of positive definiteness} then $A$ has a unique decomposition $A = LL^T$, where $L$ is \emph{lower triangular} (is zero above the diagonal). 

Computing the Cholesky factor $L$ requires $O(n^3)$ operations. This decomposition is useful because solving triangular systems is much faster ($O(n^2)$) that solving a general system ($O(n^3)$). Thus, to solve $Ax = b$, solve $Ly = b$ followed by $L^Tx = y$. This is very useful if we would like to solve for multiple values of $b$. 

A problem with this (and other matrix decompositions) is that $L$ may have many more nonzero entries than $A$. Further, the number of non zeros may depend critically on the ordering of the variables in the system, that is, on permutations of $A$. Finding an optimal permutation is a computationally hard problem, but an effective practical choice is the \emph{reverse Cuthill-McKee algorithm}, implemented in MATLAB with \texttt{symrcm}.

[The alternative to a sparse factorization is to use an \emph{iterative method} - a preconditioned conjugate gradient (PCG) method might be attractive in these examples]

\section{The Sparse Cholesky Factorization}
\begin{enumerate}[(a)]
\item Write a function m-file \texttt{sparrow.m} (with help text) which accepts an integer $n$ as an argument and produces a sparse $n \times n$ matrix with the following non-zero structure
$$
\left[ \begin{array}{cccc} 2n & 1 & \cdots &  1\\ 1 & 2n  & & \\  \vdots & & \ddots & \\  1 & & & 2n \end{array}\right]
$$
\item Write a function m-file \texttt{sparrow2.m} (with help text) defining a function which accepts an integer $n$ and produces a sparse $n \times n$ matrix of the form 
$$
\left[ \begin{array}{cccc} 2n &  & &  1\\  & \ddots & & \vdots \\   & & 2n & 1 \\  1 & \cdots & 1 & 2n \end{array}\right]
$$
\item Write a function m-file \texttt{spbanded.m} (with help text) which uses \texttt{spdiags} to generate a banded sparse matrices with value $4$ on the main diagonal, and value $1$ on the first super- and sub-diagonals and value $-1$ on the $\text{floor}(n/2)$th super- and sub-diagonals.
\item Write a script m-file \texttt{prob3.m} (with help text) which uses \texttt{disp} or \texttt{fprintf} to print out the number of nonzero entries in the Cholesky factor $L$ for a $1000 \times 1000$ matrix generated in each of the following ways
\begin{enumerate}
\item Using your \texttt{randSPD} function
\item Using yout \texttt{sprandSPD} function
\item Using yout \texttt{sprandSPD} function, permuted using the indexing provided by \texttt{symrcm}.
\item Using your \texttt{sparrow} function.
\item Using your \texttt{sparrow} function, permuted using the indexing provided by \texttt{symrcm}.
\item Using your \texttt{sparrow2} function.
\item Using your \texttt{sparrow2} function, permuted using the indexing provided by \texttt{symrcm}.
\item Using your \texttt{spbanded} function
\item Using your \texttt{spbanded} function, permuted using the indexing provided by \texttt{symrcm}.
\end{enumerate}
Does the reverse Cuthill-McKee reordering always help?
\end{enumerate}

\end{document}
