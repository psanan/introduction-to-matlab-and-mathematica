\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}


% ---- New Commands --- %
	% Derivatives
	\newcommand{\pt}[2]{\frac{\partial{#1}}{\partial{#2}}}
	\newcommand{\ptn}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}
	\newcommand{\dv}[2]{\frac{d #1}{d #2}}
	\newcommand{\dvn}[3]{\frac{d^{#3} #1}{d #2^{#3}}}

	\newcommand{\eps}{\varepsilon}
	\newcommand{\tr}{\text{tr}}
	\newcommand{\x}{\times} % cross 
	\newcommand{\ox}{\otimes} % direct product
	\newcommand{\mbf}[1]{\mathbf{#1}} %math bold face
         \newcommand{\hh}{\frac{1}{2}} % one half
         \newcommand{\argmin}{\text{argmin}}
         \newcommand{\w}{\wedge} %wedge
         \newcommand{\W}{\bigwedge} %big wedge

	% Spaces
	\newcommand{\R}{\mathbb{R}} %R
	\newcommand{\RR}{\mathbb{R}^2} %R^2
	\newcommand{\RRR}{\mathbb{R}^3} %R^3
	\newcommand{\Rn}{\mathbb{R}^n} %R^n
	\newcommand{\C}{\mathbb{C}} %C
	\newcommand{\SO}{\text{SO}} %SO
	\newcommand{\GL}{\text{GL}} %GL

	% Matrix Shortcuts
	\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
	\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
	\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
	\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}
	
	% Text Styles
	\newcommand{\red}[1]{\textcolor{red}{#1}} % red text


% For commenting out solutions
	\newif\ifanswers
	\answerstrue % comment out to hide answers


\begin{document}

\begin{center}
\bf{\Large ACM 11, Spring 2013, Homework 6}\\
\bf{\Large Optimization Tools}\\
Due Thursday, May 23, 2013. \\
\end{center}

Submit by 23:59:59 PDT, as a \texttt{.zip} file \texttt{LastnameFirstnameHw6.zip} (which expands to a directory \texttt{LastnameFirstnameHw6} containing the m-files mentioned below), to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com}, with a subject line including your full name and ``Homework 6''.  \\

It's a common situation that we'd like to solve a computational problem and need to examine and test several alternative approaches. MATLAB's high-level approach, large collection of toolboxes, and even larger collection of third-party tools means it's often quite simple to test many different ways of solving a problem. Here, our task will be to compute minima for a collection of simple functions. 

\section*{Background}
Consider a smooth\footnote{Here, ``smooth'' means $\ptn{f}{x_i}{n}$ exists for all $n \in \mathbb{N}$. It is often taken to mean ``any derivatives you want to use exist''!} function $f : \mathbb{R}^n \to \mathbb{R}$.  
\begin{itemize}
\item $x^* \in \mathbb{R}^n$ is called a \emph{global minimizer} of $f$ if $f(x^*) \le f(x), \forall x \in \mathbb{R}^n$.
\item $x^* \in \mathbb{R}^n$ is called a \emph{local minimizer} of $f$ if there exists a ball\footnote{A (closed) ball is a set of points within some distance of a given point: here, $\mathcal{B}(x,r) \doteq \{ y \in \mathbb{R}^n : ||x-y|| \le r \}$}$\mathcal{B}(x^*,\epsilon)$ centered at $x^*$ with $\epsilon > 0$ such that $f(x^*) \le f(x),\forall x \in \mathcal{B}(x^*,\epsilon)$
\end{itemize}
That is, a global minimizer is a place where a function takes on its smallest value, and a local minimizer is a place where a function takes on its lowest value in some neighborhood. These may not be unique, and in fact there may be infinitely many (consider a function with a `flat bottom'). A useful analogy (which works most directly when $n=1,2$) is to think of $f$ as defining the height of a ``landscape'', and a minimizer as a place where it is impossible to go downhill. 

\section*{Optimization Problems}
We'll refer to the following set of minimization problems:
\begin{enumerate}
\item \label{NP} Minimize $(\cosh(x/2))^2 + (\arctan(x))^2$ for $x \in \mathbb{R}$
\item \label{QP} Minimize $\frac{1}{2} x^T H x + f^T x$ for $x \in \mathbb{R}^2$, with 
$$ 
H = \mattwo{4}{1}{1}{6}, \quad f = \colvectwo{3}{4}
$$
\item \label{LCQP}  Minimize $\frac{1}{2} x^T H x + f^T x$ for $x \in \mathbb{R}^2$, with $H$ and $f$ as in problem \ref{QP}, such that $Ax - b \le 0$, with
$$
A = \mattwo{1}{2}{4}{-4}, \quad b = \colvectwo{1}{-1}
$$
\item \label{SOCP} Minimize $\frac{1}{2} x^T H x + f^T x$ for $x \in \mathbb{R}^2$, with $H$ and $f$ as in problem \ref{QP}, such that $||Ax - b||_2 \le Cx + d$, with $A$ and $b$ as in problem \ref{LCQP} and 
$$
C = \mattwo{1}{0}{2}{1} \quad d = \colvectwo{2}{0}
$$

\item \label{OscillatoryP}Minimize $f(x,y)$ over the unit square\footnote{Recall that this is the set where both $x$ and $y$ are in the interval $[0,1]$, that is the set $\{(x,y) \in \mathbb{R}^2 : 0 \le x \le 1, 0 \le y \le 1 \}$}, where $f$ is given by 
$$
 f(x,y) = \left(\sum_{i=1}^5 i \cos[(i+1)x + i]\right)\left(\sum_{j=1}^5 j \cos[(j+1)y + j]\right)
$$
%\item \label{ElastP}... % Elastic energy to do graph layout (springs!)
%\item \label{LCElastP}... % Elastic energy with point constraints (spell out ACM11 - provide a mat file with the required data! ) [Shrink down and put into title!]
\end{enumerate}

\section{ The Optimization Toolbox}
Write a script m-file \texttt{prob1and3.m} which includes a cell which uses the optimization toolbox to compute minima for problems \ref{NP}, \ref{QP}, \ref{LCQP}, and \ref{OscillatoryP}. In each case, print the minimizer and the minimal value of the objective clearly with \texttt{disp} or \texttt{fprintf}. Use \texttt{optimset} to turn off the display, and set the algorithm appropriately (by default, MATLAB will complain and switch the algorithm - make this choice yourself to suppress the warnings). Comment on your confidence in the computed solution for problem \ref{OscillatoryP} being a global minimizer.

\section{Evolutionary Minimization (Extra Credit)} 
Problem \ref{OscillatoryP} is difficult to solve efficiently, as it has so many local minima. 
For up to 30 \% extra credit, devise your own randomized `evolutionary' method to try to find the global minimum and plot the minimizer and minimal value on a graph of $f$. Include it as \texttt{prob2.m}. One procedure is
\begin{enumerate}
\item Start with a moderate number of points $p_i$, $i = 1,...,n$ ( with $n$ on the scale of a few times the number of local minima you expect) distributed in the unit square.
\item \begin{enumerate}
\item For each point $p_i$, scatter a set of new points $q_{ij}$, $j = 1,...,m$ in a square with side length $s$, centered at $p_i$. Compute the function values $f(p_i)$, and $f(q_{ij})$, and replace $p_i$ with the point which minimizes $f$ over the set $\{ p_i, q_{i1},...,q_{im}\}$.
\item Reduce $s$ and repeat until $s$ reaches a given tolerance, or no points are changed in a given iteration. 
\end{enumerate}
\item Return the point which minimizes $f$ over the set $\{p_1,...,p_n\}$.
\end{enumerate}

\section{CVX}
Download \href{http://cvxr.com/cvx/download/}{cvx} and get it running in MATLAB. Add a cell to your script m-file \texttt{probs1and3.m} which uses cvx to compute minima for problems \ref{QP}, \ref{LCQP}, and \ref{SOCP} above. Print the minimizer and the minimal value of the objective clearly with \texttt{disp} or \texttt{fprintf}. Your script may assume that \texttt{cvx\_setup} has been called. Use \texttt{quiet} once you have debugged to limit the amount of output.

\section{Mathematica (Extra Credit)}
For up to 30\% extra credit, use Mathematica to compute minimizers for  problems \ref{NP}, \ref{QP}, and \ref{LCQP} above, and include a notebook file \texttt{prob4.nb} with your submission.

\end{document}
