\documentclass[11pt]{article}

% --- Packages ---%
	\usepackage{amsmath}
	\usepackage{amsfonts}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{latexsym}
	\usepackage{graphicx}
	\usepackage{array}
	\usepackage{fullpage}
	\usepackage{color}
	\usepackage{enumerate} %lets you change the numbering
	\usepackage[colorlinks=true]{hyperref}


% ---- New Commands --- %
	% Derivatives
	\newcommand{\pt}[2]{\frac{\partial{#1}}{\partial{#2}}}
	\newcommand{\ptn}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}
	\newcommand{\dv}[2]{\frac{d #1}{d #2}}
	\newcommand{\dvn}[3]{\frac{d^{#3} #1}{d #2^{#3}}}

	\newcommand{\eps}{\varepsilon}
	\newcommand{\tr}{\text{tr}}
	\newcommand{\x}{\times} % cross 
	\newcommand{\ox}{\otimes} % direct product
	\newcommand{\mbf}[1]{\mathbf{#1}} %math bold face
         \newcommand{\hh}{\frac{1}{2}} % one half
         \newcommand{\argmin}{\text{argmin}}
         \newcommand{\w}{\wedge} %wedge
         \newcommand{\W}{\bigwedge} %big wedge

	% Spaces
	\newcommand{\R}{\mathbb{R}} %R
	\newcommand{\RR}{\mathbb{R}^2} %R^2
	\newcommand{\RRR}{\mathbb{R}^3} %R^3
	\newcommand{\Rn}{\mathbb{R}^n} %R^n
	\newcommand{\C}{\mathbb{C}} %C
	\newcommand{\SO}{\text{SO}} %SO
	\newcommand{\GL}{\text{GL}} %GL

	% Matrix Shortcuts
	\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
	\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
	\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
	\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}
	
	% Text Styles
	\newcommand{\red}[1]{\textcolor{red}{#1}} % red text


% For commenting out solutions
	\newif\ifanswers
	\answerstrue % comment out to hide answers


\begin{document}

\begin{center}
\bf{\Large ACM 11, Spring 2013, Midterm Project}\\
Due Thursday, May 2, 2013. \\
\end{center}


Submit by 23:59:59 PDT, as a single Mathematica notebook file \texttt{LastnameFirstnameMidterm.nb}, to \href{mailto:acm11spring2013@gmail.com}{acm11spring2013@gmail.com}, with a subject line including your full name and ``Midterm''.  \\

The following project should be completed with Mathematica. Note that some of the following guidelines here and below have changed from those in the homework:
\begin{itemize}
\item There is no collaboration allowed on this midterm project 
\begin{itemize}
\item You may not discuss any of the specifics of the project you choose to implement with other students.
\item Review of the previous homework and anything covered in the course notes or Mathematica's documentation is acceptable. 
\end{itemize}
\item Please ask the instructor or TA if anything is unclear about the project requirements or collaboration policy.
\end{itemize}

\section{Midterm Project}

Choose a contained computational problem to solve, either from the suggestions below, from a scientific area you're interested in, or from your imagination. The project must be your own work - if you're inspired by something you've seen online or elsewhere, please discuss with the instructors to make sure you'll be modifying it enough. The best projects will demonstrate your facility using Mathematica to pose, solve, understand, and analyze a problem. As such, creativity is encouraged!

\subsection{Content Requirements}
Whichever project you choose, it should use and demonstrate your knowledge of the following concepts and constructs (You are not required to use every single one of these techniques, but should be able to apply them when appropriate):
\begin{itemize}
\item \texttt{List} processing
\item Use of both procedural and functional programming approaches 
\item Anonymous functions
\item Scoping of variables with \texttt{Module} or \texttt{Block}.
\item Explicit consideration of the precision being used to compute with
\item A computationally intensive procedure (such as integration, solution of a differential equation, or data processing), which is timed.  
\item Well-formatted, readable, and informative plots and/or interactive elements
\end{itemize}

\subsection{Other Requirements}
\begin{itemize}
\item Make your submitted notebook is clear and readable. Use section, subsections, and comments.
\item To allow for grading, please make sure that it does not take more than a few minutes to evaluate your notebook. [If you have a project idea that will take longer than that, please let us know and we can figure out how you can produce the output yourself and include it in your submission]
\item Please follow all the other guidelines in the course guide (section 2). 
\end{itemize}

\section{Project Ideas}
\subsection{The Gravitational Three-Body Problem}

The gravitational $n$-body problem is to describe the motion of $n$ spherical bodies under the influence of a pairwise gravitational potential (which follows an inverse square law).   We consider $n$ spherical masses with masses $m_i, i = 1,...,n$, which we treat as point particles. Let $p_i$ denote the position in space (a 3-dimensional vector) of the $i$th mass. The evolution of the system is then governed by the following system of ODE:
$$
p''_i(t) = \sum_{j \ne i} m_j G \frac{p_j(t)-p_i(t)}{||p_j(t)-p_i(t)||^{3}}, \quad p_i(0) = P_i, \quad  p_i'(0) = V_i
$$

% Check that this is actually correct! 

where the sum is over $j = 1,...,n$ with $j\ne i$, and the norm $|| \cdot ||$ is the usual Euclidean 2-norm. 
For $n=3$, solutions are not simple to compute, and this problem has been around for hundreds of years. Amazingly, some new periodic solutions were recently described (\href{http://news.sciencemag.org/sciencenow/2013/03/physicists-discover-a-whopping.html}{see this news article with links to the paper}). Your task is to approximate some  solutions numerically with Mathematica.
\begin{enumerate}
\item Write a function which computes a numerical solution to the ODE system for $n=3$, given a final time $T$ and a list of rules defining the masses, initial positions, initial velocities, and other parameters of the system. 
\item Write a function which uses \texttt{Manipulate} to produce an interactive tool allowing the visualization of the body trajectories in 3D over a subinterval $[t_1,t_2] \subset [0,T]$.
\item Using astronomically plausible parameter values (that is, masses corresponding to stars\footnote{Recall that Mathematica itself can give you these values, for example \texttt{AstronomicalData["Sun", "Mass"]}}, planets, or galaxies,  distances corresponding to the spacing between them, and appropriate timescales), use your functions above to visualize solutions to two different three-body scenarios:
\begin{enumerate}
\item A `chaotic' setup with zero initial linear momentum ($\sum_{i=1}^3 m_i V_i = \vec{0} $) and non-planar motion.
\item A `symmetric' planar setup which you expect to give a periodic solution. Choose your integration method to maintain periodicity for as long as possible. 
\end{enumerate}
\end{enumerate}


\subsection{Random Graphs }

This project examines an interesting question in random graph theory. If a graph is chosen randomly by some process, what is the probability that it is connected? Recall that a connected graph is one in which any pair of vertices can be connected by a sequence of edges. Choose one of the following two methods of generating random graphs, or pick another:
\begin{itemize}
\item Erd\"{o}s-Reny\'{i} Graphs. For a given number of vertices $n$, add each possible edge to the graph independently with probability $p$. 
\item Random Geometric Graphs. Distribute $n$ points randomly in the unit square, and connect any pair with pairwise Euclidean distance less than $\gamma$.
\end{itemize}

A fascinating fact is that as $n \to\infty$ there exist critical values $p^*(n) = \frac{\log{n}}{n}$ and $\gamma^*(n) = \sqrt{\frac{\log{n}}{\pi n}}$ which sharply divide regimes where the graph is extremely likely to be connected or disconnected. Your task is to numerically observe these facts for finite $n$ by generating random graphs.
\begin{enumerate}
\item Write a function which accepts arguments $n$ and $p$/$\gamma$, and, according to the method chosen, produces a symmetric \emph{adjacency matrix}, an $n$ by $n$ matrix where entry $(i,j)$ is one if an edge connects vertices $i$ and $j$, a and is zero otherwise. 
\item Using \texttt{ConnectedGraphQ} and \texttt{AdjacencyGraph}, write a second function which accepts arguments $n$, $p$/$\gamma$, and $m$, generates $m$ random graphs using the previously defined function, and returns the fraction of graphs which are connected. That is, write a function to estimate the probability that a randomly generated graph with the given parameters is connected
\item Choose test values of $n$ and $p$/$\gamma$ which will highlight the `phase transition' and devise a way to visualize your estimated probabilities of connectedness. What do you observe? How useful are the asymptotic critical values in predicting the behavior of small graphs?
\end{enumerate}
[Note: there is a \texttt{RandomGraph} function in Mathematica, which may decide to use instead of generating adjacency matrices, but be very sure you are using it correctly]

\end{document}
